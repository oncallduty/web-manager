<?php

define('endl', "\n");

define('CACHE_EXPIRE_YEAR', 31104000); // 1 year
define('CACHE_EXPIRE_MONTH', 2592000); // 30 days
define('CACHE_EXPIRE_WEEK', 604800);   // 7 days
define('CACHE_EXPIRE_DAY', 86400);     // 1 day
define('CACHE_EXPIRE_HOUR', 3600);     // 1 hour
define('CACHE_EXPIRE_MINUTE', 60);     // 1 minute
define('CACHE_EXPIRE_SECOND', 1);      // 1 second
   
?>
$(document).ready(function(){
	$(".tbl_info tr:even, .tbl_item tr:even, ul.category >li:even, ul.category li ul li:even, .depositions_list >li:odd, .depo_folders_list >li:even, .depo_files_list >li:even").addClass("zebra");
	
    /*Custom SelectBox*/
    var openSelectList = false;
	
    $(".custom-select .arrow, .custom-select .field").bind('click', function(e){
        e.stopPropagation();  
        $(this).parent().find('ul').css({'width':$(this).parent().outerWidth()-2, 'display':'block'});
        openSelectList = true;   
        $(this).parent().find('ul li').bind('click', function(){
            $(this).parent().css({'display':'none'});
			      $(this).parents('.custom-select').find('.field').text($(this).text())
        })
    })
	
	$('body').bind('click', function(){
		if(openSelectList==true){
			$('.custom-select ul').css({'display':'none'})
		}
	})
});

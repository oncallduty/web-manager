<?php

namespace ClickBlocks\API;

/**
 * This purpose of this interface is to store all constants related to business logic;
 * they will be available in all APIController classes and MVC\Page classes
  */
interface IEdepoBase
{
	const ORM_ENTITY_ORCHESTRA = 'orc';
	const ORM_ENTITY_SERVICE = 'svc';

	const USER_TYPE_SUPERADMIN	= 'SA';
	const USER_TYPE_ADMIN		= 'A';
	const USER_TYPE_RESELLER	= 'R';
	const USER_TYPE_CLIENT		= 'C';
	const USER_TYPE_USER		= 'CU';
	const USER_TYPE_JUDGE		= 'JDG';
	const USER_TYPE_REPORTER	= 'RPT';

	const PRICING_TYPE_ONETIME  = 'O';
	const PRICING_TYPE_MONTHLY  = 'M';

	const CLIENT_TYPE_OWNER			= 'O';
	const CLIENT_TYPE_RESELLER		= 'R';
	const CLIENT_TYPE_CLIENT		= 'C';
	const CLIENT_TYPE_ENT_RESELLER	= 'ER';
	const CLIENT_TYPE_ENT_CLIENT	= 'EC';
	const CLIENT_TYPE_COURTCLIENT	= 'CRT';

	const CASE_CLASS_CASE = 'Case';
	const CASE_CLASS_DEMO = 'Demo';

	const DEPOSITION_CLASS_DEPOSITION	= 'Deposition';
	const DEPOSITION_CLASS_DEMO			= 'Demo';
	const DEPOSITION_CLASS_WITNESSPREP	= 'WitnessPrep';
	const DEPOSITION_CLASS_WPDEMO		= 'WPDemo';
	const DEPOSITION_CLASS_TRIAL		= 'Trial';
	const DEPOSITION_CLASS_DEMOTRIAL	= 'Demo Trial';
	const DEPOSITION_CLASS_TRIALBINDER	= 'Trial Binder';
	const DEPOSITION_CLASS_MEDIATION	= 'Mediation';
	const DEPOSITION_CLASS_ARBITRATION	= 'Arbitration';
	const DEPOSITION_CLASS_HEARING		= 'Hearing';

	const DEPOSITION_STATUS_NEW = 'N';
	const DEPOSITION_STATUS_IN_PROCESS = 'I';
	const DEPOSITION_STATUS_FINISHED = 'F';

	const DEPOSITIONATTENDEE_ROLE_MEMBER = 'M';
	const DEPOSITIONATTENDEE_ROLE_GUEST = 'G';
	const DEPOSITIONATTENDEE_ROLE_WITNESS = 'W';
	const DEPOSITIONATTENDEE_ROLE_TEMPWITNESS = 'TW';
	const DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER = 'WM';

	//const MOCK_TYPE_USER        = 'user';
	//const MOCK_TYPE_FS          = 'fs';
	const MOCK_TYPE_ORCHESTRA   = 'orc';
	const MOCK_TYPE_SERVICE     = 'svc';
	const MOCK_TYPE_BLL         = 'bll';
	const MOCK_TYPE_PROPERTY    = 'prop';

	const PRICING_OPTION_BASE_LICENSING           = 1;
	const PRICING_OPTION_SETUP                    = 2;
	const PRICING_OPTION_SUBSCRIPTION             = 3;
	const PRICING_OPTION_CASE_SETUP               = 4;
	const PRICING_OPTION_CASE_MONTHLY             = 5;
	const PRICING_OPTION_DEPOSITION_SETUP         = 6;
	const PRICING_OPTION_DEPOSITION_MONTHLY       = 7;
	const PRICING_OPTION_UPLOAD_PER_DOC           = 8;
	const PRICING_OPTION_ADDITIONAL_BUNDLE        = 9;	//deprecated
	const PRICING_OPTION_EXHIBIT_RECEIVED_TIER1   = 10;
	const PRICING_OPTION_EXHIBIT_RECEIVED_TIER2   = 11;
	const PRICING_OPTION_EXHIBIT_RECEIVED_TIER3   = 12;
	const PRICING_OPTION_EXHIBIT_INTRODUCED_TIER1 = 13;
	const PRICING_OPTION_EXHIBIT_INTRODUCED_TIER2 = 14;
	const PRICING_OPTION_EXHIBIT_INTRODUCED_TIER3 = 15;
	const PRICING_OPTION_ENTERPRISE_EXHIBIT_INTRODUCED_TIER1 = 16;	//deprecated
	const PRICING_OPTION_ENTERPRISE_EXHIBIT_INTRODUCED_TIER2 = 17;	//deprecated
	const PRICING_OPTION_ENTERPRISE_EXHIBIT_INTRODUCED_TIER3 = 18;	//deprecated
	const PRICING_OPTION_WITNESSPREP_SETUP    = 19;	//deprecated
	const PRICING_OPTION_WITNESSPREP_ATTENDEE = 20;

	//Reseller Non-Client Deposition Fees
	const PRICING_OPTION_NC_EXHIBIT_RECEIVED_TIER1   = 21;	//deprecated ED-2601
	const PRICING_OPTION_NC_EXHIBIT_RECEIVED_TIER2   = 22;	//deprecated ED-2601
	const PRICING_OPTION_NC_EXHIBIT_RECEIVED_TIER3   = 23;	//deprecated ED-2601
	const PRICING_OPTION_NC_EXHIBIT_INTRODUCED_TIER1 = 24;	//deprecated ED-2601
	const PRICING_OPTION_NC_EXHIBIT_INTRODUCED_TIER2 = 25;	//deprecated ED-2601
	const PRICING_OPTION_NC_EXHIBIT_INTRODUCED_TIER3 = 26;	//deprecated ED-2601

	const FOLDERS_COURTESYCOPY       = 'CourtesyCopy';
	const FOLDERS_EXHIBIT            = 'Exhibit';
	const FOLDERS_FOLDER             = 'Folder';
	const FOLDERS_PERSONAL           = 'Personal';
	const FOLDERS_TRANSCRIPT         = 'Transcript';
	const FOLDERS_TRUSTED            = 'Trusted';
	const FOLDERS_WITNESSANNOTATIONS = 'WitnessAnnotations';

	const RESELLERLEVEL_PLATINUM = 'Platinum';
	const RESELLERLEVEL_GOLD     = 'Gold';
	const RESELLERLEVEL_SILVER   = 'Silver';
	const RESELLERLEVEL_BRONZE   = 'Bronze';
	const RESELLERLEVEL_NONE     = 'None';

	const RESELLERCLASS_RESELLER   = 'Reseller';
	const RESELLERCLASS_SCHEDULING = 'Scheduling';
	const RESELLERCLASS_DEMO       = 'Demo';

}

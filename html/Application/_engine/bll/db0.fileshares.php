<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $depositionID
 * @property bigint $createdBy
 * @property timestamp $created
 * @property bigint $fileID
 * @property bigint $copyFileID
 * @property bigint $userID
 * @property bigint $attendeeID
 * @property navigation $depositionAttendees
 * @property navigation $depositions
 * @property navigation $files
 * @property navigation $users
 */
class FileShares extends BLLTable
{
	public function __construct( $ID=NULL )
	{
		parent::__construct();
		$this->addDAL( new DALFileShares(), __CLASS__ );
		if( $ID ) {
			$this->assignByID( $ID );
		}
	}

  protected function _initdepositionAttendees()
  {
    $this->navigators['depositionAttendees'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositionAttendees');
  }

  protected function _initdepositions()
  {
    $this->navigators['depositions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositions');
  }

  protected function _initfiles()
  {
    $this->navigators['files'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'files');
  }

  protected function _initusers()
  {
    $this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
  }
}

?>
<?php

namespace ClickBlocks\DB;

use ClickBlocks\MVC\Edepo;

/**
 * @property bigint $ID
 * @property bigint $clientID
 * @property bigint $createdBy
 * @property varchar $name
 * @property varchar $number
 * @property timestamp $created
 * @property timestamp $deleted
 * @property varchar $clientFirstName
 * @property varchar $clientLastName
 * @property varchar $clientAddress
 * @property varchar $clientAddress2
 * @property varchar $clientCity
 * @property char $clientState
 * @property varchar $clientRegion
 * @property varchar $clientZIP
 * @property char $clientCountryCode
 * @property varchar $clientEmail
 * @property varchar $clientPhone
 * @property navigation $clients
 * @property navigation $lookupCaseTypes
 * @property navigation $users
 * @property navigation $caseManagers
 * @property navigation $depositions
 * @property navigation $invoiceCharges
 * @property enum $class
 * @property bigint $sourceID
 */
class Cases extends BLLTable {

	public function __construct( $ID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALCases(), __CLASS__ );
		if( $ID ) {
			$this->assignByID( $ID );
		}
	}

	public function getCopy() {
		$vals = $this->getValues();
		$vals['ID'] = $vals['createdBy'] = $vals['created'] = $vals['deleted'] = '';
		$copy = new Cases;
		$copy->setValues($vals);
		return $copy;
	}

	protected function _initlookupCaseTypes() {
		$this->navigators['lookupCaseTypes'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'lookupCaseTypes' );
	}

	protected function _initclients() {
		$this->navigators['clients'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'clients' );
	}

	protected function _initusers() {
		$this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'users' );
	}

	protected function _initcaseManagers() {
		$this->navigators['caseManagers'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'caseManagers' );
	}

	protected function _initdepositions() {
		$this->navigators['depositions'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'depositions' );
	}

	protected function _initinvoiceCharges() {
		$this->navigators['invoiceCharges'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'invoiceCharges' );
	}

	public function setAsDeleted() {
		$this->deleted = date( 'Y-m-d H:i:s' );
		$this->update();
		$this->deleteCaseFolders();
		// This should be in trigger maybe?
		foreach( $this->depositions as $depo ) {
			$depo->setAsDeleted();
		}
	}

	public function delete() {
		// don't change this!
		foreach( $this->depositions as $depo ) {
			$depo->delete();
		}
		$this->deleteCaseFolders();
		parent::delete();
	}

	private function deleteCaseFolders() {
		$orcFolders = new OrchestraFolders();
		$folderIDs = $orcFolders->getCaseFolderIDs( $this->ID );
		if( is_array( $folderIDs ) && $folderIDs ) {
			foreach( $folderIDs as $folderID ) {
				$folder = new Folders( $folderID );
				if( in_array( $folder->class, [ Edepo::FOLDERS_EXHIBIT, Edepo::FOLDERS_TRANSCRIPT ] ) ) {
					$folder->deleteFromElasticSearch();
					continue;
				}
				$folder->delete();
			}
		}
	}

	public function save() {
		$insert = ($this->isInstantiated() == FALSE);
		$r = parent::save();
		if( $insert ) {
			$this->afterInsert();
		}
		return $r;
	}

	public function insert() {
		$r = parent::insert();
		$this->afterInsert();
		return $r;
	}

	public function getClientAdmin() {
		return ( new OrchestraUsers() )->getClientAdminID( $this->clientID );
	}

	public function beforeInsert() {
		$this->createdBy = $this->getClientAdmin();
	}

	public function beforeUpdate() {
		$this->createdBy = $this->getClientAdmin();
	}

	public function afterInsert() {
		$client = $this->getClient();
		if( $client->typeID != Edepo::CLIENT_TYPE_COURTCLIENT ) {
			$this->createCaseFolders();
		}
		// Charge client for all new Cases
		if( !$this->isDemo() && $client->typeID != Edepo::CLIENT_TYPE_COURTCLIENT ) {
			ServiceInvoiceCharges::charge( $this->clientID, Edepo::PRICING_OPTION_CASE_SETUP, 1, $this->ID );
			ServicePlatformLog::addLog( $this->clients[0]->resellerID, Edepo::PRICING_OPTION_CASE_SETUP, 1, $this->clientID, $this->createdBy, $this->ID );
		}
	}

	protected function createCaseFolders() {
		$folders = [
			Edepo::FOLDERS_EXHIBIT => $this->config->logic['caseExhibitsFolderName'],
			// Edepo::FOLDERS_TRANSCRIPT => $this->config->logic['caseTranscriptsFolderName']
		];
		$now = date( 'Y-m-d H:i:s' );
		$orcUser = new OrchestraUsers();
		$clientAdminID = $orcUser->getClientAdminID( $this->clientID );
		foreach( $folders as $folderClass => $folderName ) {
			$folder = new Folders();
			$folder->name = $folderName;
			$folder->class = $folderClass;
			$folder->createdBy = $clientAdminID;
			$folder->caseID = $this->ID;
			$folder->created = $now;
			$folder->lastModified = $now;
			$folder->insert();
		}
	}

	/**
	 * ED-1258 Create a demo case for a new client. This is so new clients have a case
	 * they can utilize for training purposes.
	 * @param $clientID int    The ID for the client to create the case under.
	 * @param $userID int      ID of the user who will own the case.
	 **/
	public function createDemoCase( $clientID, $userID ) {
		$client = new Clients( $clientID );
		// Do case creation
		if( !$client->ID ) {
			return false;
		}

		$this->clientID = $client->ID;
		$this->ID = NULL;
		$this->clientID = $client->ID;
		$this->createdBy = $userID;
		$this->name = 'Sample Case (DEMO)';
		$this->number = '1';
		$this->created = date( 'Y-m-d H:i:s' );
		$this->deleted = NULL;
		$this->clientFirstName = 'Demo';
		$this->clientLastName = 'Demo';
		$this->clientAddress = $client->address1 ? $client->address1 : '123 Demo Road';
		$this->clientAddress2 = $client->address2 ? $client->address2 : 'Demo Suite';
		$this->clientCity = $client->city;
		$this->clientState = $client->state;
		$this->clientZIP = $client->ZIP;
		$this->clientCountryCode = $client->countryCode;
		$this->clientRegion = $client->region;
		$this->clientEmail = $client->contactEmail;
		$this->clientPhone = $client->contactPhone;
		$this->class = Edepo::CASE_CLASS_DEMO;
	}

	public function isDemoCase() {
		return $this->isDemo();
	}

	public function isDemo() {
		return ($this->class == Edepo::CASE_CLASS_DEMO);
	}

	public function getValues( $isRawData=FALSE, $userID=NULL ) {
		$values = parent::getValues( $isRawData, $userID );
		if( $userID ) {
			$orchCases = new \ClickBlocks\DB\OrchestraCases();
			$values['sortPos'] = $orchCases->getSortPosition( $this->ID, $userID );
		}
		return $values;
	}

	public function checkPermission( $userID ) {
		$userID = (int)$userID;
		$caseID = (int)$this->ID;
		if( !$userID || !$caseID || !$this->clientID ) {
			return FALSE;
		}
		$user = new \ClickBlocks\DB\Users( $userID );
		if( !$user || !$user->ID || $user->ID != $userID || $user->clientID != $this->clientID ) {
			return FALSE;
		}

		//client admin
		if( $user->typeID === Edepo::USER_TYPE_CLIENT || $user->clientAdmin ) {
			return TRUE;
		}
		//demo case
		if( $this->class === Edepo::CASE_CLASS_DEMO ) {
			return TRUE;
		}
		//case managers
		$orchCaseMgrs = new \ClickBlocks\DB\OrchestraCaseManagers();
		if( $orchCaseMgrs->checkCaseManager( $caseID, $userID ) ) {
			return TRUE;
		}
		$orchCases = new \ClickBlocks\DB\OrchestraCases();
		//deposition owners
		$ownerIDs = $orchCases->getDepositionOwners( $caseID );
		if( in_array( $userID, $ownerIDs ) ) {
			return TRUE;
		}
		//deposition assistants
		$assistantIDs = $orchCases->getDepositionAssistants( $caseID );
		if( in_array( $userID, $assistantIDs ) ) {
			return TRUE;
		}
		//deposition attendees
		$attendeeIDs = $orchCases->getDepositionAttendees( $caseID );
		if( in_array( $userID, $attendeeIDs ) ) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * @return \ClickBlocks\DB\Clients
	 */
	protected function getClient() {
		return $this->clients[0];
	}
}

<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
	ClickBlocks\API,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\Cache,
	ClickBlocks\Debug;

/**
 * @property bigint $ID
 * @property char $typeID
 * @property bigint $clientID
 * @property varchar $firstName
 * @property varchar $lastName
 * @property varchar $email
 * @property varchar $username
 * @property varchar $phone
 * @property varchar $address1
 * @property varchar $address2
 * @property varchar $city
 * @property char $state
 * @property varchar $region
 * @property varchar $ZIP
 * @property char $countryCode
 * @property timestamp $created
 * @property timestamp $lastLogin
 * @property timestamp $deleted
 * @property tinyint $termsOfService
 * @property tinyint $clientAdmin
 * @property char $phpSessionID
 * @property varchar $affiliateCode
 * @property tinyint $activated
 * @property navigation $clients
 * @property navigation $lookupUserTypes
 * @property navigation $aPISessions
 * @property navigation $caseManagers
 * @property navigation $cases
 * @property navigation $depositionAssistants
 * @property navigation $depositionAttendees
 * @property navigation $depositions
 * @property navigation $files
 * @property navigation $folders
 * @property navigation $passwordResetRequests
 * @property navigation $userRoles
 * @property navigation $fileShares
 * @property navigation $auditLog
 * @property navigation $depositionsOwned
 * @property navigation $depositionsSpeaker
 */
class Users extends BLLTable {
	/**
	 * if provided $ID, will load User from DB
	 * @param string $ID
	 */
	public function __construct($ID = null) {
		parent::__construct();
		$this->addDAL(new DALUsers(), __CLASS__);
		if ($ID) $this->assignByID($ID);
	}

	protected function _inituniqueids() {
		$this->navigators['uniqueids'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'uniqueids');
	}

	protected function _initlookupUserTypes() {
		$this->navigators['lookupUserTypes'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'lookupUserTypes');
	}

	protected function _initclients() {
		$this->navigators['clients'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'clients');
	}

	protected function _initcaseManagers() {
		$this->navigators['caseManagers'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'caseManagers');
	}

	protected function _initcases() {
		$this->navigators['cases'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'cases');
	}

	protected function _initdepositionAssistants() {
		$this->navigators['depositionAssistants'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositionAssistants');
	}

	protected function _initdepositionAttendees() {
		$this->navigators['depositionAttendees'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositionAttendees');
	}

	protected function _initdepositions() {
		$this->navigators['depositions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositions');
	}

	protected function _initdepositionsOwned() {
		$this->navigators['depositionsOwned'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositionsOwned');
	}

	protected function _initfiles() {
		$this->navigators['files'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'files');
	}

	protected function _initfolders() {
		$this->navigators['folders'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'folders');
	}

	protected function _inituserRoles() {
		$this->navigators['userRoles'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'userRoles');
	}

	protected function _inituserAuth() {
		$this->navigators['userAuth'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'userAuth' );
	}

	public function getMyResellerUrl() {
		$resellerTypes = [API\IEdepoBase::CLIENT_TYPE_RESELLER];

		$client = $this->clients[0];
		if (!in_array( $client->typeID, $resellerTypes ))
			$client = $this->clients[0]->clients[0];
		return (in_array( $client->typeID, $resellerTypes )) ? "/{$client->URL}" : false;
	}

	public function isAdmin() {
		return in_array($this->typeID, array(Edepo::USER_TYPE_ADMIN, Edepo::USER_TYPE_SUPERADMIN));
	}

	public function isTrustedUser($depositionID) {
		return foo(new OrchestraDepositions)->isTrustedUser($this->ID, $depositionID);
	}

	public function getAppropriateBackendUrl() {
		if ($this->isAdmin()) return '/admin';
		else return $this->getMyResellerUrl();
	}

	protected function _initpasswordResetRequests() {
		$this->navigators['passwordResetRequests'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'passwordResetRequests');
	}

	public function delete() {
		foreach($this->folders as $folder) {
			if(in_array($folder->class, ['Folder', 'Personal', 'WitnessAnnotations'])) {
				foreach($folder->files as $file) {
					$file->delete();
				}
				$folder->delete();
			}
		}
		if($this->typeID == API\IEdepoBase::USER_TYPE_USER) {
			// When deleting User (maybe he is set as Case Manager and created some depositions) - just change deposition creator and owner to Client Admin (if he exists)
			$myAdminID = (new OrchestraUsers())->getClientAdminID($this->clientID);
			//if (!$myAdminID) throw new \Exception(__METHOD__.': client admin cannot be determined!');
			if($myAdminID) {
				$createdDepositionIDs = (new OrchestraDepositions())->getDepositionsByCreator( $this->ID );
				foreach($createdDepositionIDs as $createdDepoID) {
					$cDeposition = (new ServiceDepositions())->getByID( $createdDepoID );
					$cDeposition->createdBy = $myAdminID;
					if($cDeposition->ownerID === $this->ID) {
						$cDeposition->ownerID = $myAdminID;
					}
					$cDeposition->update();
				}

				$ownedDepositionIDs = (new OrchestraDepositions())->getDepositionsByOwner( $this->ID );
				foreach($ownedDepositionIDs as $ownedDepoID) {
					$oDeposition = (new ServiceDepositions())->getByID( $ownedDepoID );
					$oDeposition->ownerID = $myAdminID;
					$oDeposition->update();
				}
				foreach($this->cases as $case) {
					$case->createdBy = $myAdminID;
					$case->update();
				}
			}
		} else {
			// When deleting Client Admin - delete all depositions and cases
			foreach($this->depositions as $depo) {
				$depo->delete();
			}
			foreach($this->cases as $case) {
				$case->delete();
			}
		}
		$this->username = NULL;
		$this->deleted = date( 'Y-m-d H:i:s' );
		$this->update();
//		parent::delete(); // DO NOT DELETE Users record
	}

	protected function _initaPISessions() {
		$this->navigators['aPISessions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'aPISessions');
	}

	protected function _initfileShares() {
		$this->navigators['fileShares'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'fileShares');
	}

	protected function _initauditLog() {
		$this->navigators['auditLog'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'auditLog');
	}

	public function save() {
		$insert = ($this->isInstantiated() == false);
		if( $insert ) {
			$this->beforeInsert();
		}
		$r = parent::save();
		if( $insert ) {
			$this->afterInsert();
		} else {
			$this->afterUpdate();
		}
		return $r;
	}

	public function insert() {
		$this->beforeInsert();
		$r = parent::insert();
		$this->afterInsert();
		return $r;
	}

	public function beforeInsert() {
		$this->activated = 1;
	}

	public function afterInsert() {
		$client = $this->getClient();
		if( $client->typeID == API\IEdepoBase::CLIENT_TYPE_COURTCLIENT ) {
			return;
		}
		// Charge client for all new users
		if( in_array( $this->typeID, [API\IEdepoBase::USER_TYPE_CLIENT, API\IEdepoBase::USER_TYPE_USER] ) ) {
			ServiceInvoiceCharges::charge( $this->clientID, API\IEdepoBase::PRICING_OPTION_SUBSCRIPTION, 1 );
			$orchPMO = new \ClickBlocks\DB\OrchestraPricingModelOptions();
			$pmo = $orchPMO->getByClientAndOption( $this->clientID, API\IEdepoBase::PRICING_OPTION_SUBSCRIPTION );
			if( $pmo['typeID'] == API\IEdepoBase::PRICING_TYPE_ONETIME ) {
				ServicePlatformLog::addLog( $client->resellerID, API\IEdepoBase::PRICING_OPTION_SUBSCRIPTION, 1, $this->clientID, $this->ID );
			}
		}
	}

	public function update() {
		$r = parent::update();
		$this->afterUpdate();
		return $r;
	}

	protected function afterUpdate() {
		if( $this->deleted ) {
			//Debug::ErrorLog( "Users::afterupdate; userID: {$this->ID}, is deleted" );
			$orcAPI = new OrchestraAPISessions();
			if( $orcAPI->userHasSession( $this->ID ) ) {
				//Debug::ErrorLog( "Users::afterupdate; userID: {$this->ID}, has API Session" );
				$orcAPI->deleteByUser( $this->ID );
				$nodeJS = new \ClickBlocks\Utils\NodeJS();
				$nodeJS->sendPostCommand( 'attendee_kick', null, ['userID' => $this->ID] );
			}
		}
	}

	protected function _initdepositionsSpeaker() {
		$this->navigators['depositionsSpeaker'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositionsSpeaker');
	}

	/**
	 * @return \ClickBlocks\DB\Clients
	 */
	public function getClient() {
		return $this->clients[0];
	}
}

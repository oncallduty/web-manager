<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
	ClickBlocks\Utils,
	ClickBlocks\API\IEdepoBase,
	ClickBlocks\Core\Register,
	ClickBlocks\Elasticsearch,
	ClickBlocks\Debug;

/**
 * @property bigint $ID
 * @property bigint $caseID
 * @property bigint $depositionID
 * @property bigint $createdBy
 * @property varchar $name
 * @property timestamp $created
 * @property timestamp $lastModified
 * @property enum $class
 * @property navigation $cases
 * @property navigation $depositions
 * @property navigation $users
 * @property navigation $files
 */
class Folders extends BLLTable {

	const DB = 'db0';
	const BASENAME = 'Folders';
	const NSCLASS = __CLASS__;

	const CLASS_COURTESYCOPY = 'CourtesyCopy';
	const CLASS_EXHIBIT = 'Exhibit';
	const CLASS_FOLDER = 'Folder';
	const CLASS_PERSONAL = 'Personal';
	const CLASS_TRANSCRIPT = 'Transcript';
	const CLASS_WITNESSANNOTATIONS = 'WitnessAnnotations';
	const CLASS_TRUSTED = 'Trusted';

	const CASE_EXHIBITS = 'Case Exhibits';
	const CASE_TRANSCRIPTS = 'Case Transcripts';

	/**
	 * @var \ClickBlocks\Utils\FileSystem
	 */
	protected $fs = null;
	protected $oldName = null;
	protected $oldCaseID = NULL;
	protected $oldDepositionID = NULL;
	protected $overwrite = false;
	protected $filesToCopy = array();
	public $exhibitHistoryMap = [];

	public function __construct( $ID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALFolders(), __CLASS__ );
		$this->class = IEdepoBase::FOLDERS_FOLDER; //default
		if( $ID ) {
			$this->assignByID( $ID );
		}
		$this->fs = new Utils\FileSystem();
	}

	public function __get( $field ) {
		// bypass navigator "files" for case folders
		if( $field === 'files' && $this->isCaseFolder() ) {
			return $this->getCaseFiles();
		}
		return parent::__get( $field );
	}

	public function __set($field, $value) {
		//if ($field == 'depositionID' && $this->isInstantiated()) throw new \LogicException('Cannot move folder to other deposition!');
		return parent::__set($field, $value);
	}

	protected function _initusers() {
		$this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
	}

	protected function _initcases() {
		$this->addNavigator( 'cases', __CLASS__ );
	}

	protected function _initdepositions() {
		$this->navigators['depositions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositions');
	}

	protected function _initfiles() {
		$this->navigators['files'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'files');
	}

	public function assign( array $data=NULL ) {
		$this->oldName = $data['name'];
		$this->oldCaseID = $data['caseID'];
		$this->oldDepositionID = $data['depositionID'];
		return parent::assign( $data );
	}

	private function setOverwrite($mode = true) {
		// TODO:
		$this->overwrite = (bool)$mode;
	}

	public function delete() {
		$fileIDs = [];
		foreach( $this->files as $file ) {
			$fileIDs[] = (int)$file->ID;
			$file->delete( FALSE );
		}
		try {
			$path = $this->getFullPath();
			Debug::ErrorLog( "Folders::delete; ID: {$this->ID}, name: '{$this->name}', path: {$path}" );
			$this->deleteFromElasticSearch( $fileIDs );
			if( $this->fs->is_dir( $path ) ) {
				$this->fs->deleteDirectories( $path );
			}
		} catch( \Exception $e ) {
			Debug::ErrorLog( [ 'Folders::delete', [ 'message' => $e->getMessage(), 'code' => $e->getCode(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ] );
			throw new Core\FileOperationException( 'Delete Folder failed: '.$e->getMessage(), NULL, $e );
		}
		return parent::delete();
	}

	public function deleteFromElasticSearch( array $fileIDs=NULL ) {
		if( !is_array( $fileIDs ) || !$fileIDs ) {
			$fileIDs = [];
			foreach( $this->files as $file ) {
				$fileIDs[] = (int)$file->ID;
				$file->delete( FALSE );
			}
		}
		if( $fileIDs ) {
			$clientID = ($this->isCaseFolder() ? $this->cases[ 0 ]->clientID : $this->depositions[ 0 ]->cases[ 0 ]->clientID);
			$es = Elasticsearch::getInstance();
			$es->deleteMultipleDocuments( $fileIDs, $clientID );
		}
	}

	public function save() {
		$this->lastModified = date( 'Y-m-d H:i:s' );
		$isUpdate = $this->isInstantiated();
		if ($isUpdate) $this->beforeUpdate(); // check only when updating record
		else $this->beforeInsert();
		$res = parent::save();
		if ($isUpdate) $this->afterUpdate();
		else $this->afterInsert();
		return $res;
	}

	public function update() {
		$this->beforeUpdate();
		return parent::update();
		$this->afterUpdate();
	}

	public function insert() {
		if (!$this->isInstantiated()) $this->beforeInsert();
		$res = parent::insert();
		$this->afterInsert();
		return $res;
	}

	public static function getClientDirPath( $clientID ) {
		$clientPath = implode( DIRECTORY_SEPARATOR, [Core\IO::dir( 'media' ), 'clients', (int)$clientID] );
		if( file_exists( $clientPath ) ) {
			return realpath( $clientPath );	//resolve symlinks and relative paths
		}
		return $clientPath;
	}

	public function getFullPath( $old=FALSE ) {
		$ds = DIRECTORY_SEPARATOR;
		$fullPath = [self::getClientDirPath( $this->users[0]->clientID )];
		if( !$this->isCaseFolder() ) {
			$oldDepo = new Depositions( $this->oldDepositionID );
			$fullPath[] = 'cases' . $ds . ($old ? $oldDepo->caseID : $this->depositions->caseID);
			$fullPath[] = 'depositions' . $ds . ($old ? $oldDepo->ID : $this->depositionID);
			$fullPath[] = 'users' . $ds . ($old ? $oldDepo->createdBy : $this->createdBy);
			$fullPath[] = ($old ? $this->oldName : $this->name);
		} else {
			$fullPath[] = 'cases' . $ds . ($old ? $this->oldCaseID : $this->caseID);
			$fullPath[] = $this->ID;
		}
		$folderPath = implode( $ds, $fullPath );
//		Debug::ErrorLog( "Folders::getFullPath: {$folderPath}" );
		if( file_exists( $folderPath ) ) {
			return realpath( $folderPath );	//resolve symlinks and relative paths
		}
		return $folderPath;
	}

	public function getClientAdmin() {
		return (new OrchestraUsers())->getClientAdminIDForUser( $this->createdBy );
	}

	protected function beforeInsert() {
		if( in_array( $this->class, [IEdepoBase::FOLDERS_EXHIBIT, IEdepoBase::FOLDERS_COURTESYCOPY, IEdepoBase::FOLDERS_TRANSCRIPT, IEdepoBase::FOLDERS_TRUSTED] ) ) {
			$clientAdminID = $this->getClientAdmin();
			if( $clientAdminID ) {
				$this->createdBy = $clientAdminID;
			}
		}
		try {
			foreach( ['name','createdBy'] as $field ) {
				if( !$this->$field ) {
					throw new \Exception( "Not all required fields are set, cannot determine path: {$field}" );
				}
			}
			if( !$this->caseID && !$this->depositionID ) {
				throw new \Exception( 'Not all required fields are set, cannot determine path; missing caseID or depositionID' );
			}
			$this->handleNameCollision();
			$this->lastModified = date( 'Y-m-d H:i:s' );
		} catch (\Exception $e) {
			throw new Core\FileOperationException( 'Create Folder Failed: ' . $this->getFullPath() . '; message: ' . $e->getMessage(), NULL, $e );
		}
	}

	protected function afterUpdate() {
		$this->copyFiles();
	}

	protected function afterInsert() {
		try {
			$this->fs->createDirectories( $this->getFullPath() );
		} catch( \Exception $e ) {
			throw new Core\FileOperationException( 'Create Folder Failed: ' . $this->getFullPath() . '; message: ' . $e->getMessage(), NULL, $e );
		}
		$this->copyFiles();
	}

	public static function getFirstUniqueName($name, array $siblings) {
		if (isset($siblings[$name])) {
			$i = 1;
			while(isset($siblings[$name.'_'.$i]) && $i<9999) $i++;
			$name .= '_'.$i;
		}
		return $name;
	}

	public function handleNameCollision() {
		$orcFolders = new OrchestraFolders();
		$siblings = $this->isCaseFolder() ? $orcFolders->getCaseFolderNameMapByCase( $this->caseID ) : $orcFolders->getFolderNameMapByDepo( $this->depositionID, $this->createdBy );
		$this->name = self::getFirstUniqueName( $this->name, $siblings );
	}

	public function copyFiles() {
		if( !is_array( $this->filesToCopy ) ) {
			$this->filesToCopy = [];
			return;
		}
		foreach( $this->filesToCopy as $file ) {
			if( is_string( $file ) && is_file( $file ) ) {
				$filename = $file;
				$file = new Files;
				$file->setSourceFile( $filename, TRUE );
				$file->createdBy = $this->createdBy;
				$file->name = basename( $file );
				$file->folderID = $this->ID;
				$file->insert();
			} elseif( is_object( $file ) && $file instanceof Files ) {
				$oldfile = $file;
				$file = $oldfile->copy();
				$file->createdBy = $this->createdBy;
				$file->folderID = $this->ID;
				$file->insert();

				//ED-631; Exhibit History
				if( $file->isExhibit ) {
					$this->exhibitHistoryMap[$oldfile->ID] = $file->ID;
				}
			}
		}
		$this->filesToCopy = [];
	}

	public function getFileCount( $userID=null ) {
		$sql = 'SELECT COUNT(ID) FROM Files WHERE folderID=?';
		$bv = [$this->ID];

		if ($userID) {
			$sql .= ' AND (createdBy=? OR isExhibit=1)';
			$bv[] = $userID;
		}

		return (int)$this->getDAL()->getDB()->col( $sql, $bv );
	}

	protected function beforeUpdate() {
		try {
			if( $this->name !== $this->oldName || $this->depositionID !== $this->oldDepositionID || $this->caseID !== $this->oldCaseID ) {
				$this->handleNameCollision();
				$curPath = $this->getFullPath();
				$oldPath = $this->getFullPath( TRUE );
				if( $curPath !== $oldPath ) {
					$dstDir = dirname( $curPath );
					if( !file_exists( $dstDir ) ) {
						$umask = umask( 0 );
						mkdir( $dstDir, 02775, TRUE );	//sticky setguid
						umask( $umask );
					}
					rename( $oldPath, $curPath );
				}
				$this->copyFiles();
			}
		} catch( \Exception $e ) {
			$this->name = $this->oldName;
			$this->caseID = $this->oldCaseID;
			$this->depositionID = $this->oldDepositionID;
			throw new Core\FileOperationException( null, null, $e );
		}
		$this->oldName = $this->name;
		$this->oldCaseID = $this->caseID;
		$this->oldDepositionID = $this->depositionID;
		$this->lastModified = date( 'Y-m-d H:i:s' );

		// too dangerous without also handling the filesystem folder path change -- disabled until further review
		//if( in_array( $this->class, ['Exhibit', 'CourtesyCopy', 'Transcript', 'Trusted'] ) ) {
		//	$clientAdminID = $this->getClientAdmin();
		//	if( $clientAdminID ) {
		//		$this->createdBy = $clientAdminID;
		//	}
		//}
	}

	/**
	 * Returns a copy of folder; you can change depositionID, createdBy, name of this folder and then you MUST insert() it
	 *  @param boolean $includeContents if true, copy files as well
	 * @return Folders a copy of folder
	 */
	public function copy($includeContents = false) {
		$copy = new Folders();
		$vals = $this->getValues();
		unset($vals['ID']);
		$copy->setValues($vals);
		if ($includeContents) {
			$files = array();
			foreach ($this->files as $file) $files[] = $file;
			$copy->setFilesToCopy($files);
		}
		return $copy;
	}

	/**
	 * this files will be copied to this folder on insert or update
	 * @param array|object $files array of DB\Files objects OR DB\Folder object
	 */
	public function setFilesToCopy($files) {
		if (!is_array($files)) {
			if (is_object($files) && $files instanceof Folders) {
				$folder = $files;
				$files = array();
				foreach ($folder->files as $file) $files[] = $file;
			} else {
				throw new \Exception('Incorrect parameter type!');
			}
		}
		$this->filesToCopy = $files;
		return $this;
	}


	public function createDemoPotentialExhibitsFolder( $depoID, $userID ) {
		$this->createFolderWithName( $this->config->logic['potentialExhibitsFolderName'], $depoID, $userID, IEdepoBase::FOLDERS_TRUSTED );
	}

	public function createDemoMiscFolder( $depoID, $userID ) {
		$this->createFolderWithName( $this->config->logic['miscellaneousFilesFolderName'], $depoID, $userID, IEdepoBase::FOLDERS_TRUSTED );
	}

	public function createDemoNotesFolder( $depoID, $userID ) {
		$this->createFolderWithName( $this->config->logic['attorneyNotesFolderName'], $depoID, $userID, IEdepoBase::FOLDERS_TRUSTED );
	}

	protected function createFolderWithName( $folderName, $depoID, $userID, $class=IEdepoBase::FOLDERS_FOLDER ) {
		$now = date( 'Y-m-d H:i:s' );
		$this->created = $now;
		$this->lastModified = $now;
		$this->createdBy = $userID;
		$this->depositionID = $depoID;
		$this->name = $folderName;
		$this->class = $class;
		$this->save();
	}

	public function getValues( $isRawData=FALSE, $userID=NULL ) {
		$values = parent::getValues( $isRawData, $userID );
		if( $userID ) {
			$orchFolders = new \ClickBlocks\DB\OrchestraFolders();
			$values['sortPos'] = $orchFolders->getSortPosition( $this->ID, $userID );
		}
		return $values;
	}

	public function isCaseFolder() {
		return ($this->caseID > 0);
	}

	protected function getCaseFiles() {
		$orcFiles = new OrchestraFiles();
		$fileList = $orcFiles->getFilesWithSortPos( $this->ID, NULL );
		$files = [];
		foreach( $fileList as $file ) {
			$files[] = new Files( $file['ID'] );
		}
		return $files;
	}

	/**
	 * Folder is in a Demo session or Demo case
	 * @return boolean
	 */
	public function isDemo() {
		$orcFolders = new OrchestraFolders();
		return $orcFolders->isDemo( $this->ID );
	}

	/**
	 * Get Exhibit Folder Name
	 * @param string $sessionClass Deposition.class
	 * @return string Exhibit Folder Name
	 */
	public static function getExhibitFolderName( $sessionClass ) {
		$reg = Register::getInstance();
		$logic = $reg->config->logic;
		switch( $sessionClass ) {
			case IEdepoBase::DEPOSITION_CLASS_WITNESSPREP:
			case IEdepoBase::DEPOSITION_CLASS_WPDEMO:
				$folderName = $logic[ 'witnessPrepExhibitFolderName' ];
				break;
			case IEdepoBase::DEPOSITION_CLASS_TRIAL:
			case IEdepoBase::DEPOSITION_CLASS_DEMOTRIAL:
			case IEdepoBase::DEPOSITION_CLASS_MEDIATION:
			case IEdepoBase::DEPOSITION_CLASS_ARBITRATION:
			case IEdepoBase::DEPOSITION_CLASS_HEARING:
				$folderName = $logic[ 'trialExhibitFolderName' ];
				break;
			case IEdepoBase::DEPOSITION_CLASS_TRIALBINDER:
				$folderName = $logic[ 'trialBinderExhibitFolderName' ];
				break;
			default:
				$folderName = $logic[ 'exhibitFolderName' ];
				break;
		}
		return $folderName;
	}

}

<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $caseID
 * @property bigint $userID
 * @property timestamp $created
 * @property navigation $cases
 * @property navigation $users
 */
class CaseManagers extends BLLTable {
	public function __construct( $caseID=NULL, $userID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALCaseManagers(), __CLASS__ );
		if( $caseID && $userID ) {
			$this->assignByID( ['caseID'=>$caseID, 'userID'=>$userID] );
		}
	}

	protected function _initusers() {
		$this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'users' );
	}

	protected function _initcases() {
		$this->navigators['cases'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'cases' );
	}
}

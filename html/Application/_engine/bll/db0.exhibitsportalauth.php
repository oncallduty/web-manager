<?php

namespace ClickBlocks\DB;

/**
 * @property int $sessionID
 * @property string $email
 * @property string $word
 * @property string $spice
 */
class ExhibitsPortalAuth extends BLLTable {

	const DB = 'db0';
	const BASENAME = 'ExhibitsPortalAuth';
	const NSCLASS = __CLASS__;

	public function __construct( $sessionID=NULL, $email=NULL ) {
		parent::__construct();
		$this->addDAL( new DALExhibitsPortalAuth(), __CLASS__ );
		if( $sessionID && $email ) {
			$this->assignByID( ['sessionID' => $sessionID, 'email' => mb_strtolower( $email, mb_detect_encoding( $email ) )] );
		}
	}

	public function __get( $f ) {
		switch( $f ) {
			case 'word':
				return '';
			case 'hash':
				$f = 'word';
				break;
		}
		return parent::__get( $f );
	}

	public function __set( $f, $v ) {
		switch( $f ) {
			case 'word':
				if( $v == '' ) {
					return FALSE;
				} else {
					if( !$this->spice ) {
						$this->spice = self::generateSpice();
					}
					$v = hash( 'SHA512', "{$v}{$this->spice}" );
				}
				break;
			case 'email':
				$v = mb_strtolower( $v, mb_detect_encoding( $v ) );	// force email to lowercase
				break;
		}
		return parent::__set( $f, $v );
	}

	public static function generateSpice() {
		return parent::generateSpice();
	}

}

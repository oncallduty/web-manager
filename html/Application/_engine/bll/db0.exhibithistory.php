<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $exhibitFileID
 * @property bigint $sourceFileID
 * @property bigint $caseID
 * @property bigint $depositionID
 * @property datetime $introducedDate
 * @property varchar $introducedBy
 * @property varchar $exhibitFilename
 * @property navigation $exhibitFile
 * @property navigation $sourceFile
 * @property navigation $case
 * @property navigation $deposition
 */
class ExhibitHistory extends BLLTable
{
	public function __construct($ID = null)
	{
		parent::__construct();
		$this->addDAL( new DALExhibitHistory(), __CLASS__ );
		if( $ID ) $this->assignByID( $ID );
	}

	protected function _initexhibitFile()
	{
		$this->navigators['exhibitFile'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'exhibitFile' );
	}

	protected function _initsourceFile()
	{
		$this->navigators['sourceFile'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'sourceFile' );
	}

	protected function _initcase()
	{
		$this->navigators['case'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'case' );
	}

	protected function _initdeposition()
	{
		$this->navigators['deposition'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'deposition' );
	}
}

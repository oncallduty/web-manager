<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property varchar $node
 * @property varchar $permission
 * @property timestamp $created
 * @property text $comment
 * @property navigation $rolePermissions
 */
class Permissions extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALPermissions(), __CLASS__);
  }

  protected function _initrolePermissions()
  {
    $this->navigators['rolePermissions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'rolePermissions');
  }
}

?>
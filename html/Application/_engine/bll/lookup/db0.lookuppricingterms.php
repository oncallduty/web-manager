<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property char $typeID
 * @property varchar $type
 * @property navigation $pricingModelOptions
 */
class LookupPricingTerms extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALLookupPricingTerms(), __CLASS__);
  }

  protected function _initpricingModelOptions()
  {
    $this->navigators['pricingModelOptions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'pricingModelOptions');
  }
}

?>
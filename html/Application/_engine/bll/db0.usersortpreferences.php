<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property enum $sortObject
 * @property enum $sortBy
 * @property enum $sortOrder
 */
class UserSortPreferences extends BLLTable {
	const SORTOBJECT_CASES = 'Cases';
	const SORTOBJECT_DEPOSITIONS = 'Depositions';
	const SORTOBJECT_FOLDERS = 'Folders';
	const SORTOBJECT_FILES = 'Files';

	const SORTBY_CASENUMBER = 'Case Number';
	const SORTBY_CUSTOM = 'Custom';
	const SORTBY_DATE = 'Date';
	const SORTBY_NAME = 'Name';
	const SORTBY_TYPE = 'Type';
	const SORTBY_WITNESS = 'Witness';

	const SORTORDER_ASC = 'Asc';
	const SORTORDER_DESC = 'Desc';

	private static $sortObjects = [
		self::SORTOBJECT_CASES,
		self::SORTOBJECT_DEPOSITIONS,
		self::SORTOBJECT_FOLDERS,
		self::SORTOBJECT_FILES
	];

	private static $sortBys = [
		self::SORTBY_CASENUMBER,
		self::SORTBY_CUSTOM,
		self::SORTBY_DATE,
		self::SORTBY_NAME,
		self::SORTBY_TYPE,
		self::SORTBY_WITNESS
	];

	private static $sortOrders = [
		self::SORTORDER_ASC,
		self::SORTORDER_DESC
	];

	public static function sortObjects() {
		return self::$sortObjects;
	}

	public static function sortBys() {
		return self::$sortBys;
	}

	public static function sortOrders() {
		return self::$sortOrders;
	}

	public function __construct( $userID=NULL, $sortObject=NULL ) {
		parent::__construct();
		$this->addDAL( new DALUserSortPreferences(), __CLASS__ );
		if( $userID && $sortObject ) {
			$this->assignByID( ['userID'=>$userID, 'sortObject'=>$sortObject] );
		}
	}

	public function save() {
		$isInsert = ($this->isInstantiated() == FALSE);
		if( $isInsert ) {
			$this->beforeInsert();
		} else {
			$this->beforeUpdate();
		}
		return parent::save();
	}

	public function insert() {
		$this->beforeInsert();
		return parent::insert();
	}

	public function update() {
		$this->beforeUpdate();
		return parent::update();
	}

	public function beforeInsert() {
		$this->validateProperties();
	}

	public function beforeUpdate() {
		$this->validateProperties();
	}

	protected function validateProperties() {
		if( !in_array( $this->sortBy, $this->sortBys() ) ) {
			switch( $this->sortObject ) {
				case self::SORTOBJECT_DEPOSITIONS:
					$this->sortBy = self::SORTBY_WITNESS;
					break;
				default:
					$this->sortBy = self::SORTBY_NAME;
					break;
			}
		}
		if( !in_array( $this->sortOrder, $this->sortOrders() ) ) {
			switch( $this->sortBy ) {
				case self::SORTBY_CUSTOM:
					$this->sortOrder = self::SORTORDER_DESC;
					break;
				default:
					$this->sortOrder = self::SORTORDER_ASC;
					break;
			}
		}
	}

	/**
	 * Store User Sort Preferences
	 * @param enum $sortBy
	 * @param enum $sortOrder
	 * @return DB\UserSortPreferences
	 */
	public function setSortPreference( $sortBy, $sortOrder ) {
		$this->sortBy = $sortBy;
		$this->sortOrder = $sortOrder;
		$this->save();
		return $this;
	}
}

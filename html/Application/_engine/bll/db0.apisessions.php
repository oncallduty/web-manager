<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property varchar $sKey
 * @property bigint $userID
 * @property bigint $attendeeID
 * @property timestamp $created
 * @property navigation $depositionAttendees
 * @property navigation $users
 */
class APISessions extends BLLTable {
	public function __construct( $ID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALAPISessions(), __CLASS__ );
		if( $ID ) {
			$this->assignByID( $ID );
		}
		if( !$this->ID ) {
			$this->generateSessionKey();
		}
	}

	protected function _initdepositionAttendees() {
		$this->navigators['depositionAttendees'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'depositionAttendees' );
	}

	protected function _initusers() {
		$this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users' );
	}

	protected function generateSessionKey() {
		$rand = mt_rand( 0, 9999999 );	//add more entropy
		$this->sKey = hash( 'sha512', microtime() . $rand . $this->config->api['secretSalt'] );
	}
}

<?php

namespace ClickBlocks\ORMTest;

use ClickBlocks\DB,
   ClickBlocks\APITest\MockWrapper;

class BLLObjectMockWrapper extends MockWrapper
{
   public function __construct($mock) {
      if (!$mock instanceof DB\BLLTable) throw new \Exception("Invalid BLL Mock Object");
      parent::__construct($mock);
   }
}

?>
<?php

namespace ClickBlocks\UnitTest\DB;

use ClickBlocks\DB,
    ClickBlocks\UnitTest,
    ClickBlocks\Utils,
    ClickBlocks\Exceptions,
    ClickBlocks\Core;

abstract class BLLObjectTestCase extends UnitTest\TestCase
{
  protected $params;
  protected $return;
  
  /**
   *
   * @var \ClickBlocks\DB\DB
   */
  protected $db;
  
  /**
   *
   * @var \ClickBlocks\Utils\FileSystem
   */
  protected $fs;
  
  public function setUp() 
  {
    parent::setUp();
    foreach (array('fs'=>'\ClickBlocks\Utils\FileSystem'/*, 'db'=>'\ClickBlocks\DB\DB'*/) as $property=>$class) {
       $this->{$property} = $this->getFullMock($class);
    }
    $this->invoked = false;
  }

  protected function getBLLTableClassName() 
  {
     $n = $this->getLogicTableName();
     if ($n) return 'ClickBlocks\DB\\'.$n;
  }
  
  protected function getLogicTableName()  {  }

   /**
    * @param mixed orchestra parameters 
    */
   protected function getThisMock(array $data = null, array $mockMethods = null)
   {
      $className = $this->getBLLTableClassName();
      $bll = self::getBLLMock($className, $mockMethods);
      if ($data) $bll->assign($data);
      return $bll;
   }
   
   public function getBLLMock($className, array $mockMethods = null)
   {
      if (!is_subclass_of($className, '\ClickBlocks\DB\BLLTable'))
        throw new \Exception('Incorrect BLLTable class name');
      $shortclass = $className;
      $k = strrpos($shortclass, '\\');
      if ($k !== FALSE) $shortclass = substr($shortclass, $k+1);//'Mock_'.substr(md5(microtime(1)),0,6).'\\'.
      $mockMethods = (array)$mockMethods + array('getRow');
      $bll = $this->getMockBuilder($className)->disableOriginalConstructor()->/*setMockClassName($shortclass)->*/setMethods($mockMethods)->getMock();
      //$bll->expects(self::any())->method('getRow')
      $bll->init();
      $bll->addDAL($this->getThisDALMock(), $className);
      $bll->attachMock($this->fs);
      $bll->mockNavigators();
      return $bll;
   }

   protected function getThisDALMock()
   {
      $bllClass = $this->getBLLTableClassName();
      $k=strrpos($bllClass, '\\');
      $dalClass = substr($bllClass,0,$k+1).'DAL'.substr($bllClass,$k+1);
      return $this->getMock($dalClass, null);
   }
  
  
}

?>
<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property bigint $depositionID
 * @property bigint $sortPos
 */
class UserCustomSortDepositions extends BLLTable {
	public function __construct( $userID=NULL, $depositionID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALUserCustomSortDepositions(), __CLASS__ );
		if( $userID && $depositionID ) {
			$this->assignByID( ['userID'=>$userID, 'depositionID'=>$depositionID] );
		}
	}
}

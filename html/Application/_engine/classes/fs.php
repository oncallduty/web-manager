<?php

namespace ClickBlocks\Utils;

use ClickBlocks\Core\IO;

/**
 * proxy class used to call all filesystem related functions.
 * method bool is_dir (string $filename) http://www.php.net/manual/en/function.is-dir.php
 * method bool is_file (string $filename) http://www.php.net/manual/en/function.is-file.php
 * method bool rename(string $oldname, string $newname, resource $context = null) http://www.php.net/manual/en/function.rename.php
 * method bool copy(string $source, string $dest, resource $context = null) http://www.php.net/manual/en/function.copy.php
 * method bool unlink(string $filename, resource $context = null) http://www.php.net/manual/en/function.unlink.php
 * method string file_get_contents(string $filename, bool $use_include_path = false, resource $context = null, int $offset = -1, int $maxlen=null) http://www.php.net/manual/en/function.file-get-contents.php
 * method string file_put_contents(string $filename, mixed $data = '', int $flags = 0, resource $context=null) http://www.php.net/manual/en/function.file-put-contents.php
 */
class FileSystem 
{
   public function __call($name, $arguments) 
   {
      //echo 'actuall call to: '.$name;
      if (!in_array($name, array(
            'basename',
            'chgrp',
            'chmod',
            'chown',
            'clearstatcache',
            'copy',
            'delete',
            'dirname',
            'disk_free_space',
            'disk_total_space',
            'diskfreespace',
            'fclose',
            'feof',
            'fflush',
            'fgetc',
            'fgetcsv',
            'fgets',
            'fgetss',
            'file_exists',
            'file_get_contents',
            'file_put_contents',
            'file',
            'fileatime',
            'filectime',
            'filegroup',
            'fileinode',
            'filemtime',
            'fileowner',
            'fileperms',
            'filesize',
            'filetype',
            'flock',
            'fnmatch',
            'fopen',
            'fpassthru',
            'fputcsv',
            'fputs',
            'fread',
            'fscanf',
            'fseek',
            'fstat',
            'ftell',
            'ftruncate',
            'fwrite',
            'glob',
            'is_dir',
            'is_executable',
            'is_file',
            'is_link',
            'is_readable',
            'is_uploaded_file',
            'is_writable',
            'is_writeable',
            'lchgrp',
            'lchown',
            'link',
            'linkinfo',
            'lstat',
            'mkdir',
            'move_uploaded_file',
            'parse_ini_file',
            'parse_ini_string',
            'pathinfo',
            'pclose',
            'popen',
            'readfile',
            'readlink',
            'realpath_cache_get',
            'realpath_cache_size',
            'realpath',
            'rename',
            'rewind',
            'rmdir',
            'set_file_buffer',
            'stat',
            'symlink',
            'tempnam',
            'tmpfile',
            'touch',
            'umask',
            'unlink',
      ))) throw new \LogicException('Invalid FileSystem function: '.$name);
      return call_user_func_array($name, $arguments);
   }
   
   public function rename2($oldname, $newname, $context = null)
   {
      return rename2($oldname, $newname, $context);
   }
   
   /*public function is_dir($filename)
   {
      return is_dir($filename);
   }
   
   public function is_file ($filename) 
   {
      return is_file($filename);
   }
   
   public function rename($oldname, $newname, $context = null)
   {
      return rename($oldname, $newname, $context);
   }
   
   public function copy($source, $dest, $context = null)
   {
      return copy($source, $dest, $context);
   }
   
   public function unlink($filename, $context = null)
   {
      return unlink($filename, $context);
   }
   
   public function file_get_contents($filename, $use_include_path = false, $context = null, $offset = -1, $maxlen=null)
   {
      return file_get_contents($filename, $use_include_path, $context, $offset, $maxlen);
   }
   
   public function file_put_contents($filename, $data = '', $flags = 0, $context=null)
   {
      return file_put_contents($filename, $data, $flags, $context);
   }*/

   /**
    * Remove folder with all it's contents
    */
   public function deleteDirectories($path)
   {
      IO::deleteDirectories($path);
   }
   
   /**
    * Creates all neccessary directories
    */
   public function createDirectories($path)
   {
      IO::createDirectories($path);
   }
}

?>

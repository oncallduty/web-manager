<?php

namespace ClickBlocks\Utils;

use ClickBlocks\Core;

/**
 * Description of wkhtmltopdf
 *
 * @author Killian
 */
class WKHTMLToPDF {
   protected $status = '';
   protected $cookies = array();
   protected $params = array(
       '--copies'=>null, //num of copies
       '-O'=>null, // orientation
       '-s'=>'A4', // pagesize
       '-g'=>null, // grayscale
       '-B'=>null, // margins... bottom
       '-L'=>null, // left
       '-R'=>null, // right
       '-T'=>null, // top
       '--title'=>null,
   );
   protected $paramsPage = [];
   protected $source;
   protected $destination;
   /**
    * @var array
    */
   protected $cfg;

   public function __construct($source, $destination = null) {
      $this->source = $source;
      $this->destination = $destination;
      $this->cfg = Core\Register::getInstance()->config->wkhtmltopdf;
   }

   protected function exec($command) {
      $output = array();
      exec($command.' 2>&1', $output, $this->status);
      return implode(PHP_EOL, $output);
   }

   /*
     private static function _pipeExec($cmd,$input=''){
         $proc=proc_open($cmd,array(0=>array('pipe','r'),1=>array('pipe','w'),2=>array('pipe','w')),$pipes);
         fwrite($pipes[0],$input);
         fclose($pipes[0]);
         $stdout=stream_get_contents($pipes[1]);
         fclose($pipes[1]);
         $stderr=stream_get_contents($pipes[2]);
         fclose($pipes[2]);
         $rtn=proc_close($proc);
         return array(
            'stdout'=>$stdout,
            'stderr'=>$stderr,
            'return'=>$rtn
          );
      }

    */

   protected function parseParams(array $params)
   {
      $command = '';
      foreach ($params as $k=>$v) {
         foreach ((array)$v as $val) {
            if ($val === array() || (is_scalar($val) && strlen($val) == 0)) continue;
            if ($val === TRUE) $command .= ' '.$k;
            else if (is_array($val) && count($val)==2) $command .= ' '.$k.' '.escapeshellarg($val[0]).' '.escapeshellarg($val[1]);
            else if (is_scalar($val)) {
               $command .= ' '.$k.' '.escapeshellarg($val);
            }
         }
      }
      return $command;
   }

   /**
    *
    * @return boolean
    * @throws \LogicException if convert failed
    */
   public function render() {
      $this->paramsPage['--cookie'] = array();
      foreach ($this->cookies as $k=>$v) {
         $this->paramsPage['--cookie'][] = array($k, $v);
      }
      //print_r($this->paramsPage);
      $command = escapeshellcmd( $this->cfg['path'] ).
              ' '. $this->parseParams($this->params).
              ' '.  escapeshellarg(urldecode($this->source)).
              ' '. $this->parseParams($this->paramsPage).
              ' '.escapeshellarg($this->destination);
       //file_put_contents($this->destination, 'mock');
      $out = $this->exec($command);
      file_put_contents(Core\IO::dir('temp').'/wkhtmltopdf.log', 'Command: '.$command.PHP_EOL.$out.PHP_EOL);
      if ($this->isError()) throw new \LogicException("wkhtmltopdf failed: ".$out);
      if (!is_file($this->destination)) throw new \LogicException("wkhtmltopdf failed: No error but file was not saved.");
      return true;
   }

   public function addParam($name, $value, $value2 = null)
   {
      $this->params[$name][] = ($value2 ? array($value,$value2) : $value);
      return $this;
   }

   public function addParamPage($name, $value, $value2 = null)
   {
      $this->paramsPage[$name][] = ($value2 ? array($value,$value2) : $value);
      return $this;
   }

	/**
	 * Sets margins for pages
	 * @param type $top
	 * @param type $right
	 * @param type $bottom
	 * @param type $left
	 * @return \ClickBlocks\Utils\WKHTMLToPDF
	 */
	public function setMargins($top, $right, $bottom, $left) {
		$this->params['-B'] = ((int)$bottom) . 'mm';
		$this->params['-L'] = ((int)$left) . 'mm';
		$this->params['-R'] = ((int)$right) . 'mm';
		$this->params['-T'] = ((int)$top) . 'mm';
		return $this;
	}

   public function setCookie($name, $value)
   {
      $this->cookies[$name] = $value;
      return $this;
   }

   public function isError() {
      return ($this->status != 0);
   }

      /**
    * Set orientation, use constants from this class.
    * By default orientation is portrait.
    * @param string $mode Use constants from this class.
    */
   public function setOrientation($mode) {
      $this->params['-O'] = $mode;
      return $this;
   }

   /**
    * Set page/paper size.
    * By default page size is A4.
    * @param string $size Formal paper size (eg; A4, letter...)
    */
   public function setPageSize($size) {
      $this->params['-s'] = $size;
      return $this;
   }

   public function setDPI($dpi)
   {
      if ($dpi) $this->params['-d'] = (int)$dpi;
      return $this;
   }

   /**
    * @param float $zoom 1 is default
    */
   public function setZoom($zoom)
   {
      if( $zoom ) {
		  $this->paramsPage['--zoom'] = (float)$zoom;
	  }
      return $this;
   }

   /**
    * Whether to automatically generate a TOC (table of contents) or not.
    * By default TOC is disabled.
    * @param boolean $enabled True use TOC, false disable TOC.
    */
   /*public function setTOC($enabled) {
      $this->toc = $enabled;
      return $this;
   }*/

   /**
    * Set the number of copies to be printed.
    * By default it is one.
    * @param integer $count Number of page copies.
    */
   public function setCopies($count) {
      $this->params['--copies'] = $count;
      return $this;
   }

   /**
    * Whether to print in grayscale or not.
    * By default it is OFF.
    * @param boolean True to print in grayscale, false in full color.
    */
   public function setGrayscale($mode = true) {
      $this->params['-g'] = (bool)$mode;
      return $this;
   }

   /**
    * Set PDF title. If empty, HTML <title> of first document is used.
    * By default it is empty.
    * @param string Title text.
    */
   public function setTitle($text) {
      $this->params['--title'] = $text;
      return $this;
   }

   protected function addHeaderOrFooter($what, $url, $spacing = -100, $showLine = false) {
      $this->params[$what.'-html'] = $url;
      if ($spacing) $this->params[$what.'-spacing'] = (int)$spacing;
      if ($showLine) $this->params[$what.'-line'] = true;
   }

   public function setHeader($url, $spacing = -100, $showLine = false) {
      $this->addHeaderOrFooter('--header', $url, $spacing, $showLine);
      return $this;
   }

   public function setFooter($url, $spacing = -100, $showLine = false) {
      $this->addHeaderOrFooter('--footer', $url, $spacing, $showLine);
      return $this;
   }

   /**
    * Set html content.
    * @param string $html New html content. It *replaces* any previous content.
    */
   /*public function setHTML($html) {
      $this->html = $html;
      file_put_contents($this->tmp, $html);
   }*/

}

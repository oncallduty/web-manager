<?php

namespace ClickBlocks;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Debug,
	ClickBlocks\PDFDaemon,
	ClickBlocks\Utils,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\API\IEdepoBase;

class Annotate {

	public function annotateFile( $case_session, $form_values, $cs_id, $user_id, $dlg )
	{
		$annotate_result = [ 'file_overwrite'=>'false' ];
		$saveFolderList = $dlg->get('saveFolderList');
		$folderKey = (int)$form_values[ "key_{$saveFolderList->uniqueID}" ];

		if( $case_session !== 'case' && $case_session !== 'session' ) {
			Debug::ErrorLog( 'Annotate::annotateFile(); ERROR: Can only annotate if in case or session view' );
		}

		$saveFolderList->value = $form_values['saveFolderList'];
		$hasErrors = FALSE;

		if( !preg_match( '/^[a-zA-Z0-9_\s-@()#!,.&+=\'-^]+$/', $form_values['saveFilename'] ) ) {
			$dlg->tpl->filenameError = 'ERROR: Invalid file name';
			$hasErrors = TRUE;
			Debug::ErrorLog( 'Annotate::annotateFile(); ERROR: Invalid file name, invalid character(s) found: ' . $form_values['saveFilename'] );
		} else {
			$dlg->tpl->filenameError = FALSE;
		}

		if( !preg_match( '/^[a-zA-Z0-9_\s-@()#!,.&+=\'-^]+$/', $form_values['saveFolderList'] ) ) {
			$dlg->tpl->folderListError = 'ERROR: Invalid folder name';
			$hasErrors = TRUE;
			Debug::ErrorLog( 'Annotate::annotateFile(); ERROR: Invalid folder name, invalid character(s) found: ' . $form_values['saveFolderList'] );
		} else {
			$dlg->tpl->folderListError = FALSE;
		}

		if( $case_session !== 'case' && Edepo::isReservedFolder( $form_values['saveFolderList'], TRUE ) ) {
			$dlg->tpl->folderListError = 'ERROR: Invalid folder name';
			$hasErrors = TRUE;
			Debug::ErrorLog( 'Annotate::annotateFile(); ERROR: Invalid folder name, name is reserved: ' . $form_values['saveFolderList'] );
		}

		if( $hasErrors ) {
			$dlg->update();
			$dlg->show();
			return;
		}

		$form_values['saveFilename'] = $form_values['saveFilename'] . '.pdf';

		$folder = new DB\Folders();
		$now = date( 'Y-m-d H:i:s' );

		if( $folderKey ) {
			$folder->assignByID( $folderKey );
			if( $folder->ID && $folder->ID == $folderKey ) {

				// Check if this is a duplicate filename.
				if( $form_values[ 'overwriteFile' ] !== 'overwrite' ) {
					$files = (new DB\OrchestraFiles())->getFilesByFolder($folder->ID);
					foreach( $files as $file ) {
						if( $file[ 'name' ] === $form_values[ 'saveFilename' ] ) {
							$annotate_result[ 'file_exists_id' ] = $file[ 'ID' ];
							$_SESSION['OVERWRITE_FILE_ID'] = $file[ 'ID' ];
							$_SESSION['OVERWRITE_FOLDER_ID'] = $folder->ID;
							Debug::ErrorLog( 'Annotate::annotateFile(); Files exists. Display overwrite dialog.' );
							return $annotate_result;
						}
					}
				}
			}
		}

		if( strcasecmp( $form_values['saveFolderList'], $folder->name ) !== 0 && $form_values[ 'overwriteFile' ] !== 'overwrite' ) {
			//create new folder
			$folder = new DB\Folders();
			$folder->created = $now;
			$folder->name = $form_values['saveFolderList'];
			$folder->class = IEdepoBase::FOLDERS_TRUSTED;
			$folder->createdBy = $user_id;
			$folder->lastModified = $now;
			$folder->caseID = $case_session == 'case' ? $cs_id : NULL;
			$folder->depositionID = $case_session == 'session' ? $cs_id : NULL;
			$folder->insert();
		} else {
			$folder->lastModified = $now;
			$folder->save();
		}

		$dlg->update();
		$dlg->show();

		$sourceFileID = (int)$form_values['sourceFileID'];
		$annotations = json_encode( json_decode( $form_values['annotations'] ), JSON_UNESCAPED_SLASHES );

		if( $form_values['overwriteFile'] !== 'overwrite' ) {
			$newFile = $this->annotateFileSave( $annotations, $sourceFileID, $form_values['saveFilename'], $folder->ID, $user_id );
			$annotate_result['fileID'] = $newFile->ID;
			$annotate_result['folderID'] = $folder->ID;
		}
		else {
			$newFile = $this->annotateFileOverwrite( $annotations, $sourceFileID, $_SESSION['OVERWRITE_FILE_ID'], $_SESSION['OVERWRITE_FOLDER_ID'] );
			$annotate_result['fileID'] = $_SESSION['OVERWRITE_FILE_ID'];
			$annotate_result['folderID'] = $_SESSION['OVERWRITE_FOLDER_ID'];
			unset( $_SESSION['OVERWRITE_FILE_ID'] );
			unset( $_SESSION['OVERWRITE_FOLDER_ID'] );
		}

		if( !$newFile ) {
			$dlg->tpl->filenameError = 'ERROR: An error occurred while saving the PDF';
			Debug::ErrorLog( 'Annotate::annotateFile(); annotateFileSave: Could not save file' );
			$dlg->update();
			$dlg->show();
			return;
		}
		$annotate_result['fileName'] = $newFile->name;
		return $annotate_result;
	}

	public function annotateFileSave( $annotations, $sourceFileID, $fileName, $folderID, $userID )
	{
		$inFile = new DB\Files( $sourceFileID );

		$fileSourceID = (new DB\OrchestraFiles())->getSourceFileID( $inFile->ID );
		$folder = new DB\Folders( $folderID );

		if( !$inFile->mimeType ) {
			$inFile->setMetadata();
		}
		$inPath = $inFile->getFullName();
		$metadataPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.json';
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';

		$jsonString = json_encode( json_decode( $annotations ), JSON_UNESCAPED_SLASHES );
		if( !$jsonString ) {
			throw new \Exception( 'Invalid annotations metadata', 501 );
		}
		$metaSuccess = file_put_contents( $metadataPath, $jsonString, 0 );
		if( !$metaSuccess ) {
			throw new \Exception( 'Unable to write annotations metadata', 501 );
		}

		if( $inFile->mimeType === 'application/pdf' ) {
			if( !PDFDaemon::annotate( $inPath, $outPath, $metadataPath ) ) {
				Debug::ErrorLog( "PDFDaemon::annotate (save); failure" );
				if( file_exists( $outPath ) ) {
					unlink( $outPath );
				}
				if( file_exists( $metadataPath ) ) {
					unlink( $metadataPath );
				}
				throw new \Exception( 'Unable to perform annotations', 501 );
			}
			Debug::ErrorLog( "PDFDaemon::annotate (save); success" );
		} else {
			Debug::ErrorLog( "Annotate::annotateFileSave; failure: invalid file type." );
			throw new \Exception( 'Invalid file type', 501 );
		}

		if( file_exists( $metadataPath ) ) {
			unlink( $metadataPath );
		}
		$now = date( 'Y-m-d H:i:s' );
		$createdBy = $userID;
		$newFile = new DB\Files();
		$newFile->folderID = $folderID;
		$newFile->createdBy = $createdBy;
		$newFile->name = $fileName;
		$newFile->created = $now;
		$newFile->sourceID = $fileSourceID;
		$newFile->setSourceFile( $outPath, FALSE );
		$newFile->insert();

		Debug::ErrorLog( "Annotate::annotateFileSave(); success" );
		return $newFile;
	}

	public function annotateFileOverwrite( $annotations, $sourceFileID, $overwriteFileID, $overwriteFolderID )
	{
		$inFile = new DB\Files( $sourceFileID );
		$fileSourceID = (new DB\OrchestraFiles())->getSourceFileID( $inFile->ID );

		$outFile = new DB\Files( $overwriteFileID );
		$outFile->folderID = $overwriteFolderID;
		if( !$outFile->ID ) {
			throw new \Exception( "File not found: $overwriteFileID", 404 );
		}

		$folder = new DB\Folders( $outFile->folderID );

		$inPath = $inFile->getFullName();
		$metadataPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.json';
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';

		$jsonString = json_encode( json_decode( $annotations ), JSON_UNESCAPED_SLASHES );
		if( !$jsonString ) {
			throw new \Exception( 'Invalid annotations metadata', 501 );
		}
		$metaSuccess = file_put_contents( $metadataPath, $jsonString, 0 );
		if( !$metaSuccess ) {
			throw new \Exception( 'Unable to write annotations metadata', 501 );
		}

		if( !PDFDaemon::annotate( $inPath, $outPath, $metadataPath ) ) {
			Debug::ErrorLog( "Annotate::annotateFileOverwrite (overwrite); failure" );
			if( file_exists( $outPath ) ) {
				unlink( $outPath );
			}
			if( file_exists( $metadataPath ) ) {
				unlink( $metadataPath );
			}
			throw new \Exception( 'Unable to perform annotations', 501 );
		}

		Debug::ErrorLog( "Annotate::annotateFileOverwrite (overwrite); success" );
		if( file_exists( $metadataPath ) ) {
			unlink( $metadataPath );
		}
		if( copy( $outPath, $outFile->getFullName() ) ) {
			unlink( $outPath );
		}
		$now = date( 'Y-m-d H:i:s' );
		$outFile->created = $now;
		$outFile->sourceID = $fileSourceID;
		$outFile->update();

		Debug::ErrorLog( "Annotate::annotateFileOverwrite(); success" );
		return $outFile;
	}
}

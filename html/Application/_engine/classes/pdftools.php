<?php

namespace ClickBlocks;

use ClickBlocks\Core,
	ClickBlocks\Debug;

class PDFTools {

	public static function flatten( $inPath, $outPath ) {
		//Debug::ErrorLog( "PDFTools::flatten;\ninPath: {$inPath}\noutPath: {$outPath}" );
		$result = PDFDaemon::flatten( $inPath, $outPath );
		//Debug::ErrorLog( "PDFTools::flatten; result: {$result}" );
		return $result;
	}

	public static function cli_flatten( $inPath, $outPath ) {
		//Debug::ErrorLog( "PDFTools::flatten;\ninPath: {$inPath}\noutPath: {$outPath}" );
		$cfg = Core\Register::getInstance()->config->fpdf_util;
		$cmd = $cfg['bin'];
		if( $cfg['flatten_args'] ) $cmd .= " {$cfg['flatten_args']} ";
		if( !is_readable( $inPath ) ) {
			return FALSE;
		}
		$cmd .= " \"{$inPath}\" \"{$outPath}\"";
		$output = [];
		$retval = -1;
		$lastLine = exec( $cmd, $output, $retval );
//		Debug::ErrorLog( "lastLine: {$lastLine}\nreturn value: {$retval}" );
//		Debug::ErrorLog( print_r( $output, TRUE ) );
		return ($retval === 0);
	}

	public static function cli_annotate( $inPath, $metadataPath, $outPath ) {
		//Debug::ErrorLog( "PDFTools::annotate;\ninPath: {$inPath}\nmetadataPath: {$metadataPath}\noutPath: {$outPath}" );
		$cfg = Core\Register::getInstance()->config->fpdf_annotate;
		$cmd = $cfg['bin'];
		if( $cfg['extra_args'] ) $cmd .= " {$cfg['extra_args']} ";
		if( !is_readable( $inPath ) || !is_readable( $metadataPath ) ) {
			return FALSE;
		}
		$cmd .= " \"{$inPath}\" \"{$metadataPath}\" \"{$outPath}\"";
		$output = [];
		$retval = -1;
		$lastLine = exec( $cmd, $output, $retval );
		//Debug::ErrorLog( "command: {$cmd}\nlastLine: {$lastLine}\nreturn value: {$retval}" );
		//Debug::ErrorLog( print_r( $output, TRUE ) );
		return ($retval === 0);
	}

	public static function annotate( $inPath, $metadataPath, $outPath ) {
		Debug::ErrorLog( "PDFTools::annotate;\ninPath: {$inPath}\nmetadataPath: {$metadataPath}\noutPath: {$outPath}" );
		$result = PDFDaemon::annotate( $inPath, $outPath, $metadataPath );
		Debug::ErrorLog( "PDFTools::annotate; result: {$result}" );
		return $result;
	}

	public static function watermark( $inPath, $outPath ) {
		//Debug::ErrorLog( "PDFTools::watermark;\ninPath: {$inPath}\noutPath: {$outPath}" );
		$result = PDFDaemon::watermark( $inPath, $outPath );
		//Debug::ErrorLog( "PDFTools::watermark; result: {$result}" );
		return $result;
	}

	public static function cli_watermark( $inPath, $outPath ) {
		//Debug::ErrorLog( "PDFTools::watermark;\ninPath: {$inPath}\noutPath: {$outPath}" );
		$cfg = Core\Register::getInstance()->config->fpdf_util;
		$cmd = $cfg['bin'];
		if( $cfg['watermark_args'] ) $cmd .= " {$cfg['watermark_args']} ";
		if( !is_readable( $inPath ) ) {
			return FALSE;
		}
		$cmd .= " \"{$inPath}\" \"{$outPath}\"";
		$output = [];
		$retval = -1;
		$lastLine = exec( $cmd, $output, $retval );
		//Debug::ErrorLog( "lastLine: {$lastLine}\nreturn value: {$retval}" );
		//Debug::ErrorLog( print_r( $output, TRUE ) );
		return ($retval === 0);
	}

}

<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraAuditLog extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\AuditLog');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\AuditLog';
   }
}

?>
<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
ClickBlocks\Cache;

class OrchestraClientMessages extends OrchestraEdepo
{
	const BLLClassName = '\ClickBlocks\DB\ClientMessages';
	const TableName = 'ClientMessages';

	public function __construct()
	{
		parent::__construct( self::BLLClassName );
	}

	public static function getBLLClassName()
	{
		return self::BLLClassName;
	}
}

?>

<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
	ClickBlocks\MVC\Backend\Backend,
	ClickBlocks\API\IEdepoBase,
	ClickBlocks\Debug;

class OrchestraUsers extends OrchestraEdepo {
	public function __construct() {
		parent::__construct( '\ClickBlocks\DB\Users' );
	}

	public static function getBLLClassName() {
		return '\ClickBlocks\DB\Users';
	}

	/**
	* getPasswordHash() is deprecated in the 1.6 release
	*/
	public static function getPasswordHash( $password ) {
		return md5( $password . Core\Register::getInstance()->config->secretSalt );
	}

	protected function arrayToSql(array $array) {
		$db = self::getDB();
		foreach( $array as $k => $v ) {
			$array[$k] = $db->quote($v);
		}
		return '(' . implode(', ', $array) . ')';
	}

	public function loginAdmin( $username, $password ) {
		$q = 'SELECT u.* FROM Users u'
			. ' INNER JOIN UserAuth a ON a.userID=u.ID'
			. ' WHERE u.deleted IS NULL AND a.expired IS NULL'
			. ' AND u.typeID IN '.$this->arrayToSql( array( Backend::USER_TYPE_SUPERADMIN, Backend::USER_TYPE_ADMIN ) )
			. ' AND u.username=:username AND a.word=SHA2(CONCAT(:password, a.spice), 512)';
//echo $q;
		return $this->db->row( $q, array( ':username' => $username, ':password' => $password ) );
	}

	/**
	 * Used to login non-admin user
	 * @param string $username
	 * @param string $password plain password typed by user
	 * @param int $resellerID
	 * @return array user data array
	 */
	public function loginReseller($username, $password, $resellerID = null) {
		$softTimeout = Core\Register::getInstance()->config->auth['softLockoutTime'];

		$q = 'SELECT u.*,
			c.deactivated as ClientDeactivated, a.ID as authID, a.lockCount,
			(a.softLock IS NOT NULL AND a.softLock > (now() - INTERVAL '.$softTimeout.' SECOND)) as isSoftLocked,
			(a.hardLock IS NOT NULL) as isHardLocked, (a.word=SHA2(CONCAT(:password, a.spice), 512)) as verified
			FROM Users u
			INNER JOIN Clients c ON (c.ID=u.clientID)
			LEFT JOIN UserAuth a ON (a.userID=u.ID)
			LEFT JOIN Clients r ON (c.typeID="C" AND r.ID=c.resellerID AND r.typeID="R")
			WHERE u.deleted IS NULL AND a.expired IS NULL AND u.username=:username
			AND r.deactivated IS NULL AND r.deleted IS NULL AND c.deactivated IS NULL AND c.deleted IS NULL
			AND u.typeID IN '.$this->arrayToSql( [Backend::USER_TYPE_RESELLER, Backend::USER_TYPE_CLIENT, Backend::USER_TYPE_USER] );
		$bindings = [':username'=>$username, ':password'=>$password];
		if( $resellerID ) {
			$q .= ' AND (IF(c.typeID="'.Backend::CLIENT_TYPE_ENT_CLIENT.'", (c.ID=:resellerID), (c.ID=:resellerID OR c.resellerID=:resellerID)))';
			$bindings[':resellerID'] = $resellerID;
		}

		$result = $this->db->row( $q.' LIMIT 1', $bindings );

		if( !$result || !$result['activated'] || $result['isHardLocked'] || $result['isSoftLocked'] ) {
			$fail = [];
			if( !$result['activated'] && $result['ID'] && $result['email'] ) {
				$fail['activated'] = FALSE;
				$fail['email'] = $result['email'];
			} elseif( $result['isSoftLocked'] ) {
				$fail['accountLocked'] = TRUE;
			} elseif( $result['isHardLocked'] ) {
				$fail['accountBanned'] = TRUE;
			}
			return $fail;
		}

		// If user does not have password, send a password reset request
		if( $result['ID'] && $result['authID'] === null ) {
			try {
				PasswordResetRequests::sendToUser( $result['ID'] );
			} catch( Exception $e ) {
				// TODO : write mailer error to log, do NOT throw exception
			}
			return ['unfederated'=>true, 'userID'=>$result['ID']];
		}

		if( $result['verified'] === '1' ) {
			if($result['lockCount'] > 0) {
				$this->db->execute( 'UPDATE UserAuth SET lockCount=0 WHERE userID=?', [$result['ID']] );
			}
			return $result;
		}

		return $this->unauthorizedLoginAttempt( $result['ID'], $result['lockCount'] );
	}

	public function loginCourtReporter( $sessionID, $email, $password ) {
		$epa = new ExhibitsPortalAuth( $sessionID, $email );
		if( !$epa || !$epa->sessionID || $epa->sessionID != $sessionID || !$epa->email || $epa->email != $email ) {
			return;
		}
		$pHash = hash( 'sha512', "{$password}{$epa->spice}" );
		$sql = 'SELECT a.* FROM ExhibitsPortalAuth a
			INNER JOIN Depositions d ON (d.ID=a.sessionID)
			WHERE a.sessionID=:sessionID AND a.word=:word AND d.deleted IS NULL';
		return $this->db->row( $sql, ['sessionID'=>(int)$sessionID, 'word'=>$pHash] );
	}

	public function loginCourtClient( $username, $password ) {
		$q = 'SELECT u.* FROM Users u
			INNER JOIN UserAuth a ON (a.userID=u.ID)
			INNER JOIN Clients c ON (c.ID=u.clientID)
			WHERE c.deleted IS NULL AND c.deactivated IS NULL AND u.deleted IS NULL AND a.expired IS NULL AND c.typeID="' . Backend::CLIENT_TYPE_COURTCLIENT . '" AND u.username=:username
			AND a.word=SHA2(CONCAT(:password, a.spice), 512)';
		return $this->db->row( $q, ['username' => $username, 'password' => $password] );
	}

	/**
	 * @param type $username
	 * @param type $password
	 * @return user array
	 * @deprecated since version 1.6
	 */
	public function loginAPI( $username, $password ) {
		$useSecureAuth = (bool)$this->db->col( 'SELECT COUNT(ua.userID) FROM Users u INNER JOIN UserAuth ua ON (u.ID=ua.userID) WHERE u.username=:username', ['username'=>$username] );
		if( $useSecureAuth ) {
			return $this->secureLoginAPI( $username, sha1( $password ) );
		}
		$types = $this->arrayToSql( array( Backend::USER_TYPE_CLIENT, Backend::USER_TYPE_USER ) );
		$bv = array( ':username'=>$username, ':password'=>self::getPasswordHash($password) );
		$sel   = 'SELECT u.*,c.deactivated as ClientDeactivated FROM Users u ';
		$join  = ' INNER JOIN Clients c ON (u.clientID=c.ID) ';
		$where = ' WHERE u.username=:username AND u.password=:password AND u.deleted IS NULL AND u.typeID IN ' . $types . ' AND c.deleted IS NULL AND c.deactivated IS NULL ';
		return $this->db->row( $sel.$join.$where.' LIMIT 1', $bv );
	}

	/**
	 * @param type $username
	 * @param type $password
	 * @return user array
	 */
	public function secureLoginAPI( $username, $password ) {
		$softTimeout = Core\Register::getInstance()->config->auth['softLockoutTime'];

		$userTypes = $this->arrayToSql( [
			Backend::USER_TYPE_CLIENT,
			Backend::USER_TYPE_USER,
			Backend::USER_TYPE_REPORTER,
			Backend::USER_TYPE_JUDGE
		] );

		$sql = 'SELECT u.*, c.deactivated as ClientDeactivated, c.typeID as clientTypeID, a.ID as authID, a.lockCount,
			(a.softLock IS NOT NULL AND a.softLock > (NOW() - INTERVAL ' . $softTimeout . ' SECOND)) as isSoftLocked,
			(a.hardLock IS NOT NULL) as isHardLocked,
			(a.word=SHA2(CONCAT(:password, a.spice), 512)) as verified
			FROM Users u
			INNER JOIN Clients c ON (c.ID=u.clientID)
			LEFT JOIN Clients r ON (r.ID=c.resellerID AND c.typeID="C")
			LEFT JOIN UserAuth a ON (a.userID=u.ID)
			WHERE r.deleted IS NULL AND r.deactivated IS NULL
			AND c.deleted IS NULL AND c.deactivated IS NULL
			AND a.expired IS NULL
			AND u.deleted IS NULL AND u.username=:username AND u.typeID IN '. $userTypes . ' AND u.activated=1
			LIMIT 1';

		$this->lastRequest = $sql;

		$result = $this->db->row( $sql, ['username'=>$username, 'password'=>$password] );

		if( empty( $result ) || $result['isHardLocked'] || $result['isSoftLocked'] ) {

			return $result['isSoftLocked'] ? ['accountLocked' => true] : ( $result['isHardLocked'] ? ['accountBanned' => true] : [] );
		}

		// If user does not have password, send a password reset request
		if( $result['ID'] && $result['authID'] === null ) {
			try {
				PasswordResetRequests::sendToUser( $result['ID'] );
			} catch( Exception $e ) {
				// TODO : write mailer error to log, do NOT throw exception
			}
			return ['unfederated'=>true, 'userID'=>$result['ID']];
		}

		if( $result['verified'] === '1' ) {
			if( $result['lockCount'] > 0 ) {
				$this->db->execute( 'UPDATE UserAuth SET lockCount=0 WHERE userID=?', [$result['ID']] );
			}
			return $result;
		}

		return $this->unauthorizedLoginAttempt( $result['ID'], $result['lockCount'] );
	}

	public function checkUniqueEmail( $value ) {
		throw new \Exception( __METHOD__ . ' is deprecated in sprint4!' );
		return !( $this->db->col( 'SELECT COUNT(*) FROM Users WHERE email = ? ', array( $value ) ) > 0 );
	}

	public function checkUniqueUsername( $value, $ignoreUserID = null ) {
		return (bool)$this->db->col( 'SELECT COUNT(*) FROM Users WHERE username = ? AND NOT ID = ?', array( $value, (int)$ignoreUserID ) ) ? false : true;
	}

	/**
	 * @param string $email
	 * @return \ClickBlocks\DB\Users
	 */
	public function getUserByEmail( $email ) {
		return $this->getObjectByQuery( array( 'email' => $email ) );
	}

	/**
	 * @param string $val
	 * @return \ClickBlocks\DB\Users
	 */
	public function getUserByUsername( $val ) {
		return $this->getObjectByQuery( array( 'username' => $val ) );
	}

	public function getUserIDByUsername( $username ) {
		$q = 'SELECT ID FROM Users WHERE username=:username AND deleted IS NULL'
			. ' AND typeID IN '.$this->arrayToSql( [Backend::USER_TYPE_RESELLER, Backend::USER_TYPE_CLIENT, Backend::USER_TYPE_USER, Backend::USER_TYPE_JUDGE, Backend::USER_TYPE_REPORTER] );
		return $this->db->col( $q, [':username'=>$username] );
	}

	/**
	 * Get the User IDs in an array by user type ID
	 * @param string $type
	 * @return array
	 */
	public function getUserIdsByType($type) {
		$result = array();
		$q = 'SELECT ID FROM `Users` WHERE `typeID`=?';
		$ids = $this->db->cols( $q, [$type] );
		return $ids;
	}

	public function getUserFirstAndLastNameByUserID( $id ) {
		$sql = 'SELECT firstName, lastName FROM Users WHERE ID=:id';
		return $this->db->row( $sql, array(id=>$id) );
	}

	public function checkEmail($email) {
		return $this->db->col( 'SELECT ID FROM Users WHERE email = ? ', array( $email ) );
	}

	public function getWidgetUsersData( $type, array $params ) {
		$whereClause = ' FROM Users u  WHERE (u.deleted is NULL OR u.deleted = \'0000-00-00 00:00:00\') AND u.typeID = \'' . $params['typeID'] . '\'';
		if(isset($params['clientID'])){
			$whereClause .= ' AND u.clientID = \''. $params['clientID'] .'\'';
			if( isset( $params['userID'] ) ) {
				$whereClause .= ' AND u.ID != \''. $params['userID'] .'\'';
			}
		}
		if ($params['searchValue'] != '') {
			$whereClause .= ' AND (u.firstName like ' . $this->db->quote('%' . $params['searchValue'] . '%') . ' OR u.lastName like ' . $this->db->quote('%' . $params['searchValue'] . '%') . ' OR u.email like ' . $this->db->quote('%' . $params['searchValue'] . '%') . ')';
		}

		switch ($type) {
		case 'count':
			return $this->db->col('SELECT COUNT(*) ' . $whereClause . $w);
		case 'rows':
			switch ( abs( $params['sortBy'] ) ) {
			case 1:
				$sortBy = 'u.ID';
				break;
			case 2:
				$sortBy = 'u.firstName';
				break;
			case 3:
				$sortBy = 'u.lastName';
				break;
			default:
				$sortBy = 'u.ID';
			}
			if( $params['sortBy'] > 0 ) {
				$sortBy .= ' DESC';
			} else {
				$sortBy .= ' ASC';
			}

			$limit = ' LIMIT ' . ( $params['pageSize'] * $params['pos'] ) . ', ' . $params['pageSize'];

			return $this->db->rows( 'SELECT u.* ' . $whereClause . ' ORDER BY ' . $sortBy . $limit );
		}
	}

	public function getUsersByClient( $clientID, $typeID ) {
		return $this->db->col( 'SELECT ID FROM Users WHERE clientID = ? AND typeID = ? ', array( $clientID, $typeID ) );
	}

	public function getAllUsersByClient( $clientID, $typeID ) {
		return $this->db->rows( 'SELECT ID FROM Users WHERE clientID = ? AND (typeID = \'C\' OR typeID = \'CU\') ', array( $clientID ) );
	}

	public function getAllUsersByClientIDDateRange( $clientID, $startDate, $endDate ) {
		$sql = 'SELECT u.ID, u.firstName, u.lastName, u.username, u.city, u.state, u.region, u.created, u.deleted FROM Users u
				WHERE u.clientID=:clientID
				AND (u.typeID=:typeClient OR u.typeID=:typeUser)
				AND (u.deleted IS NULL OR u.deleted BETWEEN :sDate AND :eDate OR u.deleted > :eDate)';
		$bind = [
			'clientID'=>$clientID,
			'sDate'=>$startDate,
			'eDate'=>$endDate,
			'typeClient'=>IEdepoBase::USER_TYPE_CLIENT,
			'typeUser'=>IEdepoBase::USER_TYPE_USER
		];
		return $this->db->rows( $sql, $bind );
	}

	public function getWidgetUserSearchData( $type, array $params ) {

		$w = ' WHERE u.deleted is NULL ';
		$ids = (array) $params['ids'];
		if( !$ids ) {
			$ids = array( 0 );
		}
		foreach( $ids as &$id ) {
			$id = (int) $id;
		}
		$ids = implode(',', $ids);
		$w .= ' AND u.ID IN (' . $ids . ')';
		switch ($type) {
		case 'count':
			return $this->db->col( 'SELECT COUNT(*) FROM Users u ' . $w );
		case 'rows':

			$sortBy = ' ORDER BY u.firstName, u.lastName';
			$sql = 'SELECT u.ID, u.firstName, u.lastName
				FROM Users u ' . $w . $sortBy;

			return $this->db->rows($sql);
		}
	}

	public function getUsersByName( $name, $exact = false, $id = 0 ) {
		$query = 'SELECT ID, CONCAT(firstName,\' \',lastName,\' (\',email,\')\') as name FROM Users';
		$where = ' WHERE ';

		if( !empty( $name ) ) {
			if( $exact ) {
				$query .= $where . ' (firstName LIKE ' . $this->db->quote( $name ) . ' OR lastName LIKE '.  $this->db->quote( $name ) . ' OR email LIKE '. $this->db->quote( $name ) . ')';
			} else {
				$query .= $where . ' (firstName LIKE ' . $this->db->quote( '%' . $name . '%' ) . ' OR lastName LIKE '.  $this->db->quote( '%' . $name . '%' ) . ' OR email LIKE '. $this->db->quote( '%' . $name . '%' ) . ')';
			}
			$where = ' AND ';
		}

		if( $id > 0 ) {
			$query .= $where . ' ID <> ' . (int) $id;
			$where = ' AND ';
		}

		$query .= $where . ' deleted IS NULL AND typeID = \'CU\'';

		return $this->db->rows($query);
	}

	public function  getUsersForDeposition( $name, $exact = false, $clientid = 0, $excludeIds = array() ) {
		$query = 'SELECT * FROM Users';
		$where = ' WHERE ';

		if( !empty( $name ) ) {
			if( $exact ) {
				$query .= $where . ' (firstName LIKE ' . $this->db->quote( $name ) . ' OR lastName LIKE '.  $this->db->quote( $name ) . ' OR email LIKE '. $this->db->quote( $name ) . ')';
			} else {
				$query .= $where . ' (firstName LIKE ' . $this->db->quote( '%' . $name . '%' ) . ' OR lastName LIKE '.  $this->db->quote( '%' . $name . '%' ) . ' OR email LIKE '. $this->db->quote( '%' . $name . '%' ) . ')';
			}
			$where = ' AND ';
		}

		if( count( $excludeIds ) > 0 ){
			$excludeIds = implode( ',', $excludeIds );
			$query .= $where . ' ID not in ( ' . $excludeIds . ' ) ';
			$where = ' AND ';
		}

		if( $clientid > 0 ) {
			$query .= $where . ' clientID = ' . (int) $clientid;
			$where = ' AND ';
		}

		$query .= $where . ' deleted IS NULL';

		return $this->db->rows( $query );
	}

	public function searchTrustedUsers( $caseID, $depoID, $name ) {
		return $this->db->rows( 'SELECT u.* FROM Users u
			LEFT JOIN DepositionAssistants da ON (da.depositionID=:depoID AND da.userID=u.ID)
			INNER JOIN Cases ca ON ca.ID=:caseID
			LEFT JOIN CaseManagers cm ON (cm.caseID=:caseID AND cm.userID=u.ID)
			WHERE ((u.typeID="C" AND ca.clientID=u.clientID) OR da.userID IS NOT NULL OR cm.userID IS NOT NULL) AND (u.firstName LIKE :search OR u.lastName LIKE :search)',
				array( 'caseID'=>$caseID, 'depoID'=>$depoID, 'search'=>'%'.$name.'%' ) );
	}

	public function getUsersBLLByClient( $clientID ) {
		return $this->getObjectsByQuery( array( 'clientID' => $clientID ) );
	}

	public function getClientAdminID( $clientID ) {
		return $this->db->col( 'SELECT u.ID FROM Users u WHERE u.clientID=:clientID AND u.typeID=:typeID', ['clientID' => (int)$clientID, 'typeID' => self::USER_TYPE_CLIENT] );
	}

	public function getClientAdminIDForUser( $userID ) {
		$sql = 'SELECT u.ID FROM Users u
				INNER JOIN Users cu ON (cu.clientID=u.clientID)
				WHERE cu.ID=:userID AND u.typeID=:typeID';
		return (int)$this->db->col( $sql, ['userID' => (int)$userID, 'typeID' => self::USER_TYPE_CLIENT] );
	}

	public function getWidgetNotificationData( $type, array $params ) {
		$t = ' FROM Users u JOIN Clients c ON c.ID=u.clientID';

		$w = ' WHERE u.deleted IS NULL AND c.deactivated IS NULL';

		if( isset( $params['typeID'] ) ) {
			$w .= ' AND u.typeID LIKE '.$this->db->quote( $params['typeID'] );
		}

		if ( $type === 'count' ) {
			return $this->db->col( "SELECT COUNT(*) $t $w" );
		} else if( $type === 'rows' ) {
			switch( abs($params['sortBy'] ) ) {
			case 1:
				$sortBy = 'u.ID';
				break;
			case 2:
				$sortBy = 'c.name';
				break;
			case 3:
				$sortBy = 'c.startDate';
				break;
			case 4:
				$sortBy = 'c.city';
				break;
			case 5:
				$sortBy = 'c.state';
				break;
			case 6:
				$sortBy = 'c.deactivated';
				break;
			}
			if( $params['sortBy'] > 0 ) {
				$sortBy .= ' DESC';
			} else {
				$sortBy .= ' ASC';
			}

			$sortBy = ' ORDER BY ' . $sortBy;
			if( $params['pageSize'] ) {
				$limit = ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];
			}
			$sql = 'SELECT c.ID as clientID, u.ID as userID, c.name, c.startDate, c.city, c.state, c.contactName, u.firstName, u.lastName, c.deactivated'
				. $t . $w . $sortBy . $limit;

			return $this->db->rows( $sql );
		}
	}

	private function unauthorizedLoginAttempt( $userID, $previousAttempts ) {
		$hardMaxAttempts = Core\Register::getInstance()->config->auth['hardLockoutMaxAttempts'];
		$softMaxAttempts = Core\Register::getInstance()->config->auth['softLockoutMaxAttempts'];

		if( $softMaxAttempts === 0 ) {
			return [];
		}

		if( $previousAttempts%10 < $softMaxAttempts ) {
			$this->db->execute( 'UPDATE UserAuth SET lockCount=? WHERE userID=?', [$previousAttempts+1, $userID] );
			return ['lockCount' => ($previousAttempts%10)+1];
		}

		if( $previousAttempts/10 >= $hardMaxAttempts ) {
			$this->db->execute( 'UPDATE UserAuth SET lockCount=0, hardLock=now() WHERE userID=?', [$userID] );
			return ['accountBanned' => true];
		}

		$this->db->execute( 'UPDATE UserAuth SET lockCount=?, softLock=now() WHERE userID=?', [($previousAttempts+(10-($previousAttempts%10))), $userID] );
		return ['accountLocked' => true];
	}

	/**
	 * Get the report data for commissions by type
	 * @param array $params
	 * @return array
	 */
	public function getCommissionReportData( array $params ) {
		// PARAMETERS
		$resellers = esd( 'resellerIDs', $params, [] );
		$users = esd( 'userIDs', $params, [] ); // empty array means all users
		$startDate = esd( 'startDate', $params, '' );
		$endDate = esd( 'endDate', $params, '' );
		$excludeDemo = esd( 'excludeDemo', $params, true );

		// ORDERING
//		$sortBy = esd( 'sortBy', $params );

		// PAGE SIZE AND POSITION
//		$pageSize = esd( 'pageSize', $params, '' );
//		$pos = intval( esd( 'pos', $params, 0 ) );

		// FIELDS
		$fields = 'us.ID, '
			. 'us.firstName, '
			. 'us.lastName, '
			. 'us.username, '
			. 'us.city, '
			. 'us.state, '
			. 'us.region, '
			. 'us.created, '
			. 'us.deleted, '
			. 'cl.name AS clientName, '
			. 'cl.ID AS clientId, '
			. 'cl.resellerID, '
			. 'de.title, '
			. 'de.started AS dateStarted, '
			. 'de.finished AS dateFinished, '
			. 'de.created AS dateCreated, '
			. 'de.depositionOf, '
			. 'de.volume, '
			. 'de.ID as depositionId, '
			. 'ca.name AS caseName, '
			. 'ca.ID as caseId, '
//			. '(SELECT COUNT(fi.ID) '
//			. '		FROM Folders fl '
//			. '		INNER JOIN Files fi ON fi.folderID = fl.ID '
//			. '		WHERE depositionID = de.ID '
//			. '			AND (fl.class = \'Exhibit\' OR fi.isExhibit) '
//			. '			AND fi.sourceUserID = us.ID) AS introducedNum, '
//			. '(SELECT COUNT(fi.ID) '
//			. '		FROM Folders fl '
//			. '		INNER JOIN Files fi ON fi.folderID = fl.ID '
//			. '		WHERE depositionID = de.ID '
//			. '			AND (fl.class = \'Exhibit\' OR fi.isExhibit) '
//			. '			AND fi.sourceUserID != us.ID) AS receivedNum, '
			. 'de.class, '
			. 'ca.class';

		// TABLES
		$tables = ' FROM `Users` us
			JOIN `Clients` cl ON cl.ID = us.clientID
			JOIN `DepositionAttendees` da ON da.userID=us.ID
			JOIN `Depositions` de ON de.ID=da.depositionID
			JOIN `Cases` ca ON ca.ID=de.caseID ';

		// WHERE USING PARAMETERS
		$where = ' WHERE us.deleted IS NULL AND cl.deactivated IS NULL AND ca.deleted IS NULL
				AND (cl.freeTrial = 0 OR (cl.freeTrial = 1 AND LAST_DAY(ADDDATE(cl.created, INTERVAL 30 DAY)) < CURDATE()))';

		if( $excludeDemo ) {
			$where .= " AND de.`class` NOT IN ('Demo','WPDemo','Demo Trial') AND ca.`class` <> 'Demo'";
		}

		if( $startDate ) {
			$startDateTimestamp = $startDate.' 00:00:00';
			$where .= " AND de.`started` >= '".$startDateTimestamp."'";
		}

		if( $endDate ) {
			$endDateTimestamp = $endDate.' 23:59:59';
			$where .= " AND de.`started` <= '".$endDateTimestamp."'";
		}

		$whereResellers = "";
		if( is_array( $resellers ) && count( $resellers ) > 0 ) {
			$resellers = array_map( array( $this->db, 'quote' ), $resellers );
			$resellerStr = implode( ',', $resellers );
			$whereResellers = "(cl.ID IN ($resellerStr) OR cl.resellerID IN ($resellerStr))";
		}

		$whereUsers = "";
		if( is_array( $users ) && count( $users ) > 0 ) {
			$users = array_map( array( $this->db,'quote' ), $users );
			$userStr = implode( ',', $users );
			$whereUsers = "us.ID IN ($userStr)";
		}

		if( $whereResellers && $whereUsers ) {
			$where .= " AND ($whereResellers OR $whereUsers)";
		}
		elseif( $whereResellers ) {
			$where .= " AND $whereResellers";
		}
		elseif( $whereUsers ) {
			$where .= " AND $whereUsers";
		}

//		if( $sortBy ) {
//			$sortBy = ' ORDER BY ' . $sortBy;
//		}

//		$limit = '';
//		if( $pageSize ) {
//			$limit = ' LIMIT ' . ( $pageSize * $pos ) . ', ' . $pageSize;
//		}

//		$sql = "SELECT $fields $tables $where $sortBy $limit";
		$sql = "SELECT $fields $tables $where GROUP BY ca.ID, us.ID";

		return $this->db->rows( $sql );
	}

	public function getWidgetCourtUsersData( $type, $params ) {
		$bind = [
			'nullDate' => '0000-00-00 00:00:00',
			'clientID' => (int)$params['clientID']
			];
		$userTypes = implode( '","', array_keys( Backend::getCourtUserTypes() ) );
		$from = 'FROM Users u';
		$where = ' WHERE (u.deleted is NULL OR u.deleted=:nullDate) AND u.typeID IN ("' . $userTypes . '") AND u.clientID=:clientID';
		if( isset( $params['userID'] ) ) {
			$where .= ' AND u.ID != \''. $params['userID'] .'\'';
		}
		if( $params['searchValue'] ) {
			$bind['search'] = '%'. $params['searchValue'] . '%';
			$where .= ' AND (u.firstName LIKE :search OR u.lastName LIKE :search OR u.email LIKE :search)';
		}

		switch( $type ) {
		case 'count':
			return $this->db->col( "SELECT COUNT(*) {$from} {$where}", $bind );
		case 'rows':
			$order = ' ORDER BY ';
			$dir = (intval( $params['sortBy'] ) > 0) ? 'DESC' : 'ASC';
			switch( abs( $params['sortBy'] ) ):
			case 1:
				$order .= "u.firstName {$dir}, u.lastName ASC, u.ID ASC";
				break;
			case 2:
				$order .= "u.lastName {$dir}, u.firstName ASC, u.ID ASC";
				break;
			case 3:
				$order .= "u.typeID {$dir}, u.lastName ASC, u.firstName ASC, u.ID ASC";
				break;
			endswitch;
			$limit = '';
			if( $params['pageSize'] ) {
				$limit = ' LIMIT ' . ( $params['pageSize'] * $params['pos'] ) . ', ' . $params['pageSize'];
			}
			$sql = "SELECT u.* {$from} {$where} {$order} {$limit}";
			return $this->db->rows( $sql, $bind );
		}
	}

	public function getAssociatedUsersReport( $clientID, $startDate, $endDate ) {
		$sql = "SELECT u.ID, u.firstName, u.lastName, u.username, u.city, u.state, u.region, u.created, u.deleted, ca.ID as caseID, ca.name as caseName, ca.number as caseNumber, ca.created as caseCreatedDate,
			ca.clientFirstName, ca.clientLastName, ca.clientPhone, ca.clientEmail, ca.clientAddress, ca.clientAddress2, ca.clientCity, ca.clientState, ca.clientRegion, ca.clientZIP, ca.clientCountryCode FROM Users u
			LEFT JOIN Cases ca ON (ca.clientID = u.clientID)
			LEFT JOIN CaseManagers cm ON (cm.caseID = ca.ID AND cm.userID = u.ID)
			LEFT JOIN Depositions d ON (d.caseID = ca.ID)
			LEFT JOIN DepositionAssistants das ON (das.depositionID = d.ID AND das.userID = u.ID)
			LEFT JOIN DepositionAttendees dat ON (dat.depositionID = d.ID AND dat.userID = u.ID)
			WHERE u.clientID=:clientID
			AND (cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID OR dat.userID=u.ID)
			AND (ca.deleted IS NULL OR ca.deleted BETWEEN :sDate AND :eDate OR ca.deleted > :eDate)
			AND (d.deleted IS NULL OR d.deleted BETWEEN :sDate AND :eDate OR d.deleted > :eDate)
			AND (u.deleted IS NULL OR u.deleted BETWEEN :sDate AND :eDate OR u.deleted > :eDate)
			AND (ca.class IS NULL OR ca.class=:caClassCase)
			AND (d.class IS NULL OR (d.class!=:dClassDemo AND d.class!=:dClassDemoWP AND d.class!=:dClassDemoTrial))
			GROUP BY ca.ID,u.ID
			ORDER BY ca.name,ca.ID,u.lastName";
		$bind = [
			'clientID'=>$clientID,
			'sDate'=>$startDate,
			'eDate'=>$endDate,
			'caClassCase'=>IEdepoBase::CASE_CLASS_CASE,
			'dClassDemo'=>IEdepoBase::DEPOSITION_CLASS_DEMO,
			'dClassDemoWP'=>IEdepoBase::DEPOSITION_CLASS_WPDEMO,
			'dClassDemoTrial'=>IEdepoBase::DEPOSITION_CLASS_DEMOTRIAL
		];
		return $this->db->rows( $sql, $bind );
	}

	public function getUnassociatedUsersReport( $clientID, $startDate, $endDate ) {
		$sql = 'SELECT u.ID, u.firstName, u.lastName, u.username, u.city, u.state, u.region, u.created, u.deleted FROM Users u
			LEFT JOIN Cases ca ON (ca.clientID=u.clientID AND ca.class=:caClassCase AND (ca.deleted IS NULL OR ca.deleted BETWEEN :sDate AND :eDate OR ca.deleted > :eDate))
			LEFT JOIN CaseManagers cm ON (cm.caseID=ca.ID AND cm.userID=u.ID)
			LEFT JOIN Depositions d ON (d.caseID=ca.ID AND (d.deleted IS NULL OR d.deleted BETWEEN :sDate AND :eDate OR d.deleted > :eDate))
			LEFT JOIN DepositionAssistants das ON (das.depositionID=d.ID AND das.userID=u.ID)
			LEFT JOIN DepositionAttendees dat ON (dat.depositionID=d.ID AND dat.userID=u.ID)
			WHERE (u.typeID=:cAdmin OR u.typeID=:cUser)
			AND (ca.deleted IS NULL OR ca.deleted BETWEEN :sDate AND :eDate OR ca.deleted > :eDate)
			AND (d.deleted IS NULL OR d.deleted BETWEEN :sDate AND :eDate OR d.deleted > :eDate)
			AND (u.deleted IS NULL OR u.deleted BETWEEN :sDate AND :eDate OR u.deleted > :eDate)
			AND (u.typeID != :cAdmin OR (u.typeID=:cAdmin AND u.created < :gfDate))
			AND u.clientID=:clientID
			GROUP BY u.ID HAVING SUM(IF(cm.userID=u.ID,1,0))+SUM(IF(d.ownerID=u.ID,1,0))+SUM(IF(das.userID=u.ID,1,0))+SUM(IF(dat.userID=u.ID,1,0)) = 0
			ORDER BY u.lastName,u.firstName,u.ID';
		$bind = [
			'clientID'=>$clientID,
			'sDate'=>$startDate,
			'eDate'=>$endDate,
			'gfDate' => $this->config->userGrandfatherDate,
			'cAdmin'=>IEdepoBase::USER_TYPE_CLIENT,
			'cUser'=>IEdepoBase::USER_TYPE_USER,
			'caClassCase'=>IEdepoBase::CASE_CLASS_CASE
		];
		return $this->db->rows( $sql, $bind );
	}

	public function getUsageReport( $params ) {
		$sql = "SELECT u.ID as userID, CONCAT(u.firstName, ' ', u.lastName) as userName, ca.ID as caseID, ca.name as caseName, ca.number as caseNumber,
			d.ID as sessionID, d.depositionOf as sessionName, d.started, d.class as sessionClass FROM Users u
			LEFT JOIN Cases ca ON (ca.clientID = u.clientID)
			LEFT JOIN Depositions d ON (d.caseID = ca.ID)
			LEFT JOIN DepositionAttendees dat ON (dat.depositionID = d.ID AND dat.userID = u.ID)
			WHERE u.clientID=:clientID
			AND (dat.userID=u.ID)
			AND (ca.deleted IS NULL OR ca.deleted BETWEEN :sDate AND :eDate OR ca.deleted > :eDate)
			AND (d.started BETWEEN :sDate AND :eDate)
			AND (u.deleted IS NULL OR u.deleted BETWEEN :sDate AND :eDate OR u.deleted > :eDate)
			AND (ca.class IS NULL OR ca.class=:caClassCase)
			AND (d.class IS NULL OR (d.class!=:dClassDemo AND d.class!=:dClassDemoWP AND d.class!=:dClassDemoTrial))
			ORDER BY d.started, u.firstName,u.lastName";
		$bind = [
			'clientID'=>$params['clientID'],
			'sDate'=>$params['startDate'],
			'eDate'=>$params['endDate'],
			'caClassCase'=>IEdepoBase::CASE_CLASS_CASE,
			'dClassDemo'=>IEdepoBase::DEPOSITION_CLASS_DEMO,
			'dClassDemoWP'=>IEdepoBase::DEPOSITION_CLASS_WPDEMO,
			'dClassDemoTrial'=>IEdepoBase::DEPOSITION_CLASS_DEMOTRIAL
		];
		return $this->db->rows( $sql, $bind );
	}

	public function getUnactivatedUser( $username ) {
		$sql = 'SELECT u.ID FROM Users u
			INNER JOIN Clients c ON (c.ID=u.clientID)
			INNER JOIN Clients r ON (r.ID=c.resellerID)
			WHERE r.deactivated IS NULL AND r.deleted IS NULL
			AND c.deactivated IS NULL AND c.deleted IS NULL
			AND u.username=:username AND u.activated=0 AND u.deleted IS NULL';
		$userID = (int)$this->db->col( $sql, ['username'=>trim( $username )] );
		return $this->getObjectByQuery( ['ID' => $userID] );
	}
}

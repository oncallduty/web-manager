<?php

namespace ClickBlocks\DB;

class OrchestraUserCustomSortCases extends OrchestraEdepo {
	public function __construct() {
		parent::__construct( '\ClickBlocks\DB\UserCustomSortCases' );
	}

	public static function getBLLClassName() {
		return '\ClickBlocks\DB\UserCustomSortCases';
	}
}

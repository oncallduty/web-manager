<?php

namespace ClickBlocks\UnitTest\DB;

use ClickBlocks\DB,
    ClickBlocks\UnitTest,
    ClickBlocks\Utils,
    ClickBlocks\Exceptions,
    ClickBlocks\Core;

abstract class OrchestraMethodTest extends UnitTest\TestCase
{
  protected $params;
  protected $return;
  
  /**
   *
   * @var \ClickBlocks\DB\DB
   */
  protected $db;
  
  /**
   *
   * @var \ClickBlocks\Utils\FileSystem
   */
  protected $fs;
  
  private $invoked = false;
   
  public function setUp() 
  {
    parent::setUp();
    foreach (array('fs'=>'\ClickBlocks\Utils\FileSystem', 'db'=>'\ClickBlocks\DB\DB') as $property=>$class) {
       $this->{$property} = $this->getFullMock($class);
    }
    $this->invoked = false;
  }

  protected function getMethodName() { }
  protected function getOrchestraClassName() { }

   /**
    * @param mixed orchestra parameters 
    */
   protected function invokeThis($params = null)
   {
      $className = $this->getOrchestraClassName();
      if (!is_subclass_of($className, '\ClickBlocks\DB\OrchestraEdepo'))
        throw new \Exception('Incorrect Orchestra class name');
      $orchestra = $this->getMockBuilder($className)->disableOriginalConstructor()->setMethods(null)->getMock();
      $method = $this->getMethodName();
      if (!$method) {
         $cls = get_class($this);
         if (stripos($cls,'Test')!==FALSE) $method = substr($cls, strrpos($cls, '\\')+1, -4);
      }
      if (!$method) throw new \Exception('Method for page '.$this->getPageClassName ().' is not defined');
      if (!is_callable(array($orchestra, $method)))
         throw new \Exception('Class '.$className.' does not have method '.$method.'()');
      foreach (array('fs'=>'\ClickBlocks\Utils\FileSystem', 'db'=>'\ClickBlocks\DB\DB') as $property=>$class) {
         $orchestra->attachMock($property, $this->{$property});
      }
      $this->return = call_user_func_array(array($orchestra, $method), func_get_args());
      return $this->return;
   }
  
  
}

?>
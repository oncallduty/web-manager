<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraPasswordResetRequests extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\PasswordResetRequests');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\PasswordResetRequests';
   }

	public function getHashByPassword( $password )
	{
		return $this->db->row( 'SELECT `hash` FROM `PasswordResetRequests` WHERE `hash`=SHA2(CONCAT(?,`salt`),512) AND `finished` IS NULL', [$password] );
	}

	public function finishExistingRequests( $userID )
	{
		return $this->db->execute( 'UPDATE PasswordResetRequests SET finished=now() WHERE userID=?', [$userID] );
	}
}

?>
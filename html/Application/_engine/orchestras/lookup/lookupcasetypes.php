<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraLookupCaseTypes extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\LookupCaseTypes');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\LookupCaseTypes';
   }

  public static function getLookup()
  {
      return self::getDB()->couples('SELECT * FROM `LookupCaseTypes`');
   }
}

?>
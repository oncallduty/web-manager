<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\UserCustomSortFiles;

class OrchestraUserCustomSortFiles extends OrchestraEdepo {
	public function __construct() {
		parent::__construct( UserCustomSortFiles::NSCLASS );
	}

	public static function getBLLClassName() {
		return UserCustomSortFiles::NSCLASS;
	}

}

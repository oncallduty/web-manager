<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraRoles extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\Roles');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\Roles';
   }
}

?>
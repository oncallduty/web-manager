<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraDepositionAssistants extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\DepositionAssistants');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\DepositionAssistants';
   }

  public function deleteByDeposition($dpID)
  {
      $this->db->execute('DELETE FROM DepositionAssistants WHERE depositionID = ?', array($dpID));
   }

  public function getByDepo($dpID)
  {
       return $this->db->rows('SELECT userID FROM DepositionAssistants WHERE depositionID = ?', array($dpID)); 
   }

	public function checkDepositionAssistant( $depositionID, $userID )
	{
		$depositionID = (int)$depositionID;
		$userID = (int)$userID;
		$sql = 'SELECT COUNT(*) FROM DepositionAssistants WHERE depositionID=:depoID AND userID=:userID';
		return (bool)$this->db->col( $sql, ['depoID'=>$depositionID, 'userID'=>$userID] );
	}
}

<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\Presentations;

class OrchestraPresentations extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct( Presentations::BLL_CLASSNAME );
	}

	public static function getBLLClassName()
	{
		return Presentations::BLL_CLASSNAME;
	}
}

<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class PopUp extends Panel
{
   public function __construct($id)
   {
      parent::__construct($id);
      $this->attributes['class'] = 'popup';
      $this->properties['shadow'] = false;
   }

   public function init()
   {
      $this->js->addTool('controls');
      $this->js->add(new Helpers\Script('popup', null, Core\IO::url('common-js') . '/popup.js'), 'link');
      $this->tpl->uniqueID = $this->attributes['uniqueID'];
      $click = 'popup.hide(\'' . $this->attributes['uniqueID'] . '\');';
      if ($this->get('btnNo')) $this->get('btnNo')->onclick = $click;
      if ($this->get('btnCancel')) $this->get('btnCancel')->onclick = $click;
      $style = &$this->attributes['style'];
      /*if (!preg_match('/display\s*\:\s*none/i', $style)) */ 
      $style = 'display:none;'.$style;
   }

	public function show( $opacity=0.5, $centre=TRUE, $fadeTime=0, $closeTime=0, $time=0, $forceClose=FALSE )
	{
		$opacity = (float)$opacity;
		$centre = (int)$centre;
		$fadeTime = (int)$fadeTime;
		$p = '';
		if( $this->ajax->isSubmit() ) {
			$p = 'parent.';
		}
		$script = $p . 'popup.show(\'' . $this->attributes['uniqueID'] . '\', ' . $opacity . ', \'' . $centre . '\', ' . $fadeTime . ');';
		$this->hide( $closeTime );
		if( Web\Ajax::isAction() ) {
			$this->ajax->script( $script, $time );
		} else {
			$this->js->add( new Helpers\Script( null, $script ), 'foot' );
		}
		$this->update();
		if( $forceClose ) {
			$this->forceClose();
		}
	}
   
   public function forceClose()
   {
   	if ($this->ajax->isSubmit()) $p = 'parent.';
   	$script = $p . 'popup.forceClose(\'' . $this->attributes['uniqueID'] . '\');';
   	if (Web\Ajax::isAction()) $this->ajax->script($script, 0);
   	else $this->js->add(new Helpers\Script(null, $script), 'foot');
   }
   
   public function hide($time = 0)
   {
      if ($this->ajax->isSubmit()) $p = 'parent.';
      $script = $p . 'popup.hide(\'' . $this->attributes['uniqueID'] . '\');';
      if (Web\Ajax::isAction()) $this->ajax->script($script, $time);
      else $this->js->add(new Helpers\Script(null, $script), 'foot');
      //$this->update();
   }

   public function JS()
   {
      parent::JS();
      $script = 'popup.initialize(\'' . $this->attributes['uniqueID'] . '\', ' . (int)$this->properties['shadow'] . ');';
      if (Web\Ajax::isAction())
      {
         if ($this->ajax->isSubmit()) $p = 'parent.';
         $this->ajax->script($script, $this->updated, true);
      }
      else $this->js->add(new Helpers\Script('popupinit_' . $this->attributes['uniqueID'], $script), 'foot');
      return $this;
   }
}

?>
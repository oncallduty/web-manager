<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
  ClickBlocks\Web\UI\Helpers;

class CustomRadioButton extends RadioButton
{
  public function JS()
  {
    $this->js->add(new Helpers\Script('cradio', null, Core\IO::url('common-js') . '/customradio.js'), 'link');
    return $this;
  }

  public function render()
  {
    if (!$this->properties['visible']) return $this->invisible();
	   $this->addClass('custom_radio');
    if ($this->properties['checked']) $this->addClass('checked');
	   else $this->removeClass('checked');
    $onclick = 'cradio.click(\'' . $this->attributes['uniqueID'] . '\', \'' . addslashes(htmlspecialchars($this->attributes['name'])) . '\');';
    if (!$this->properties['disabled'])
    {
	     $this->removeClass('disabled');
	     $this->attributes['onclick'] = str_replace($onclick, '', $this->attributes['onclick']);
		    $this->attributes['onclick'] = $onclick . $this->attributes['onclick'];
	   }
	   else 
    {
      $this->addClass('disabled');
      $this->attributes['onclick'] = str_replace($onclick, '', $this->attributes['onclick']);
    }
    if ($this->properties['caption'] != '') $label = '<label onclick="' . $this->attributes['onclick'] . '" style="' . htmlspecialchars($this->properties['styleCaption']) . '" class="' . htmlspecialchars($this->properties['classCaption']) . '">' . $this->properties['caption'] . '</label>';
    $html = '<' . $this->properties['tagContainer'] . ' id="container_' . $this->attributes['uniqueID'] . '" style="' . htmlspecialchars($this->properties['styleContainer']) . '" class="' . htmlspecialchars($this->properties['classContainer']) . '">';
    if ($this->properties['align'] == 'left') $html .= $label;
	   $UID = $this->attributes['uniqueID'];
    $this->attributes['uniqueID'] = 'block_' . $this->attributes['uniqueID'];
    $tmp = $this->attributes['name'];
    $html .= '<div' . $this->getParams() . '>';
    $this->attributes['uniqueID'] = $UID;
    $html .= '<input type="radio" runat="server" ' . ($this->properties['checked'] ? 'checked="checked"' : '') . ' style="display:none;" id="' . $this->attributes['uniqueID'] . '" value="' . htmlspecialchars($this->attributes['value']) . '" name="' . htmlspecialchars($this->attributes['name']) . '" /></div>';
    if ($this->properties['align'] == 'right') $html .= $label;
    $html .= '</' . $this->properties['tagContainer'] . '>';
    return $html;
  }
}

?>
<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class ReportDateRangeSelector extends Panel
{
   public function __construct($id)
   {
      parent::__construct($id);
      $this->attributes['class'] = 'option_block';
      $this->properties['btnCaption'] = 'Run Report';
      $this->properties['callBack'] = __CLASS__.'@'.$this->uniqueID.'->runReport';
   }
   
   public function parse(array &$attributes, Core\ITemplate $tpl)
   {
      $attributes['template'] = Core\IO::dir('backend').'/common/reportdaterange.html';
      return parent::parse($attributes, $tpl);
   }

   public function init()
   {
      $this->tpl->btnCaption = $this->properties['btnCaption'];
      $this->tpl->callBack = $this->properties['callBack'];
   }
   
   public function runReport()
    {
       $uri = new Utils\URI;
       $uri->query['startDate'] = Utils\DT::date2sql($this->get('startDate')->value, 'm/d/Y',1);
       $uri->query['endDate'] = Utils\DT::date2sql($this->get('endDate')->value, 'm/d/Y',1);
       $this->page->ajax->redirect($uri->getQueryString());
    }

   public function JS()
   {
      parent::JS();
      $this->js->add(new Helpers\Script('datetime',null,Core\IO::url('common-js').'/datetime.js'), 'link');
      return $this;
   }
}

?>
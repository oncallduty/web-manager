<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\SessionCaseFolders;

class ServiceSessionCaseFolders extends Service {

	public function __construct() {
		parent::__construct( SessionCaseFolders::NSCLASS );
	}

	protected function useCache() {
		return FALSE;
	}
}

<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\Folders;

class ServiceFolders extends Service {

	public function __construct() {
		parent::__construct( Folders::NSCLASS );
	}

	/**
	 * @param string $name
	 * @param bigint $createdBy
	 * @param bigint $depoID
	 * @param string $class
	 * @return \ClickBlocks\DB\Folders
	 */
	public static function createFolder( $name, $createdBy, $depoID, $class=Folders::CLASS_FOLDER ) {
		$folder = new Folders();
		$folder->name = $name;
		$folder->createdBy = $createdBy;
		$folder->depositionID = $depoID;
		$folder->class = $class;
		$folder->insert();
		return $folder;
	}
}

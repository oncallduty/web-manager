<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\ExhibitsPortalAuth;

class ServiceExhibitsPortalAuth extends Service {

	public function __construct() {
		parent::__construct( ExhibitsPortalAuth::NSCLASS );
	}

	protected function useCache() {
		return FALSE;
	}
}

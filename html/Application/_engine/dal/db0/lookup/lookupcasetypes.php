<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property int $typeID
 * @property varchar $type
 */
class DALLookupCaseTypes extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'LookupCaseTypes');
  }
}

?>
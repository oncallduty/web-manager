<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property varchar $actionID
 * @property varchar $action
 */
class DALLookupAuditActions extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'LookupAuditActions');
  }
}

?>
<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property int $ID
 * @property varchar $name
 * @property tinyint $isEnterprise
 */
class DALLookupPricingOptions extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'LookupPricingOptions');
  }
}

?>
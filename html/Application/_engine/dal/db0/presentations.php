<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $depositionID
 * @property bigint $fileID
 */
class DALPresentations extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'Presentations' );
	}
}

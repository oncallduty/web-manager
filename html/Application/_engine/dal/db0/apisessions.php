<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property varchar $sKey
 * @property bigint $userID
 * @property bigint $attendeeID
 * @property timestamp $created
 */
class DALAPISessions extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'APISessions');
  }
}

?>
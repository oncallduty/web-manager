<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $clientID
 * @property bigint $invoiceID
 * @property decimal $price
 * @property int $optionID
 * @property int $quantity
 * @property timestamp $created
 */ 
class DALInvoiceCharge extends \ClickBlocks\DB\DALTable 
{
  public function __construct()
  {
    parent::__construct('db0', 'InvoiceCharge');
  }
}

?>
<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property char $typeID
 * @property bigint $resellerID
 * @property varchar $name
 * @property date $startDate
 * @property timestamp $created
 * @property timestamp $deactivated
 * @property timestamp $deleted
 * @property varchar $contactName
 * @property varchar $contactPhone
 * @property varchar $contactEmail
 * @property text $description
 * @property varchar $logo
 * @property varchar $bannerColor
 * @property varchar $URL
 * @property varchar $address1
 * @property varchar $address2
 * @property varchar $city
 * @property char $state
 * @property varchar $region
 * @property varchar $ZIP
 * @property char $countryCode
 * @property int $includedAttendees
 * @property int $attendeeBundleSize
 * @property varchar $location
 * @property varchar $courtReporterEmail
 * @property varchar $liveTranscriptsEmail
 * @property varchar $salesRep
 * @property varchar $casesDemoDefault
 * @property enum $resellerLevel
 * @property enum $resellerClass
 * @property tinyint $freeTrial
 */
class DALClients extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'Clients');
  }
}

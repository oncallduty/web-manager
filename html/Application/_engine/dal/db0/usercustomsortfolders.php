<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\UserCustomSortFolders;

/**
 * @property bigint $userID
 * @property bigint $folderID
 * @property bigint $sortPos
 */
class DALUserCustomSortFolders extends DALTable {

	public function __construct() {
		parent::__construct( UserCustomSortFolders::DB, UserCustomSortFolders::BASENAME );
	}

}

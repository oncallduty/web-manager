<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $caseID
 * @property bigint $userID
 * @property timestamp $created
 */
class DALCaseManagers extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'CaseManagers');
  }
}

?>
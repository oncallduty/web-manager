<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $clientID
 * @property bigint $childClientID
 * @property bigint $invoiceID
 * @property bigint $caseID
 * @property bigint $depositionID
 * @property bigint $userID
 * @property decimal $price
 * @property tinyint $optionID
 * @property int $quantity
 * @property timestamp $created
 */
class DALInvoiceCharges extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'InvoiceCharges');
  }
}

?>
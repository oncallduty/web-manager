<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property int $version
 */
class DALDbVersion extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', '_db_version');
  }
}

?>
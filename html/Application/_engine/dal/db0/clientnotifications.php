<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $userID
 * @property string $name
 * @property string $email
 * @property bigint $clientMessageID
 * @property timestamp $createdOn
 * @property timestamp $sentOn
 */
class DALClientNotifications extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'ClientNotifications' );
	}

	public function replace()
	{
		throw new Core\FileOperationException( 'replace() on ClientNotifications is not allowed' );
	}
}

?>

<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $userID
 * @property char $word
 * @property char $spice
 * @property timestamp $updated
 * @property timestamp $expired
 * @property tinyint $lockCount
 * @property timestamp $softLock
 * @property timestamp $hardLock
 */
class DALUserAuth extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'UserAuth' );
	}
}

?>
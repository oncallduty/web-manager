<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $userID
 * @property int $roleID
 * @property timestamp $created
 */
class DALUserRoles extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'UserRoles');
  }
}

?>
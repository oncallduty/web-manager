<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property varchar $node
 * @property varchar $permission
 * @property timestamp $created
 * @property text $comment
 */
class DALPermissions extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'Permissions');
  }
}

?>
<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $folderID
 * @property bigint $createdBy
 * @property varchar $name
 * @property timestamp $created
 * @property tinyint $isExhibit
 * @property tinyint $isPrivate
 * @property bigint $sourceID
 * @property tinyint $isUpload
 * @property string $mimeType
 * @property integer $filesize
 * @property string $hash
 */
class DALFiles extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'Files');
  }

  public function replace()
  {
    throw new Core\FileOperationException('replace() on File is not allowed');
  }
}

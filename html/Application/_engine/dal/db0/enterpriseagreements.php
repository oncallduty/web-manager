<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $enterpriseResellerID
 * @property bigint $enterpriseClientID
 * @property tinyint isInactive
 * @property tinyint isDisabled
 * @property timestamp $created
 * @property timestamp $accepted
 * @property timestamp $deleted
 */

class DALEnterpriseAgreements extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'EnterpriseAgreements' );
	}
}

?>
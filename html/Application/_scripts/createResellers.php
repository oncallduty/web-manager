<?php

namespace ClickBlocks\MiscScripts;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB,
	Exception;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../connect.php' );

ob_end_flush();

class CreateResellers {

	/**
	 * @var \ClickBlocks\DB\DB
	 */
	private $db = NULL;

	/**
	 * @var \ClickBlocks\Core\Register
	 */
	private $reg = NULL;

	private $numClients = 2;
	private $numUsers = 2;
	private $numDatasources = 2;
	private $numCases = 2;
	private $numDepositions = 2;

	/**
	 * @var \ClickBlocks\DB\Clients
	 */
	private $reseller;
	/**
	 * @var \ClickBlocks\DB\Clients
	 */
	private $client;
	/**
	 * @var \ClickBlocks\DB\Users
	 */
	private $clientAdmin;
	/**
	 * @var \ClickBlocks\DB\Cases
	 */
	private $case;
	/**
	 * @var \ClickBlocks\DB\Depositions
	 */
	private $deposition;

	private $userIDs = NULL;
	private $resellerPricing = NULL;
	private $clientPricing = NULL;

	public function __construct()
	{
		$this->db = DB\ORM::getInstance()->getDB( 'db0' );
		$this->db->catchException = TRUE;
		$this->reg = Core\Register::getInstance();
		$this->init();
	}

	private function init()
	{
		global $argv;
		$numResellers = isset( $argv[1] ) ? (int)$argv[1] : 1;
		$numResellers = min( [$numResellers, 50] );
		if( isset( $argv[2] ) && (int)$argv[2] ) $this->numClients = (int)$argv[2];
		for( $r=1; $r<=$numResellers; ++$r ) {
			echo "Creating Reseller {$r}/{$numResellers}\n";
			$this->createReseller();
			echo "\n";
		}
	}

	private function createReseller()
	{
		//reseller client
		$reseller = new DB\Clients();
		$reseller->name = 'Frivilous Reseller #';
		$reseller->typeID = 'R';
		$reseller->startDate = date( 'Y-m-d H:i:s' );
		$reseller->created = $reseller->startDate;
		$reseller->deactivated = $reseller->startDate;	//create deactivated resellers
		$reseller->contactName = 'Frivilous Contact';
		$reseller->contactPhone = '1234567890';
		$reseller->contactEmail = 'frivilous.contact@edepoze.com';
		$reseller->address1 = '123 Frivilous Street Way Lane Drive';
		$reseller->city = 'Unknown City';
		$reseller->state = 'UT';
		$reseller->ZIP = '84097';
		$reseller->insert();
		$reseller->name .= $reseller->ID;
		$reseller->URL = 'fr' . $reseller->ID;
		$reseller->update();
		$this->reseller = $reseller;
		echo "-- Reseller: {$reseller->ID}\n";

		//reseller pricing options
		echo "-- Pricing Model Options: ";
		$this->resellerPricing = [];
		for( $pmo=1; $pmo<=15; ++$pmo ) {
			if( $pmo === 9 ) continue;	//skip deprecated
			$pricingModelOption = new DB\PricingModelOptions();
			$pricingModelOption->clientID = $reseller->ID;
			$pricingModelOption->optionID = $pmo;
			$pricingModelOption->price = '1.00';
			$pricingModelOption->typeID = ($pmo === 3 || $pmo === 5 || $pmo === 7) ? 'M' : 'O';
			$pricingModelOption->insert();
			$this->resellerPricing[$pmo] = $pricingModelOption->price;
			echo '.';
		}
		echo "\n";

		//reseller user
		$resellerUser = new DB\Users();
		$resellerUser->typeID = 'R';
		$resellerUser->clientID = $reseller->ID;
		$resellerUser->firstName = 'Frivilous';
		$resellerUser->lastName = 'Reseller';
		$resellerUser->email = 'frivilous.reseller@edepoze.com';
		$resellerUser->username = '_fr' . mt_rand( 100000, 999999 );
		$resellerUser->password = '123';
		$resellerUser->created = $reseller->created;
		$resellerUser->insert();
		$resellerUser->lastName = 'Reseller ' . $resellerUser->ID;
		$resellerUser->username = 'fr' . $resellerUser->ID;
		$resellerUser->update();

		DB\UserAuth::updatePassword( $resellerUser, hash( 'sha1', '123' ) );

		for( $u=1; $u<=$this->numClients; ++$u ) {
			echo "-- Creating Clients {$u}/{$this->numClients}\n";
			$this->createClient();
		}
	}

	private function createClient()
	{
		//client
		$client = new DB\Clients();
		$client->name = 'Frivilous Client #';
		$client->typeID = 'C';
		$client->resellerID = $this->reseller->ID;
		$client->startDate = date( 'Y-m-d H:i:s' );
		$client->created = $client->startDate;
		$client->contactName = 'Frivilous Contact';
		$client->contactPhone = '1234567890';
		$client->contactEmail = 'frivilous.contact@edepoze.com';
		$client->address1 = '123 Frivilous Street Way Lane Drive';
		$client->city = 'Unknown City';
		$client->state = 'UT';
		$client->ZIP = '84097';
		$client->insert();
		$client->name .= $client->ID;
		$client->URL = 'fc' . $client->ID;
		$client->update();
		echo "-- Client: {$client->ID}\n";
		$this->client = $client;

		//client pricing options
		echo "-- Pricing Model Options: ";
		$this->clientPricing = [];
		for( $pmo=1; $pmo<=15; ++$pmo ) {
			if( $pmo === 9 ) continue;	//skip deprecated
			$pricingModelOption = new DB\PricingModelOptions();
			$pricingModelOption->clientID = $client->ID;
			$pricingModelOption->optionID = $pmo;
			$pricingModelOption->price = '1.50';
			$pricingModelOption->typeID = ($pmo === 3 || $pmo === 5 || $pmo === 7) ? 'M' : 'O';
			$pricingModelOption->insert();
			$this->clientPricing[$pmo] = $pricingModelOption->price;
			echo '.';
		}
		echo "\n";

		//client admin
		$clientUser = new DB\Users();
		$clientUser->typeID = 'C';
		$clientUser->clientID = $client->ID;
		$clientUser->firstName = 'Frivilous';
		$clientUser->lastName = 'Client';
		$clientUser->email = 'frivilous.client@edepoze.com';
		$clientUser->username = '_fc' . mt_rand( 100000, 999999 );
		$clientUser->password = '123';
		$clientUser->created = $client->created;
		$clientUser->insert();
		$clientUser->lastName = 'Client ' . $clientUser->ID;
		$clientUser->username = 'fc' . $clientUser->ID;
		$clientUser->update();
		$this->clientAdmin = $clientUser;

		DB\UserAuth::updatePassword( $clientUser, hash( 'sha1', '123' ) );

		echo "-- Creating ({$this->numUsers}) Users: ";
		$this->userIDs = [];
		for( $u=0; $u<$this->numUsers; ++$u ) {
			$this->createUser();
		}
		echo "\n";
		echo "-- Creating ({$this->numDatasources}) Datasources: ";
		for( $d=0; $d<$this->numDatasources; ++$d ) {
			$this->createDatasource();
		}
		echo "\n";
		echo "-- Creating ({$this->numCases}) Cases: ";
		for( $c=0; $c<$this->numCases; ++$c ) {
			$this->createCase();
		}
		echo "\n";
		echo "-- Creating Invoices: ";
		$this->createInvoices();
		echo "\n";
	}

	private function createUser()
	{
		$user = new DB\Users();
		$user->typeID = 'CU';
		$user->clientID = $this->client->ID;
		$user->firstName = 'Frivilous';
		$user->lastName = 'User';
		$user->email = 'frivilous.user@edepoze.com';
		$user->username = '_fu' . mt_rand( 100000, 999999 );
		$user->password = '123';
		$user->created = $this->client->created;
		$user->insert();
		$user->lastName = 'User ' . $user->ID;
		$user->username = 'fu' . $user->ID;
		$user->update();

		DB\UserAuth::updatePassword( $user, hash( 'sha1', '123' ) );

		$this->userIDs[] = $user->ID;
		echo '.';
	}

	private function createDatasource()
	{
		$datasource = new DB\Datasources();
		$datasource->clientID = $this->client->ID;
		$datasource->vendor = 'Relativity';
		$datasource->name = 'Frivilous Datasource #';
		$datasource->URI = 'https://frivilous.datasources.lan/';
		$datasource->insert();
		$datasource->name .= $datasource->ID;
		$datasource->update();
		echo 'D';
		foreach( $this->userIDs as $userID ) {
			$dsUser = new DB\DatasourceUsers();
			$dsUser->userID = $userID;
			$dsUser->datasourceID = $datasource->ID;
			$dsUser->username = 'dsu_fu' . $userID;
			$dsUser->password = '123';
			$dsUser->insert();
			echo 'u';
		}
		echo ' ';
	}

	private function createCase()
	{
		try {
			$case = new DB\Cases();
			$case->clientID = $this->client->ID;
			$case->createdBy = $this->clientAdmin->ID;
			$case->name = 'Frivolous Case #';
			$case->number = '1';
			$case->created = date( 'Y-m-d H:i:s' );
			$case->clientFirstName = '';
			$case->clientLastName = '';
			$case->clientState = 'UT';
			$case->insert();
			$case->name .= $case->ID;
			$case->number = $case->ID;
			$case->update();
			echo 'C';
			$this->case = $case;



			//client charges
			$charges = new DB\InvoiceCharges();
			$charges->clientID = $this->client->ID;
			$charges->caseID = $case->ID;
			$charges->price = $this->clientPricing[4];
			$charges->optionID = 4;
			$charges->quantity = 1;
			$charges->created = $case->created;
			$charges->insert();

			//reseller charges
			$charges = new DB\InvoiceCharges();
			$charges->clientID = $this->reseller->ID;
			$charges->childClientID = $this->client->ID;
			$charges->caseID = $case->ID;
			$charges->price = $this->resellerPricing[4];
			$charges->optionID = 4;
			$charges->quantity = 1;
			$charges->created = $case->created;
			$charges->insert();
		} catch( Exception $e ) {
			echo $e->getMessage(), "\n";
		}

		for( $d=0; $d<$this->numDepositions; ++$d ) {
			$this->createDeposition();
		}
		echo ' ';
	}

	private function createDeposition()
	{
		try {
			$depo = new DB\Depositions();
			$depo->caseID = $this->case->ID;
			$depo->createdBy = $this->clientAdmin->ID;
			$depo->ownerID = reset( $this->userIDs );
			$depo->statusID = 'N';
			$depo->uKey = '_depo' . mt_rand( 100000, 999999 );
			$depo->password = '123';
			$depo->openDateTime = date( 'Y-m-d H:00:00' );
			$depo->depositionOf = 'Frivilous Deposition #';
			$depo->volume = '1';
			$depo->title = 'None';
			$depo->location = 'Here';
			$depo->attendeeLimit = 1;
			$depo->insert();
			$depo->depositionOf .= $depo->ID;
			$depo->uKey = $depo->ID;
			$depo->update();
			echo 'd';
			$this->deposition = $depo;

			//client charges
			$charges = new DB\InvoiceCharges();
			$charges->clientID = $this->client->ID;
			$charges->caseID = $case->ID;
			$charges->price = $this->clientPricing[6];
			$charges->optionID = 6;
			$charges->quantity = 1;
			$charges->created = $case->created;
			$charges->insert();

			//reseller charges
			$charges = new DB\InvoiceCharges();
			$charges->clientID = $this->reseller->ID;
			$charges->childClientID = $this->client->ID;
			$charges->caseID = $case->ID;
			$charges->price = $this->resellerPricing[6];
			$charges->optionID = 6;
			$charges->quantity = 1;
			$charges->created = $case->created;
			$charges->insert();
		} catch( Exception $e ) {
			echo $e->getMessage(), "\n";
		}
	}

	private function createInvoices()
	{
		$clientInvoice = new DB\Invoices();
		$clientInvoice->created = date( 'Y-m-d H:i:s' );
		$clientInvoice->clientID = $this->client->ID;
		$clientInvoice->startDate = date( 'Y-m-01' );
		$clientInvoice->endDate = date( 'Y-m-t' );
		$clientInvoice->insert();
		echo '.';

		$resellerInvoice = new DB\Invoices();
		$resellerInvoice->created = date( 'Y-m-d H:i:s' );
		$resellerInvoice->clientID = $this->reseller->ID;
		$resellerInvoice->startDate = date( 'Y-m-01' );
		$resellerInvoice->endDate = date( 'Y-m-t' );
		$resellerInvoice->insert();
		echo '.';
	}
}

$CR = new CreateResellers();
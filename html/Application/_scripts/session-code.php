#!/usr/bin/php
<?php

use ClickBlocks\DB,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\Debug;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../connect.php' );

date_default_timezone_set('UTC');

session_write_close();
ob_end_clean();

$depoAuth = new \ClickBlocks\DB\DepositionsAuth( 10265 );
echo $depoAuth->getPasscode();
echo "\n";


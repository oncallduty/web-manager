<?php

namespace ClickBlocks\API\v2_1_0;
const apiVersion = '2.1.0';
const codeName = 'mountrainier';

require_once(__DIR__ . '/../../connect.php');

/**
 * lowercased array of allowed controllers
 */
$ents = array('edepo','door','user','chat','deposition','clients','cases','file');

$api = new API();
$api->setEntities($ents)->execute();

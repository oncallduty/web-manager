<?php

namespace ClickBlocks\API\v1_6_0;
const apiVersion = '1.6.0';
const codeName = 'sequoia';

require_once(__DIR__ . '/../../connect.php');

/**
 * lowercased array of allowed controllers
 */
$ents = array('edepo','door','user','chat','deposition','clients','cases','file');

$api = new API();
$api->setEntities($ents)->execute();

?>

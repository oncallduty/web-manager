<?php

namespace ClickBlocks\API\v2_2_0;
const apiVersion = '2.2.0';
const codeName = 'craterlake';

require_once(__DIR__ . '/../../connect.php');

/**
 * lowercased array of allowed controllers
 */
$ents = array('edepo','door','user','chat','deposition','clients','cases','file','search');

$api = new API();
$api->setEntities($ents)->execute();

<?php

namespace ClickBlocks\UnitTest;

class InvocationMocker
{
   protected $mocker;
   
   public function __construct(\PHPUnit_Framework_MockObject_Builder_InvocationMocker $mocker) {
      //if (!method_exists($mock, 'expects')) throw new \Exception('Incorrect Mock object');
      $this->mocker = $mocker;
   }

   public function __call($name, $arguments) {
      //call_user_func_array(array($mock,''), $param_arr)
      $method = $this->mocker->method($name);
      if (count($arguments) > 0) {
        foreach ($arguments as &$arg) {
           if (is_object($arg) && ($arg instanceof \PHPUnit_Framework_Constraint)) continue;
           $arg = \PHPUnit_Framework_Assert::equalTo($arg);
        }
        call_user_func_array(array($method, 'with'), $arguments);
      }
      return $this;
   }
   
   public function result($value) {
      if (!is_object($value) && !($value instanceof \PHPUnit_Framework_MockObject_Stub)) $value = new \PHPUnit_Framework_MockObject_Stub_Return($value);
      return $this->mocker->will($value);
   }
   
   public function resultException(\Exception $exc/*, $code = null, $message = null*/) {
      //if (is_string($exc) && is_)
      return $this->mocker->will(\PHPUnit_Framework_TestCase::throwException($exc));
   }
   
   public function resultMap(array $map) {
      return $this->mocker->will(\PHPUnit_Framework_TestCase::returnValueMap($map));
   }
   
   public function resultSelf() {
      return $this->mocker->will(\PHPUnit_Framework_TestCase::returnSelf());
   }

   public function resultCall($callable) {
      if (!is_object($callable) && !($callable instanceof \PHPUnit_Framework_MockObject_Stub)) $callable = new \PHPUnit_Framework_MockObject_Stub_ReturnCallback($callable);
      return $this->mocker->will($callable);
   }
}

?>
<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
	ClickBlocks\MVC\Edepo,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetExhibits extends WidgetBackend {
	public function __construct($id, $template = null) {
		parent::__construct($id, ($template) ? : 'exhibits/exhibits.html' );
		
		$this->properties['source'] = 'ClickBlocks\DB\OrchestraExhibitHistory->getWidgetExhibitData';
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : 1;
		$this->properties['depoID'] = null;
	}

	public function init() {
		parent::init();
	}
	
	protected function getData() {
		$html = parent::getData();
		
		$sortByMap = [1=>'introducedDate', 2=>'exhibitFilename', 3=>'introducedBy'];
        $this->tpl->rows = $this->sortRows($this->properties['rows'], $sortByMap);
		
		$this->tpl->links = array(
			$this->sortLink(1, 'Date'),
            $this->sortLink(2, 'Name'),
            $this->sortLink(3, 'Introduced By'),
		);
		
		return $html;
	}
	
	public function refresh() {
        $this->init();
        $this->update();
    }

	private function getDepositionID() {
		$path = $this->getDepositionURL();
		return $this->session['/' . $path]['depositionID'];
	}

	private function getDepositionURL() {
		$path = $this->uri->getPath();
		return explode('/', $path)[1];
	}
	
	function sortRows($data, $sortByMap) {
		if (!empty($data)) {
			$sortOrder = ($this->properties['sortBy'] > 0) ? SORT_DESC : SORT_ASC;

			foreach ($data as $key => $row) {
				$primary[$key]  = $row[$sortByMap[abs($this->properties['sortBy']) ] ];
				$secondary[$key] = $row['ID'];
			}

			array_multisort($primary, $sortOrder, SORT_NATURAL | SORT_FLAG_CASE, $secondary, SORT_DESC, SORT_NATURAL, $data);
		}
		
		return $data;
	}
}

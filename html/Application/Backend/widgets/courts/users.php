<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\DB,
	ClickBlocks\MVC\Backend\Backend;

class WidgetCourtUsers extends WidgetBackend {

	/**
	 * @var \ClickBlocks\DB\Users
	 */
	protected $curUser;

	public function __construct( $id, $template=NULL ) {
		parent::__construct( $id, ($template) ? : 'courts/users.html' );
		$this->curUser = $this->page->user;
		$this->properties['source'] = 'ClickBlocks\DB\OrchestraUsers->getWidgetCourtUsersData';
		$this->properties['searchValue'] = NULL;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : -2;
		$this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
		$this->properties['clientID'] = NULL;
		$this->properties['userID'] = $this->curUser->ID;
	}

	public function init() {
		parent::init();
		$this->sort( $this->sortBy );	//fix opposite sorting from parent::init()
	}

	protected function getData() {
		$html = parent::getData();
		$this->get( 'pageSize1' )->value = $this->get( 'pageSize' )->value = $this->properties['pageSize'];
		$this->tpl->rows = $this->properties['rows'];
		$this->tpl->sortBy = $this->properties['sortBy'];
		$this->tpl->links = [
			$this->sortLink( 1, 'First Name' ),
			$this->sortLink( 2, 'Last Name' ),
			$this->sortLink( 3, 'Type' ),
		];
		$this->tpl->userTypes = Backend::getCourtUserTypes();
		return $html;
	}

	public function refresh() {
		$this->init();
		$this->update();
	}

	public function search() {
		$this->properties['searchValue'] = $this->tpl->searchValue = $this->page->get( 'searchText' )->value;
		$this->ajax->script( 'controls.focus("' . $this->get( 'searchText' )->uniqueID . '")', 100 );
	}
}

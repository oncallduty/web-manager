<?php

namespace ClickBlocks\Web\UI\POM;

class WidgetUserList extends WidgetBackend {
	/**
	* @var \ClickBlocks\DB\Users
	*/
	protected $curUser;

	public function __construct($id, $template = null) {
		parent::__construct($id, $template);
		$this->curUser = $this->page->user;
		$this->properties['source'] = 'ClickBlocks\DB\OrchestraUsers->getWidgetUsersData';
		$this->properties['searchValue'] = null;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : ( $id === 'users' ) ? 2 : 1;
		$this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
		$this->properties['clientID'] = NULL;
		$this->properties['userID'] = $this->curUser->ID;
	}

	public function search() {
		$this->properties['searchValue'] = $this->tpl->searchValue = $this->page->get( 'searchText' )->value;
		$this->ajax->script( 'controls.focus(\'' . $this->get( 'searchText' )->uniqueID . '\')', 100 );
	}

	protected function getData() {
		$html = parent::getData();
		$this->get( 'pageSize1' )->value = $this->get( 'pageSize' )->value = $this->properties['pageSize'];
		$this->tpl->rows = $this->properties['rows'];
		$this->tpl->sortBy = $this->properties['sortBy'];
		return $html;
	}

	public function refresh() {
		$this->init();
		$this->update();
	}
}

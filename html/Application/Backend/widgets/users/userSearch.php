<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Utils,
	ClickBlocks\MVC,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\Helpers;

class WidgetUserSearch extends WidgetBackend {

	public function __construct( $id, $template = null ) {
		parent::__construct( $id, ( $template ) ? : 'users/userSearch.html' );
		$this->properties['source'] = 'ClickBlocks\DB\OrchestraUsers->getWidgetUserSearchData';
		$this->properties['onRemoveCallback'] = null;
		$this->properties['searchValue'] = null;
		$this->properties['sortBy'] = -1;
		$this->properties['pageSize'] = 10;
		$this->properties['ids'] = array(0);
	}

	protected function getData() {
		$html = parent::getData();
		$this->tpl->rows = $this->properties['rows'];
		$this->tpl->sortBy = $this->properties['sortBy'];

		return $html;
	}

	public function removeUser( $userID ) {
		if( $this->properties['onRemoveCallback'] && ( ( new Core\Delegate( $this->properties['onRemoveCallback'] ) )->call( array( $userID ) ) == false ) ) {
			return;
		}
		$ids = (array) $this->properties['ids'];
		$newIds = array();
		foreach ($ids as $id) {
			if ($id != $userID) {
				array_push( $newIds, $id );
			}
		}
		$this->properties['ids'] = $newIds;
	}

}

?>

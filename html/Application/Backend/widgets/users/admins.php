<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Utils,
	ClickBlocks\MVC,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\Helpers;

class WidgetAdmins extends WidgetUserList {
	public function __construct( $id, $template = null ) {
		parent::__construct( $id, ($template) ? : 'users/list.html' );
		$this->properties['typeID'] = \ClickBlocks\MVC\Edepo::USER_TYPE_ADMIN;
	}

	protected function getData() {
		$html = parent::getData();
		$this->tpl->links = array( $this->sortLink( 1, 'ID' ),
			$this->sortLink( 2, 'Admin Name' ),
			$this->sortLink( 5, '' ) );
		$this->tpl->elink = '/admin/admin/edit?ID=';
		return $html;
	}
}

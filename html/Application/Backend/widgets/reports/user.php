<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
	ClickBlocks\MVC\Edepo,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetUser extends WidgetBackend
{
	/**
	 * @var \ClickBlocks\DB\Users
	 */
	protected $user = NULL;

	/**
	 * @var \ClickBlocks\DB\OrchestraClients
	 */
	protected $orchestraClients = NULL;

	public function __construct( $id, $template=NULL )
	{
		parent::__construct( $id, ($template) ? : 'reports/user.html' );
		$this->properties['source'] = 'ClickBlocks\DB\OrchestraPlatformLog->getWidgetUsageData';
		$this->properties['typeID'] = $this->page->user->typeID;
		$this->properties['resellerID'] = $this->page->reg->reseller['ID'];
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : -2;
		$this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
		$this->properties['month'] = isset( $this->page->pageSession['month'] ) ? $this->page->pageSession['month'] : date('m');
		$this->properties['year'] = isset( $this->page->pageSession['year'] ) ? $this->page->pageSession['year'] : date('Y');
		$this->properties['active'] = isset( $this->page->pageSession['active'] ) ? $this->page->pageSession['active'] : '';
		$this->properties['location'] = isset( $this->page->pageSession['location'] ) ? $this->page->pageSession['location'] : '';
		$this->properties['ids'] = isset( $this->page->pageSession['ids'] ) ? $this->page->pageSession['ids'] : [];
		$this->properties['hasCheckboxes'] = TRUE;
		$this->user = $this->page->user;
		$this->orchestraClients = new \ClickBlocks\DB\OrchestraClients();
		if( !isset( $this->page->pageSession['ids'] ) ) {
			$this->selectAll();
		}
	}

	public function init()
	{
		parent::init();
		$this->sort( $this->sortBy );	//fix opposite sorting from parent::init()
		$this->tpl->clientID = $this->reg->fv['clientID'];
		$this->get('status')->options = [''=>'All', '1'=>'Active', '0'=>'Inactive'];
		$this->get('status')->onchange = "widgetUser.applyFilter();";
		$this->get('status')->value = $this->properties['active'];
		$this->get('month')->options = [
			1=>'January',
			2=>'February',
			3=>'March',
			4=>'April',
			5=>'May',
			6=>'June',
			7=>'July',
			8=>'August',
			9=>'September',
			10=>'October',
			11=>'November',
			12=>'December'
		];
		$this->get('month')->value = (int)$this->properties['month'];
		$this->get('month')->onchange = "widgetUser.applyFilter();";
		$yearRange = range( 2014, date('Y') );
		$this->get('year')->options = array_combine( $yearRange, $yearRange );
		$this->get('year')->value = (int)$this->properties['year'];
		$this->get('year')->onchange = "widgetUser.applyFilter();";

		$this->tpl->isAdmin = ( in_array( $this->page->user->typeID, [self::USER_TYPE_SUPERADMIN, self::USER_TYPE_ADMIN] ) );
		if ( !$this->tpl->isAdmin ) {
			$results = $this->orchestraClients->getResellerLocations( $this->properties['resellerID'] );
			$locations = [''=>'All'];
			if( $results && is_array( $results ) && count( $results ) ) {
				foreach( $results as $row ) {
					$locations[$row['location']] = $row['location'];
				}
			}
			$this->get('location')->options = $locations;
			$this->get('location')->onchange = "widgetUser.applyFilter()";
			if( in_array( $this->properties['location'], $locations ) ) {
				$this->get('location')->value = $this->properties['location'];
			}
		}
	}

	protected function getData()
	{
		$html = parent::getData();
		$this->filterSelected();
		$this->tpl->rows = $this->properties['rows'];
		$this->tpl->sortBy = $this->properties['sortBy'];
		$this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
		$this->tpl->isAllSelected = $this->isAllSelected();
		$this->tpl->IDs = $this->properties['ids'];
		$this->tpl->links = [
			$this->sortLink( 1, 'ID' ),
			$this->sortLink( 2, 'Client Name' ),
			$this->sortLink( 3, 'Location' ),
			$this->sortLink( 4, 'City' ),
			$this->sortLink( 5, 'State' ),
			$this->sortLink( 6, 'Primary Contact' ),
			$this->sortLink( 7, 'Status' )
		];

		$this->page->tpl->count = $this->properties['count'];
		return $html;
	}

	public function applyFilter( $fv )
	{
		$this->properties['month'] = $fv['month'];
		$this->properties['year'] = $fv['year'];
		$this->properties['active'] = $fv['status'];
		$this->properties['location'] = $fv['location'];
		$this->savePageSession();
		$this->update();
	}

	protected function savePageSession()
	{
		parent::savePageSession();
		$this->page->pageSession['month'] = $this->properties['month'];
		$this->page->pageSession['year'] = $this->properties['year'];
		$this->page->pageSession['active'] = $this->properties['active'];
		$this->page->pageSession['location'] = $this->properties['location'];
		$this->page->pageSession['ids'] = $this->properties['ids'];
	}

	public function selectID( $id, $info=NULL )
	{
		parent::selectID( $id, $info );
		$this->savePageSession();
	}

	public function viewDetail( $fv )
	{
		if( $this->properties['ids'] ) {	//selected count > 0
			$url = $this->page->basePath . '/reports/user/detail';
			$sDate = $fv['month'] . '/01/' . $fv['year'];
			$sTime = strtotime( $sDate );
			$eDate = date( 'm/t/Y', $sTime );
			$eTime = strtotime( $eDate );
			$qs = '?IDs[]=' . implode( '&IDs[]=', array_values( $this->properties['ids'] ) ) . '&startDate=' . $sTime . '&endDate=' . $eTime;
			$this->ajax->redirect( "{$url}{$qs}" );
			return;
		}
		$dlg = $this->page->get( 'noticePopup' );
		$dlg->tpl->message = '<div style="text-align:center;">No clients have been selected.<br/>Please select a client to view the report.</div>';
		$dlg->get( 'btnCancel' )->value = 'OK';
		$dlg->show();
	}
}

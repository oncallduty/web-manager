<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils,
	ClickBlocks\MVC\Edepo;

class PageUserDetail extends Backend {

	private $reports = [];
	private $isAdmin = FALSE;
	private $startDate = FALSE;
	private $endDate = FALSE;

	/**
	 * @var \ClickBlocks\DB\OrchestraClients
	 */
	private $orchCL = NULL;
	/**
	 * @var \ClickBlocks\DB\OrchestraUsers
	 */
	private $orchUS = NULL;

	public function __construct()
	{
        parent::__construct( 'reports/userdetail' . ($this->isPrintMode() ? '_print' : '') . '.html' );
		$this->orchCL = $this->getOrchestraClients();
		$this->orchUS = $this->getOrchestraUsers();
		$this->isAdmin = ( in_array( $this->user->typeID, [self::USER_TYPE_SUPERADMIN, self::USER_TYPE_ADMIN] ) );
		$this->startDate = date( 'Y-m-d 00:00:00', $this->fv['startDate'] );
		$this->endDate = date( 'Y-m-d 23:59:59', $this->fv['endDate'] );
	}

	public function access()
	{
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		return in_array( $this->user->typeID, [self::USER_TYPE_SUPERADMIN, self::USER_TYPE_ADMIN, self::USER_TYPE_RESELLER] );
	}

	public function init()
	{
		parent::init();
		$this->head->name = 'Reports';
		$this->tpl->url = $this->basePath;
		$this->tpl->isAdmin = $this->isAdmin;

		$month = (int)date( 'm', $this->fv['startDate'] );
		$monthOptions = [
			1=>'January',
			2=>'February',
			3=>'March',
			4=>'April',
			5=>'May',
			6=>'June',
			7=>'July',
			8=>'August',
			9=>'September',
			10=>'October',
			11=>'November',
			12=>'December'
		];
		$year = (int)date( 'Y', $this->fv['startDate'] );
		$yearRange = range( 2014, date('Y') );

		if( !isset( $this->fv['PRINT'] ) ) {
			$this->get('month')->options = $monthOptions;
			$this->get('month')->value = $month;
			$this->get('year')->options = array_combine( $yearRange, $yearRange );
			$this->get('year')->value = $year;
		}

		$this->tpl->reportDate = $monthOptions[$month] . ' ' . $year;

		if ( isset( $this->fv['IDs'] ) && is_array( $this->fv['IDs'] ) ) {
			if ( $this->isAdmin ) {
				$resellers = [];
				foreach ( $this->fv['IDs'] as $ID ) {
					$reseller = new \ClickBlocks\DB\Clients( $ID );
					if ( !$reseller || !$reseller->ID || $reseller->ID != $ID ) {
						continue;
					}
					$resellers[] = $reseller;
				}
				usort($resellers, function($a, $b) {
					return strcasecmp($a->name, $b->name);
				});
				foreach ($resellers as $reseller ) {
					$rows = $this->orchCL->getClientByResellerIDDateRange( $reseller->ID, $this->startDate, $this->endDate );
					foreach ( $rows as $idx => $row ) {
						$client = new \ClickBlocks\DB\Clients( $row['ID'] );
						if ( !$client || !$client->ID || $client->ID != $row['ID'] ) {
							continue;
						}
						$showReseller = ( $idx === 0 ) ? TRUE : FALSE;
						$this->userReport( $client, $reseller, $showReseller );
					}
				}
			} else {
				$clients = [];
				foreach ( $this->fv['IDs'] as $ID ) {
					$client = new \ClickBlocks\DB\Clients( $ID );
					if( !$client || !$client->ID || $client->ID != $ID ) {
						continue;
					}
					$clients[] = $client;
				}
				usort($clients, function($a, $b) {
					return strcasecmp($a->name, $b->name);
				});
				foreach ( $clients as $client ) {
					$this->userReport( $client, FALSE, FALSE );
				}
			}
		}
		$reportCount = count( $this->reports );
		if( !$reportCount || $reportCount === 0 ) {
			header( "Location: {$this->basePath}/reports/user" );
		}
		$this->tpl->count = $reportCount;

		$this->tpl->reports = $this->reports;
		$this->tpl->userPricingOptions = [
			Edepo::PRICING_OPTION_UPLOAD_PER_DOC,
			Edepo::PRICING_OPTION_WITNESSPREP_ATTENDEE
		];
	}

	public function runReport( $fv )
	{
		$sDate = $fv['month'] . '/01/' . $fv['year'];
		$sTime = strtotime( $sDate );
		$eDate = date( 'm/t/Y', $sTime );
		$eTime = strtotime( $eDate );
		$url = $this->basePath . '/reports/user/detail';
		$qs = '?IDs[]=' . implode( '&IDs[]=', array_values( $this->fv['IDs'] ) ) . '&startDate=' . $sTime . '&endDate=' . $eTime;
		$this->ajax->redirect( "{$url}{$qs}" );
	}

	/**
	 * Enable page as printable for _print.html counterpart
	 * @return boolean
	 */
	public function isPrintable()
	{
		return TRUE;
	}

	private function userReport( \ClickBlocks\DB\Clients $client, $reseller, $showReseller )
	{
		$report = new \stdClass();
		$report->client = $client->getValues();
		$report->client['contactPhone'] = \ClickBlocks\Utils::formatPhone( $client->contactPhone );
		if ( $client->typeID === self::CLIENT_TYPE_ENT_CLIENT ) {
			$report->unassociatedUsers = $this->orchUS->getAllUsersByClientIDDateRange( $client->ID, $this->startDate, $this->endDate );
		} else {
			$report->unassociatedUsers = $this->orchUS->getUnassociatedUsersReport( $client->ID, $this->startDate, $this->endDate );
		}
		$rows = $this->orchUS->getAssociatedUsersReport( $client->ID, $this->startDate, $this->endDate );
		$report->cases = $this->consolidateRows($rows);
		$report->reseller = NULL;
		if ( $reseller ) {
			$report->reseller = $reseller->getValues();
			if ( $showReseller ) {
				$report->showReseller = TRUE;
				$report->resellerSummary = $this->orchCL->getBillableUserSummaryForReseller( $reseller->ID, $this->startDate, $this->endDate );
			}
		}
		$this->reports[] = $report;
	}

	public function consolidateRows( $rows ) {
		$caseTemplate = array_flip([ "caseName", "caseNumber", "caseCreatedDate", "clientFirstName", "clientLastName", "clientEmail" ]);
		$userTemplate = array_flip([ "ID", "firstName", "lastName", "username", "city", "state", "region", "created", "deleted" ]);
		$cases = [];
		foreach ( $rows as $idx => $row ) {
			if( !isset( $cases[$row[ 'caseID' ]] )) {
				$state = ( $row[ 'clientCountryCode' ] === 'OT' ) ? $row[ 'clientRegion' ] : $row[ 'clientState' ];
				$street = $row[ 'clientAddress' ] . ( $row[ 'clientAddress2' ] ) ? ' ' . $row[ 'clientAddress2' ] : '';
				$address = $street . ' ' . $row[ 'clientCity' ] . ', ' . $state . ' ' . $row[ 'clientZIP' ] . ', ' . $row[' clientCountryCode '];
				$cases[ $row[ 'caseID' ]] = array_intersect_key( $row, $caseTemplate );
				$cases[ $row[ 'caseID' ]][ 'clientFullAddress' ] = $address;
				$cases[ $row[ 'caseID' ]][ 'clientPhone' ] = Utils::formatPhone( $row['clientPhone'] );
				$cases[ $row[ 'caseID' ]][ 'users' ] = [];
			}
			$cases[ $row[ 'caseID' ]][ 'users' ][] = array_intersect_key( $row, $userTemplate );
		}
		return $cases;
	}

	public function printThisPage()
	{
		$url = $this->basePath . '/reports/user/detail';
		$qs = '?IDs[]=' . implode( '&IDs[]=', $_REQUEST['IDs'] ) . "&startDate={$_REQUEST['startDate']}&endDate={$_REQUEST['endDate']}&PRINT=1";
		$this->ajax->script( 'window.open("' . addslashes( "{$url}{$qs}" ) . '")' );
	}

	public function exportXLS()
	{
        $xls = \PHPExcel_IOFactory::load(Core\IO::dir( 'backend' ) . '/_templates/userreport.xls' );
        $sheet = $xls->setActiveSheetIndex( 0 );
        $sheet->setTitle( 'User Report' );
		$this->fillSpreadSheet( $sheet );
        $writer = new \PHPExcel_Writer_Excel5( $xls );
		$fName = 'User Report.xls';
        $fileName = Core\IO::dir( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID();
        $writer->save( $fileName );
        $this->ajax->downloadFile( $fileName, $fName, 'application/vnd.ms-excel' );
    }

	private function fillSpreadSheet( \PHPExcel_Worksheet $sheet )
	{
		$colHeaders = ['User', 'Username', 'City', 'State/Region', 'Created', 'Deleted', 'Case', 'Case/Matter #', 'Firm', 'Date', 'Contact', 'Contact Phone', 'Contact Email',
			'Contact Address', 'Contact Address2', 'Contact City', 'Contact State/Region', 'Contact Zip', 'Contact Country'];
		$rowI = 1;
		$colI = 0;
		$date = $this->get('month')->options[$this->get('month')->value] . ' ' . $this->get('year')->value;

		foreach ( $colHeaders as $i => $header )
		{
			$sheet->getCellByColumnAndRow( $i, $rowI )->setValue( $header );
		}

		++$rowI;

		foreach ( $this->tpl->reports as $report )
		{
			foreach ( $report->unassociatedUsers as $uaUser ) {
				$userFullName = $uaUser['lastName'] . ', ' . $uaUser['firstName'];
				$userStateRegion = ( $uaUser['state'] ) ? $uaUser['state'] : $uaUser['region'];
				$created = date( 'm/d/Y', strtotime( $uaUser['created'] ) );
				$deleted = ( $uaUser['deleted'] ) ? date( 'm/d/Y', strtotime( $uaUser['deleted'] ) ) : '' ;
				$userInfo = [$userFullName, $uaUser['username'], $uaUser['city'], $userStateRegion, $created, $deleted];
				$caseInfo = ['', '', $report->client['name'], $date];

				foreach ( $userInfo as $uInfo ) {
					$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $uInfo );
					++$colI;
				}
				foreach ( $caseInfo as $caInfo ) {
					$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $caInfo );
					++$colI;
				}

				$colI = 0;
				++$rowI;
			}

			foreach ( $report->cases as $case ) {
				$contact = $case['clientFirstName'] . ' ' . $case['clientLastName'];
				$stateRegion = ( $case['clientState'] ) ? $case['clientState'] : $case['clientRegion'];
				$caseInfo = [$case['caseName'], $case['caseNumber'], $report->client['name'], $date, $contact, $case['clientPhone'], $case['clientEmail'], $case['clientAddress'], $case['clientAddress2'],
						$case['clientCity'], $stateRegion, $case['clientZIP'], $case['clientCountryCode']];

				foreach ( $case['users'] as $user ) {
					$userFullName = $user['lastName'] . ', ' . $user['firstName'];
					$userStateRegion = ( $user['state'] ) ? $user['state'] : $user['region'];
					$created = date( 'm/d/Y', strtotime( $user['created'] ) );
					$deleted = ( $user['deleted'] ) ? date( 'm/d/Y', strtotime( $user['deleted'] ) ) : '' ;
					$userInfo = [$userFullName, $user['username'], $user['city'], $userStateRegion, $created, $deleted];

					foreach ( $userInfo as $uInfo ) {
						$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $uInfo );
						++$colI;
					}
					foreach ( $caseInfo as $caInfo ) {
						$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $caInfo );
						++$colI;
					}

					$colI = 0;
					++$rowI;
				}
			}
		}
	}
}
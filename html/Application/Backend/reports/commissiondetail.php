<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

// COMMISSION REPORT
// JIRA: ED-2686
// 2. The report is a list of users and what sessions they attended 
//		-excluding demos (see attachment WebMgr-CommisionReports-2.png)
//	a.  Display the following fields:
//		-Date of the session (deposition.started)
//		-Reseller name (reseller.name)
//		-City associated with the user (user.city)
//		-State associated with the user (user.state)
//		-User name (user.firstName, user.LastName)
//		-Client associated with the user (client.name)
//		-Name of the case associated with the session (case.name)
//		-Name of the session (deposition.title)
//		-Number of exhibits the user introduced (sub-select)
//		-Wholesale price for the exhibits introduced (php-calculated)
//		-Number of exhibits the user received (sub-select)
//		-Wholesale price of the exhibits received (php calculated)
//		-The line total of wholesale prices (php-calculated)
//	b.  The list has a primary sort by state and secondary sort by city (user.state, user.city)
//	c.  keep a breadcrumb allowing the user to return to the previous page 
//	d.  provide the ability for the user to print the report
//	e.  allow the user to export the report in .xls format



class PageCommissionDetail extends Backend {

	protected $baseURL = '/';
	protected $reports = [];
	protected $pricing = [];

	/**
	 * @var \ClickBlocks\DB\OrchestraUsers
	 */
	protected $orchUsers = NULL;

	/**
	 * @var \ClickBlocks\DB\OrchestraPricingModelOptions
	 */
	protected $orchPMO = NULL;

	public function __construct()
	{
		$isPrint = $this->isPrintMode();
        parent::__construct( 'reports/commissiondetail' . ($isPrint ? '_print' : '') . '.html' );
	}

	public function access()
	{
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath.'/';
		return in_array($this->user->typeID, [self::USER_TYPE_SUPERADMIN,self::USER_TYPE_ADMIN]);
	}

	public function init()
	{
		parent::init();
		
		$this->head->name = 'Commission Report';
		$this->tpl->headName = 'Commission Report';
		$this->tpl->isAdmin = true;
		
		$this->orchUsers = new \ClickBlocks\DB\OrchestraUsers();
		$this->orchPMO = new \ClickBlocks\DB\OrchestraPricingModelOptions();
		
		$userIDs = esd('uID',$this->fv,[]); // explicit users to include
		$resellerIDs = esd('cID',$this->fv,[]); // resellers to include
		$date = date_create(esd('startDate',$this->fv));
		$startDate = date_format($date,'Y-m-d'); // start date
		$endDate = date_format(date_create(esd('endDate',$this->fv)),'Y-m-d'); // end date
		
		$this->tpl->reportDate = $date->format('F Y');
		
		$statesSelect = self::getUSAStates(false);
		
		$sortBy = esd('sortBy',$this->fv,'us.state, us.city');
		
		$params = [ 'userIDs'=>$userIDs,
					'resellerIDs'=>$resellerIDs,
					'startDate'=>$startDate,
					'endDate'=>$endDate,
					'excludeDemo'=>true,
					'sortBy'=>$sortBy];
		
		// use the resellers and start and end date to get the report data
		$rows = $this->orchUsers->getCommissionReportData($params);
		
		$ResellerNames = [];
		if(is_array($rows)) {
			foreach($rows as $row) {
				$resellerID = $row['resellerID'];
				if($resellerID == '') {
					$resellerID = $row['clientId'];
				}
				if(!isset($ResellerNames[$resellerID])) {
					$ResellerNames[$resellerID] = (new DB\Clients($resellerID))->name;
				}
				
				$row['resellerName'] = $ResellerNames[$resellerID];
				$row['userFullName'] = $row['firstName'] . ' ' . $row['lastName'];
				$row['state'] = $statesSelect[$row['state']];

				$reports[] = $row;
			}
		}
		
		$this->tpl->count = count( $reports );
		$this->tpl->reports = $this->reports = $this->sortRows( $reports );
		$this->tpl->printPerPage = 25;
	}
	
	function sortRows( $data ) {
		if ( !empty($data) ) {
			foreach ( $data as $key => $row ) {
				$primary[$key]  = $row['resellerName'];
				$secondary[$key] = $row['clientName'];
				$tertiary[$key] = $row['userFullName'];
			}

			array_multisort($primary, SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE, $secondary, SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE, $tertiary, SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE, $data);
		}
		
		return $data;
	}

	/**
	 * Enable page as printable for _print.html counterpart
	 * @return boolean
	 */
	public function isPrintable()
	{
		return TRUE;
	}

    public function runReport() {
        $viewLink = '/admin/report/commission/detail';

        $viewLink .= '?startDate=' . date_format(date_create($this->get('startDate')->value), 'Ymd');
        $viewLink .= '&endDate=' . date_format(date_create($this->get('endDate')->value), 'Ymd');

		foreach ((array) $this->fv['cID'] as $i=>$clientID) {
			$viewLink .= '&cID['.$i.']='.(int)$clientID;
		}

		// Update the saved date.
		$this->session['WidgetCommissionReport::startDate'] = $this->get('startDate')->value;
		$this->session['WidgetCommissionReport::endDate'] = $this->get('endDate')->value;

		$this->ajax->redirect($viewLink);
    }

	public function printThisPage()
	{
		$url = '/admin/report/commission/detail';
		$qs = '?startDate='.$this->fv['startDate']
				.'&endDate='.$this->fv['endDate']
				.'&cID[]='.implode('&cID[]=', $this->fv['cID']).'&PRINT=1';
		$this->ajax->script( 'window.open("' . addslashes( "{$url}{$qs}" ) . '")' );
	}

	public function exportXLS()
	{
        $xls = \PHPExcel_IOFactory::load(Core\IO::dir( 'backend' ) . '/_templates/commissionreport.xls' );
        $sheet = $xls->setActiveSheetIndex( 0 );
        $sheet->setTitle( 'Commission Report' );
		$this->fillSpreadSheet( $sheet );
        $writer = new \PHPExcel_Writer_Excel5( $xls );
		$fName = 'Commission Report.xls';
        $fileName = Core\IO::dir( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID();
        $writer->save( $fileName );
        $this->ajax->downloadFile( $fileName, $fName, 'application/vnd.ms-excel' );
    }

	private function fillSpreadSheet( \PHPExcel_Worksheet $sheet )
	{
		$colHeaders = ['Reseller', 'Firm', 'Name', 'Username', 'City', 'State/Region', 'Case', 'Created', 'Deleted'];
		$rowI = 1;
		$colI = 0;

		foreach ($colHeaders as $i => $header) {
			$sheet->getCellByColumnAndRow( $i, $rowI )->setValue( $header );
		}

		foreach($this->tpl->reports as $report) {
			++$rowI;
			
			$created = date_format(date_create($report['created']),'m/d/Y');
			$deleted = ( $report['deleted'] ) ? date_format(date_create($report['deleted']),'m/d/Y') : '';
			
			$colI=0; $sheet->getCellByColumnAndRow($colI, $rowI)->setValue($report['resellerName']);
			$colI++; $sheet->getCellByColumnAndRow($colI, $rowI)->setValue($report['clientName']);
			$colI++; $sheet->getCellByColumnAndRow($colI, $rowI)->setValue($report['firstName'].' '.$report['lastName']);
			$colI++; $sheet->getCellByColumnAndRow($colI, $rowI)->setValue($report['username']);
			$colI++; $sheet->getCellByColumnAndRow($colI, $rowI)->setValue($report['city']);
			$colI++; $sheet->getCellByColumnAndRow($colI, $rowI)->setValue($report['state']);
			$colI++; $sheet->getCellByColumnAndRow($colI, $rowI)->setValue($report['caseName']);
			$colI++; $sheet->getCellByColumnAndRow($colI, $rowI)->setValue($created);
			$colI++; $sheet->getCellByColumnAndRow($colI, $rowI)->setValue($deleted);
		}
    }
}

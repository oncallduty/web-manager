<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageDetailReport extends Backend {

    protected $rls = array();

    public function __construct() {
        $print = $this->isPrintMode();
        parent::__construct("reports/detailreport".($print?'_print':'').".html");
		if ($print) {
			// When printing, the command line code will double urlencode the query string.  We need to undo this and update $this->fv.
			$query_string = urldecode(urldecode($_SERVER['QUERY_STRING']));
			parse_str($query_string, $this->fv);
		}

		foreach ((array) $this->fv['rID'] as $i=>$resellerID) {
			$resellerID = (int)$resellerID;
			$reseller = new DB\Clients( $resellerID );
			if ( !$reseller || !$reseller->ID || $reseller->ID != $resellerID ) {
				continue;
			}
			$this->rls[$i] = $reseller;
		}
		usort($this->rls, function($a, $b) {
			return strcasecmp($a->name, $b->name);
		});
    }

	public function access() {
		if( !parent::access() ) {
			return false;
		}
		$this->noAccessURL = $this->basePath . '/';
		return in_array( $this->user->typeID, [self::USER_TYPE_SUPERADMIN, self::USER_TYPE_ADMIN] );
	}

	public function init() {
        parent::init();
        $this->head->name = 'Reports';
		$this->tpl->headName = 'Usage Reports';
		$this->tpl->isAdmin = true;

        $this->tpl->url = $this->basePath;

		$params['startDate'] = date( 'Y-m-01 00:00:00', strtotime( $this->fv['startDate'] ) );
		$params['endDate'] = date( 'Y-m-t 23:59:59', strtotime( $this->fv['endDate'] ) );
		$displayStartDate = date( 'm/d/Y', strtotime( $params['startDate'] ) );
		$displayEndDate = date( 'm/d/Y', strtotime( $params['endDate'] ) );
		$date_range = "From: {$displayStartDate} To: {$displayEndDate}";

		if ( !$this->isPrintMode() ) {
			$this->get('startDate')->value = $displayStartDate;
			$this->get('endDate')->value = $displayEndDate;
		}
		$this->tpl->startDate = $this->get('startDate')->value;
		$this->tpl->endDate = $this->get('endDate')->value;

        $this->get('repeat')->count = count($this->rls);
		$this->tpl->count = count($this->rls);

		$panelCount = 0;
        foreach ( $this->rls as $i => $r ) {
			$resellerID = (int)$r->ID;
			$reseller = new DB\Clients( $resellerID );
			$panel = $this->get('repeat.client_'.$panelCount);
			$panel->tpl->showClientInfo = true;
			$panel->tpl->date_range = $date_range;
			$this->fillPanelClient( $panel, $reseller );

			$rows = foo(new DB\OrchestraClients())->getClientByResellerIDDateRange( $resellerID, $params['startDate'], $params['endDate'] );
			$clients = [];
			foreach ( $rows as $row ) {
				$client = new \ClickBlocks\DB\Clients( $row['ID'] );
				if ( $client && $client->ID ) {
					$clients[] = $client;
				}
			}
			if ( count($clients) > 0 ) {
				$this->get('repeat')->count = $this->get('repeat')->count + count($clients) - 1;
			} else {
				$panelCount++;
			}

			$panel->tpl->lastClient = false;
			$showedHeader = false;
			foreach ( $clients as $cl ) {
				//See if we need to add a new panel.
				if ( $showedHeader ) {
					$panel = $this->get('repeat.client_' . $panelCount);
					$panel->tpl->date_range = $date_range;
					$panel->tpl->resellerName = $reseller->name;
				}
				$params['clientID'] = $cl->ID;
				$panel->tpl->clientName = $cl->name;
				$panel->tpl->clientLocation = $cl->location;
				$panel->tpl->isEnterprise = false;
				$charges = foo(new DB\OrchestraUsers())->getUsageReport( $params );
				$panel->tpl->charges = $this->getExhibitCounts( $charges );
				$showedHeader = true;
				$panelCount++;
			}
			$panel->tpl->lastClient = true;

			$panel->tpl->isLast = ( $i == $this->tpl->count - 1 );
		}
	}

	// Fill in the client information into the panel
	protected function fillPanelClient($panel, $reseller) {
		$panel->tpl->resellerName = $reseller->name;
		$panel->tpl->resellerID = $reseller->ID;
		$panel->tpl->clientTypeID = $reseller->typeID;
		$panel->tpl->contact = $reseller->contactName;
		$panel->tpl->sinceDate = date_format(date_create($reseller->startDate), 'm/d/Y');
		$panel->tpl->phone = \ClickBlocks\Utils::formatPhone($reseller->contactPhone);
		$panel->tpl->email = $reseller->contactEmail;
		$panel->tpl->address = $reseller->address1;
		$panel->tpl->address2 = $reseller->address2;
		$panel->tpl->city = $reseller->city . ', ' . $reseller->state . ' ' . $reseller->ZIP;
		$panel->tpl->numResellerClients = $reseller->getClientsCount();
		$panel->tpl->active = $reseller->deactivated ? 0 : 1;
		$panel->tpl->location = $reseller->location->name;
		$panel->tpl->salesRep = $reseller->salesRep ?: '';
	}

	public function getExhibitCounts( $charges ) {
		$orchDepos = $this->getOrchestraDepositions();

		foreach ( $charges as &$charge ) {
			$charge['exhibitsIntroduced'] = $orchDepos->getExhibitsIntroducedCount( $charge['sessionID'], $charge['userID'] );
			$charge['exhibitsReceived'] = $orchDepos->getExhibitsReceivedCount( $charge['sessionID'], $charge['userID'] );
		}

		return $charges;
	}

	public function isPrintable() {
		return true;
    }

    public function runReport() {
		$viewLink = '/admin/report/detailreport';
        $viewLink .= '?startDate=' . date_format(date_create($this->get('startDate')->value), 'Ymd');
        $viewLink .= '&endDate=' . date_format(date_create($this->get('endDate')->value), 'Ymd');

		foreach ( (array) $this->fv['rID'] as $i => $resellerID ) {
			$viewLink .= '&rID['.$i.']='.(int)$resellerID;
		}

		// Update the saved date.
		$this->session['WidgetReports::startDate'] = $this->get('startDate')->value;
		$this->session['WidgetReports::endDate'] = $this->get('endDate')->value;

        $this->ajax->redirect($viewLink);
    }

    public function exportXLS()
	{
        $xls = \PHPExcel_IOFactory::load(Core\IO::dir('backend') . '/_templates/detailreportclients.xls');
        $sheet = $xls->setActiveSheetIndex(0);
        $sheet->setTitle('Usage Report');
		$this->fillSpreadSheet( $sheet );
        $writer = new \PHPExcel_Writer_Excel5($xls);
		$fName = 'Usage Report.xls';
        $fileName = Core\IO::dir('temp').'/'.\ClickBlocks\Utils::createUUID();
        $writer->save($fileName);
        $this->ajax->downloadFile($fileName, $fName, 'application/vnd.ms-excel');
    }

    public function fillSpreadSheet( \PHPExcel_Worksheet $sheet )
	{
		$colHeaders = ['Reseller', 'Client', 'Date', 'User', 'SessionType', 'Case', 'Session', 'Exhibits Introduced', 'Exhibits Received'];
		$colValueIndexes = ['created', 'userName', 'depositionClass', 'casename', 'depositionOf', 'exhibitsIntroduced', 'exhibitsReceived'];
		$rowI = 1;
		foreach ( $colHeaders as $i => $header ) {
			$sheet->getCellByColumnAndRow( $i, $rowI )->setValue( $header );
		}
		$rowI++;
		for ( $i = 0; $i < $this->get('repeat')->count; $i++ ) {
			$panel = $this->get( "repeat.client_$i" );
			$charges = $panel->tpl->charges;
			if ( $charges && !empty($charges) ) {
				$resellerName = $panel->tpl->resellerName;
				$clientName = $panel->tpl->clientName;
				foreach ( $panel->tpl->charges as $charge ) {
					$sheet->getCellByColumnAndRow( 0, $rowI )->setValue( $resellerName );
					$sheet->getCellByColumnAndRow( 1, $rowI )->setValue( $clientName );

					foreach ( $colValueIndexes as $j => $chargeIndex ) {
						$sheet->getCellByColumnAndRow( $j+2, $rowI )->setValue( $charge[$chargeIndex] );
					}
					$rowI++;
				}
			}
		}
    }

    public function printInvoice() {
        $this->downloadPageAsPDF();
    }
}
?>

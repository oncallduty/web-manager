<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

class PageDepositionDetail extends Backend {

	private $baseURL = '/';
	private $reports = [];

	/**
	 * @var \ClickBlocks\DB\OrchestraDepositionAttendees
	 */
	private $orchDA = NULL;

	/**
	 * @var \ClickBlocks\DB\OrchestraPricingModelOptions
	 */
	private $orchPMO = NULL;

	/**
	 * @var \ClickBlocks\DB\OrchestraPricingEnterpriseOptions
	 */
	private $orchPEO = NULL;

	/**
	 * @var \ClickBlocks\DB\OrchestraEnterpriseAgreements
	 */
	private $orchEA = NULL;

	public function __construct()
	{
		$isPrint = $this->isPrintMode();
        parent::__construct( 'reports/depositiondetail' . ($isPrint ? '_print' : '') . '.html' );
	}

	public function access()
	{
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		return ($this->user->typeID === self::USER_TYPE_RESELLER);
	}

	public function init()
	{
		parent::init();
		$this->orchDA = new \ClickBlocks\DB\OrchestraDepositionAttendees();
		$this->orchPMO = new \ClickBlocks\DB\OrchestraPricingModelOptions();
		$this->orchPEO = new \ClickBlocks\DB\OrchestraPricingEnterpriseOptions();
		$this->orchEA = new \ClickBlocks\DB\OrchestraEnterpriseAgreements();
		$this->head->name = 'Reports';
		$this->baseURL .= mb_strtolower( $this->reseller['URL'] );
		$this->tpl->url = $this->baseURL;
		if( isset( $this->fv['IDs'] ) && is_array( $this->fv['IDs'] ) ) {
			foreach( $this->fv['IDs'] as $ID ) {
				$deposition = new \ClickBlocks\DB\Depositions( $ID );
				if( !$deposition || !$deposition->ID || $deposition->ID != $ID || $deposition->parentID ) {
					continue;
				}
				$case = new \ClickBlocks\DB\Cases( $deposition->caseID );
				if( !$case || !$case->ID || $case->ID != $deposition->caseID ) {
					continue;
				}
				$client = new \ClickBlocks\DB\Clients( $case->clientID );
				if( !$client || !$client->ID || $client->ID != $case->clientID ) {
					continue;
				}
				if( $deposition->enterpriseResellerID ) {
					if( $deposition->enterpriseResellerID != $this->reseller['ID'] ) {
						continue;
					}
				} elseif( $client->resellerID != $this->reseller['ID'] ) {
					continue;
				}
				$this->depositionReport( $deposition, $client );
			}
		}
		if( !count( $this->reports ) ) {
			header( "Location: {$this->baseURL}/reports/deposition" );
		}
		$this->tpl->count = count( $this->reports );
		$this->tpl->reports = $this->reports;
	}

	/**
	 * Enable page as printable for _print.html counterpart
	 * @return boolean
	 */
	public function isPrintable()
	{
		return TRUE;
	}

	private function depositionReport( \ClickBlocks\DB\Depositions $deposition, \ClickBlocks\DB\Clients $client )
	{
		$report = new \stdClass();
		$report->date = date( 'M. d, Y', strtotime( $deposition->started ) );
		$report->caseName = $deposition->cases[0]->name;
		$report->witness = $deposition->depositionOf;
		$report->volume = $deposition->volume;
		$report->ownerID = $deposition->ownerID;
		$report->jobNumber = $deposition->jobNumber;
		$report->location = $deposition->location;

		$report->clients = [];
		$report->guests = FALSE;

		$orchDepo = new \ClickBlocks\DB\OrchestraDepositions();
		$hostingResellerID = $client->parentClients1[0]->ID;
		$hostingClient = $this->clientDetails( $deposition, $client, $hostingResellerID );
		if( $hostingClient ) {
			$report->clients[] = $hostingClient;
		} else {
			//something went very wrong -- abort?
			return;
		}
		$guests = $this->orchDA->getAttendeesByRoles( $deposition->ID, ['G'] );
		if( $guests ) {
			$report->guests = $guests;
		}
		$childDepoIDs = $orchDepo->getChildDepositionIDs( $deposition->ID );
		foreach( $childDepoIDs as $childDepoID ) {
			$attDeposition = new \ClickBlocks\DB\Depositions( $childDepoID );
			if( !$attDeposition || !$attDeposition->ID || $attDeposition->ID != $childDepoID || $attDeposition->parentID != $deposition->ID ) {
				continue;
			}
			$attClient = $this->clientDetails( $attDeposition, NULL, $hostingResellerID );
			if( $attClient ) {
				$report->clients[] = $attClient;
			}
		}
		$this->reports[] = $report;
	}

	private function clientDetails( \ClickBlocks\DB\Depositions $deposition, \ClickBlocks\DB\Clients $client=NULL, $hostingResellerID=NULL )
	{
		$attClient = new \stdClass();
		if( !$client ) {
			$case = new \ClickBlocks\DB\Cases( $deposition->caseID );
			if( !$case || !$case->ID || $case->ID != $deposition->caseID ) {
				return;
			}
			$client = new \ClickBlocks\DB\Clients( $case->clientID );
			if( !$client || !$client->ID || $client->ID != $case->clientID || $client->getReseller()->resellerClass === self::RESELLERCLASS_DEMO ) {
				return;
			}
		}
		$attClient->name = $client->name;
		$attClient->phone = \ClickBlocks\Utils::formatPhone( $client->contactPhone );
		$attClient->address1 = $client->address1;
		$attClient->address2 = $client->address2;
		$attClient->city = $client->city;
		$attClient->state = $client->state;
		$attClient->zipCode = $client->ZIP;
		$attClient->attendees = $this->orchDA->getMemberAttendees( $deposition->ID );
		$resellerID = ($hostingResellerID) ? $hostingResellerID : $client->resellerID;
		foreach( $attClient->attendees as &$attendee ) {
			$attendee['introduced'] = $deposition->getExhibitsIntroducedCount( $attendee['ID'] );
			$attendee['received'] = $deposition->getExhibitsReceivedCount( $attendee['ID'] );
		}
		return $attClient;
	}

	public function printThisPage()
	{
		$url = '/' . $this->reseller['URL'] . '/reports/deposition/detail';
		$qs = '?IDs[]=' . implode( '&IDs[]=', $_REQUEST['IDs'] ) . '&PRINT=1';
		$this->ajax->script( 'window.open("' . addslashes( "{$url}{$qs}" ) . '")' );
	}

	public function exportXLS()
	{
        $xls = \PHPExcel_IOFactory::load(Core\IO::dir( 'backend' ) . '/_templates/depositionreport.xls' );
        $sheet = $xls->setActiveSheetIndex( 0 );
        $sheet->setTitle( 'Deposition Report' );
		$this->fillSpreadSheet( $sheet );
        $writer = new \PHPExcel_Writer_Excel5( $xls );
		$fName = 'Depostion Report.xls';
        $fileName = Core\IO::dir( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID();
        $writer->save( $fileName );
        $this->ajax->downloadFile( $fileName, $fName, 'application/vnd.ms-excel' );
    }

	private function fillSpreadSheet( \PHPExcel_Worksheet $sheet )
	{
		$colHeaders = ['Deposition', 'Case', 'Date', 'Volume', 'Location', 'Client', 'Client Phone', 'Client Address', 'Client City', 'Client State', 'Client ZIP',
			'User', 'User Type', 'User Email', 'Exhibits Introduced', 'Exhibits Received'];
		$rowI = 1;
		$colI = 0;

		foreach ($colHeaders as $i => $header)
		{
			$sheet->getCellByColumnAndRow( $i, $rowI )->setValue( $header );
		}

		++$rowI;

		for ($i = 0; $i < count($this->tpl->reports); ++$i)
		{
			$depoInfo = [$this->tpl->reports[$i]->witness, $this->tpl->reports[$i]->caseName, $this->tpl->reports[$i]->date, $this->tpl->reports[$i]->volume, $this->tpl->reports[$i]->location];
			$depoInfoCount = count($depoInfo);

			foreach ($this->tpl->reports[$i]->clients as $client)
			{
				$clientAddress = $client->address2 ? $client->address1 . ' ' . $client->address2 : $client->address1;
				$clientInfo = [$client->name, $client->phone, $clientAddress, $client->city, $client->state, $client->zipCode];
				$clientInfoCount = count($clientInfo);

				foreach ($client->attendees as $attendee)
				{
					$colI = 0;
					for ($j = 0; $j < $depoInfoCount; ++$j)
					{
						$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $depoInfo[$j] );
						++$colI;
					}
					for ($j = 0; $j < $clientInfoCount; ++$j)
					{
						$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $clientInfo[$j] );
						++$colI;
					}

					$userType = ($attendee['ID'] == $this->tpl->reports[$i]->ownerID) ? "Owner" : "Member";
					$attendeeInfo = [$attendee['firstName'] . ' ' . $attendee['lastName'], $userType, $attendee['email'], $attendee['introduced'], $attendee['received']];
					$attendeeInfoCount = count($attendeeInfo);

					for ($j = 0; $j < $attendeeInfoCount; ++$j)
					{
						$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $attendeeInfo[$j] );
						++$colI;
					}

					++$rowI;
				}
			}

			if ($this->tpl->reports[$i]->guests)
			{
				foreach ($this->tpl->reports[$i]->guests as $guest)
				{
					$colI = 0;
					for ($j = 0; $j < $depoInfoCount; ++$j)
					{
						$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $depoInfo[$j] );
						++$colI;
					}
					for ($j = 0; $j < $clientInfoCount; ++$j)
					{
						$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( 'n/a' );
						++$colI;
					}

					$guestInfo = [$guest['name'], 'Guest', $guest['email'], 0, 0];
					$guestInfoCount = count($guestInfo);

					for ($j = 0; $j < $guestInfoCount; ++$j)
					{
						$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $guestInfo[$j] );
						++$colI;
					}
					++$rowI;
				}
			}
		}
    }
}

<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
ClickBlocks\DB,
ClickBlocks\Web,
ClickBlocks\Web\UI\POM,
ClickBlocks\Web\UI\Helpers,
ClickBlocks\Utils;

class PageResellerPricing extends BaseMyAccount
{
	protected $client = NULL;

	public function __construct()
	{
		parent::__construct();
		$this->client = new \ClickBlocks\DB\Clients( $this->user->clientID );
	}

	public function access()
	{
		if( !parent::access() ) {
			return false;
		}
		$this->noAccessURL = $this->basePath . '/';
		return ($this->user->typeID == self::USER_TYPE_RESELLER);
	}

	public function init()
	{
		parent::init();
		$this->tpl->mtab = self::TAB_RESELLER_PRICING;
		$panelInfo = $this->tpl->mpanels[self::TAB_RESELLER_PRICING];
		$pnl = $this->get( $panelInfo['id'] );
		$pnl->setTemplate( $panelInfo['url'] );

		$orchPMO = new \ClickBlocks\DB\OrchestraPricingModelOptions();
		$pricingModel = $orchPMO->getByClient( $this->client->ID );

		$this->tpl->subcriptionFee = $this->priceFromModel( $pricingModel, self::PRICING_OPTION_SUBSCRIPTION, TRUE );
	}
}
<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageNotificationLogs extends Backend
{
    public function __construct()
    {
       parent::__construct( 'notifications/notificationLogs.html' );
    }

	public function access()
	{
		if (!parent::access()) {
			return false;
		}
		$this->noAccessURL = $this->basePath.'/';
		return ($this->user->typeID == self::USER_TYPE_SUPERADMIN || $this->user->typeID == self::USER_TYPE_ADMIN);
	}

    public function init()
    {
		parent::init();

        $this->tpl->tab = 1;
        $this->head->name = 'Notification Logs';
		$this->tpl->headName = 'Notification Logs';
		$this->tpl->isAdmin = true;
		$this->tpl->url = $this->basePath;
    }

    public function exportNotificationLogs() {
        $this->get('notificationLogs')->exportToExcel();
    }
}

?>
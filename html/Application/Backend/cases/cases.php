<?php

namespace ClickBlocks\MVC\Backend;

class PageCases extends Backend {

	protected $isAdmin = FALSE;

	public function __construct() {
		parent::__construct( 'cases/cases.html' );
	}

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/login';
		$this->isAdmin = ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin);
		return (in_array( $this->user->typeID, [self::USER_TYPE_CLIENT, self::USER_TYPE_USER] ));
	}

	public function init() {
		parent::init();
		$this->head->name = 'Case Management';
		$this->tpl->url = mb_strtolower( $this->reg->reseller['URL'] );
		$this->tpl->isAdmin = $this->isAdmin;

		$casesWidget = $this->get( 'cases' );
		$casesWidget->clientID = $this->user->clientID;
		$casesWidget->userID = $this->user->ID;
	}
}

<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\Utils,
	ClickBlocks\EDPDFConvert,
	ClickBlocks\DB\ExhibitsPortalAuth,
	ClickBlocks\Annotate;

//defined( 'MOVE_ITEM_TOKEN' ) or define( 'MOVE_ITEM_TOKEN', 'moveitemmem' );

class PageCourtSessionView extends Backend
{
	/**
	 * @var \ClickBlocks\DB\Depositions
	 */
	protected $deposition = NULL;
	protected $case = null;
	protected $exhibit = 'Exhibit';
	protected $canDelete = false;
	protected $isAdmin = false;
	protected $isCreator = false;
	protected $isDepoOwner = false;
	protected $isDemoAdmin = false;
	protected $isTrustedUser = false;
	protected $isClientUser = FALSE;

	public function __construct() {
		parent::__construct( 'courts/sessionView.html' );
		$dpid = isset($this->fv['depoID']) ? (int) $this->fv['depoID'] : 0;
		$cid = isset($this->fv['caseID']) ? (int) $this->fv['caseID'] : 0;
		$this->deposition = foo(new DB\ServiceDepositions)->getByID($dpid);
		$this->case = foo(new DB\ServiceCases)->getByID($cid);
		$this->client = foo(new DB\ServiceClients)->getByID( $this->case->clientID );
	}

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		if( $this->user->clientID != $this->case->clientID ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/cases';
		$this->isAdmin = ($this->user->clients[0]->typeID === Edepo::CLIENT_TYPE_COURTCLIENT && $this->user->typeID === Edepo::USER_TYPE_CLIENT || $this->user->clientAdmin);
		$this->isClientUser = in_array( $this->user->typeID, [Edepo::USER_TYPE_USER, Edepo::USER_TYPE_JUDGE, Edepo::USER_TYPE_REPORTER] );
//		$this->isAdmin = ($this->case->ID && (new DB\OrchestraCases())->checkCaseAdmin($this->case->ID, $this->user->ID));
//		$this->isDemoAdmin = ($this->user->clientID == $this->case->clientID && $this->case->class == 'Demo');
//		$this->isManager = ( new DB\OrchestraCaseManagers() )->checkCaseManager( $this->case->ID, $this->user->ID );
		$this->isDepoOwner = ($this->deposition->ID && $this->deposition->ownerID == $this->user->ID);
		$this->isDepoAssist = foo( new DB\OrchestraCases() )->checkDepoAssist( $this->case->ID, $this->user->ID );
		$this->isDepoAttend = foo( new DB\OrchestraCases() )->checkDepoAttendee( $this->case->ID, $this->user->ID );
//		$this->isAssistant = (bool)(new DB\ServiceDepositionAssistants)->getByID(array('userID'=>$this->user->ID,'depositionID'=>$this->deposition->ID))->userID;
		$this->isTrustedUser = $this->getOrchestraDepositions()->isTrustedUser( $this->user->ID, $this->deposition->ID );
		$this->canDelete = FALSE;

		return $this->isAdmin || ($this->isClientUser && ($this->isDepoOwner || $this->isDepoAssist || $this->isDepoAttend));
//		return ($this->isDepoOwner || $this->canDelete);
	}

	public function init()
	{
		parent::init();
		$this->js->addTool( 'ui' );

//		$this->tpl->isAdmin = $this->isAdmin;
//		$this->tpl->isDemoAdmin = $this->isDemoAdmin;
//		$this->tpl->isManager = $this->isManager;
		$this->tpl->isDepoOwner = $this->isDepoOwner;
		$this->tpl->isDepoAssist = $this->isDepoAssist;
		$this->tpl->admin = 1;

		$this->head->name = 'Session View';
		$this->tpl->caseID = $this->case->ID;
		$this->tpl->ID = $this->deposition->ID;
		$this->tpl->hname = $this->deposition->depositionOf;
		$this->tpl->cname = $this->case->name;

		$depoInfoPanel = $this->get('depoInfoPanel');
		$depoInfoPanel->tpl->ID = $this->deposition->ID;
		$depoInfoPanel->tpl->depositionOf = $this->deposition->depositionOf;
		$depoInfoPanel->tpl->openDateTime = $this->deposition->openDateTime;
		$depoInfoPanel->tpl->volume = $this->deposition->volume;
		$depoInfoPanel->tpl->class = $this->deposition->class;
		$depoInfoPanel->tpl->statusID = $this->deposition->statusID;

		$this->tpl->hasDatasources = $this->getOrchestraDatasources()->hasDatasources( $this->user->clientID );

		$isChild = ($this->deposition->parentID !== null) ? TRUE : FALSE;
		$isFinished = ($this->deposition->statusID == 'F') ? TRUE : FALSE;
		$isWitnessPrep = $this->deposition->isWitnessPrep();

		$isDemo = $this->deposition->isDemo();
		$active = ($this->deposition->statusID === self::DEPOSITION_STATUS_IN_PROCESS);
		$started = $this->deposition->started ? 1 : 0;
        $finished = $this->deposition->finished ? 1 : 0;

//		$this->tpl->attendees = (new DB\OrchestraDepositionAttendees())->getAttendeesByDepo($this->deposition->parentID);

		$folders = $this->getOrchestraFolders()->getFoldersByUserID($this->deposition->ID, $this->user->ID);
		$folders = $this->sortList($folders, 'Folders');
		$folderPanel = $this->get('folder_panel');
		$addFolderParams = [];
        foreach ($folders as $idx => $folder)
		{
			$addFolderParams[] = [
				'ID' => $folder['ID'],
				'name' => $folder['name']
			];
        }
		$folderPanel->tpl->folderList = $addFolderParams;

		//Needed for the Documents sort header info to show up
		$prefs = $this->getSortPrefs('Folders');
		$this->updateSortHeader($prefs['sortBy'], $prefs['sortOrder'], 'Folders');
		$prefs = $this->getSortPrefs('Files');
		$this->updateSortHeader($prefs['sortBy'], $prefs['sortOrder'], 'Files');

		$sortMenuOptions = [
			['name'=>'Name'],
			['name'=>'Date'],
			['name'=>'Custom']
		];

		$this->get( 'sortMenuPopup' )->tpl->sortMenuItems = $sortMenuOptions;

		$addMenuOptions = [];
		if ( $this->isDepoOwner || $this->isDepoAssist ) {
			if ( $this->isDepoAssist ) {
				$addMenuOptions['Download Session'] = ['onclick'=>'ajax.doit(\'->downloadDeposition\');'];
			}
			if ( ($this->isDepoOwner || $this->isDepoAssist) && ($finished || ($isDemo && $active) ) ) {
				$addMenuOptions['Exhibit Log'] = ['onclick'=>'ajax.doit(\'->depoExhibitLog\');'];
			}
//			if ( $this->canDelete && $this->deposition->statusID != 'I') {
//				$addMenuOptions['Delete'] = ['onclick'=>'ajax.doit(\'->confirmDeleteDepo\', \'' . $this->deposition->ID . '\');'];
//			}
		}
		if ( $this->user->typeID == self::USER_TYPE_REPORTER || $this->user->clientAdmin ) {
			$addMenuOptions['Send Portal Invite'] = ['onclick'=>'showReporterNotice()'];
		}
		$this->get('session_btn_menu')->options = $addMenuOptions;
	}

	public function showReporterNotice( $depId ) {
		$deposition = foo( new DB\ServiceDepositions() )->getByID( $depId );

		$dlg = $this->get( 'sendReporterNoticePopup' );
		$dlg->get( 'errormsg' )->text = '';
		$dlg->get( 'reporterEmail' )->value = '';
		$dlg->get( 'btnOK' )->onclick = "sendReporterNotice({$depId})";
		$dlg->show();
	}

	public function sendReporterNotice( $depId ) {
		$dlg = $this->get( 'sendReporterNoticePopup' );
		$dlg->get( 'errormsg' )->text = '';
		$deposition = new DB\Depositions($depId);

		if ( $this->user->typeID != self::USER_TYPE_REPORTER && !$this->user->clientAdmin ) {
			$dlg->get( 'errormsg' )->text = 'Encountered an invalid state. Please press Cancel.';
			return false;
		}

		$courtReporterEmail = $dlg->get( 'reporterEmail' )->value;

		if ($courtReporterEmail === null || strlen( $courtReporterEmail ) < 1) {
			$dlg->get( 'errormsg' )->text = 'Please enter a valid email address.';
			return false;
		}

		$dlg->hide();

        // update the court reporter email in the record
//        $deposition->courtReporterEmail = $courtReporterEmail;
//        $deposition->update();

		$pwd = substr( ExhibitsPortalAuth::generateSpice(), 0, 8 );
		$epa = new ExhibitsPortalAuth( $deposition->ID, $courtReporterEmail );
		if( !$epa->sessionID || !$epa->email ) {
			$epa->sessionID = $deposition->ID;
			$epa->email = $courtReporterEmail;
		}
		$epa->spice = ExhibitsPortalAuth::generateSpice();
		$epa->word = hash( 'sha1', $pwd );
		$epa->save();

		Utils\EmailGenerator::sendCourtReporterInvite( $deposition->ID, $epa->email, $pwd );
	}

	public function sortList($list, $type) {
		$sortedList = [];

		if (!empty($list)) {
			$prefs = $this->getSortPrefs($type);
			$sortOrderMap = ['Asc'=>SORT_ASC, 'Desc'=>SORT_DESC];
			$sortedList = $this->sortRows($list, $prefs['sortOn'], $sortOrderMap[$prefs['sortOrder'] ] );
		}

		return $sortedList;
	}

	function getSortPrefs($type) {
		$sortPrefs = (new DB\OrchestraUserSortPreferences())->getSortPreferencesForUserIDBySortObject($this->user->ID, $type);
		$sortBy = '';
		$sortOrder = '';
		$sortOn = '';
		if ( !empty($sortPrefs) ) {
			switch ($sortPrefs['sortBy']) {
				case 'Name':
					$sortBy = 'Name';
					$sortOrder = $sortPrefs['sortOrder'];
					$sortOn = 'name';
					break;
				case 'Date':
					$sortBy = 'Date';
					$sortOrder = $sortPrefs['sortOrder'];
					$sortOn = 'created';
					break;
				case 'Custom':
					$sortBy = 'Custom';
					$sortOrder = $sortPrefs['sortOrder'];
					$sortOn = 'sortPos';
					break;
			}
		} else {
			$sortBy = 'Name';
			$sortOrder = 'Asc';
			$sortOn = 'name';
		}

		return ['sortBy'=>$sortBy, 'sortOrder'=>$sortOrder, 'sortOn'=>$sortOn];
	}

	function updateSortHeader($sortBy, $sortOrder, $type) {
		$sortOrder = ($sortBy == 'Custom') ? '' : $sortOrder;

		if ($type == 'Folders') {
			$folderSortPanel = $this->get('folderSortPanel');
			$folderSortPanel->tpl->folderSortBy = $sortBy;
			$folderSortPanel->tpl->folderSortOrder = $sortOrder;
			$folderSortPanel->update();
		} else if ($type == 'Files') {
			$fileSortPanel = $this->get('fileSortPanel');
			$fileSortPanel->tpl->fileSortBy = $sortBy;
			$fileSortPanel->tpl->fileSortOrder = $sortOrder;
			$fileSortPanel->update();
		}
	}

	function sortRows($data, $sortBy, $sortOrder) {
		foreach ($data as $key => $row) {
			$primary[$key]  = $row[$sortBy];
			$secondary[$key] = $row['ID'];
		}

		array_multisort($primary, $sortOrder, SORT_NATURAL | SORT_FLAG_CASE, $secondary, SORT_DESC, SORT_NATURAL, $data);

		return $data;
	}

	public function changeSort($sortObject, $sortBy, $sortOrder, $selectedFolderID = 0) {
		$sortPref = new \ClickBlocks\DB\UserSortPreferences( $this->user->ID, $sortObject );
		if( !$sortPref || !$sortPref->userID || !$sortPref->sortObject || $sortPref->userID != $this->user->ID || $sortPref->sortObject != $sortObject ) {
			$sortPref->userID = $this->user->ID;
			$sortPref->sortObject = $sortObject;
		}
		$sortPref->setSortPreference( $sortBy, $sortOrder );

		if ($sortObject == 'Folders') {
			$this->refreshFolders();
			$this->ajax->script( "highlightFolder();" );
		} else if ($sortObject == 'Files') {
			$this->selectFolder( $selectedFolderID );
		}
	}

	public function setCustomSort($sortObject, $sortPosList) {
		$sortedListIDs = $sortPosList;
		sort($sortedListIDs, SORT_NUMERIC);

		if ($sortObject == 'Folders') {
			foreach( $sortedListIDs as $idx => $id ) {
				$objectID = (int)$sortedListIDs[$idx];
				$sortPos = (int)$sortPosList[$idx];
				$cs = new \ClickBlocks\DB\UserCustomSortFolders( $this->user->ID, $objectID );
				if( $cs->folderID != $objectID ) {
					$cs->userID = $this->user->ID;
					$cs->folderID = $objectID;
				}
				$cs->sortPos = $sortPos;
				$cs->save();
			}
		} else if ($sortObject == 'Files') {
			foreach( $sortedListIDs as $idx => $id ) {
				$objectID = (int)$sortedListIDs[$idx];
				$sortPos = (int)$sortPosList[$idx];
				$cs = new \ClickBlocks\DB\UserCustomSortFiles( $this->user->ID, $objectID );
				if( $cs->fileID != $objectID ) {
					$cs->userID = $this->user->ID;
					$cs->fileID = $objectID;
				}
				$cs->sortPos = $sortPos;
				$cs->save();
			}
		}
	}

	public function refreshFolders() {
		$folders = $this->getOrchestraFolders()->getFoldersByUserID($this->deposition->ID, $this->user->ID);
		$folders = $this->sortList($folders, 'Folders');
		$folderPanel = $this->get('folder_panel');
		$addFolderParams = [];
        foreach ($folders as $idx => $folder)
		{
			$addFolderParams[] = [
				'ID' => $folder['ID'],
				'name' => $folder['name']
			];
		}
		$folderPanel->tpl->folderList = $addFolderParams;
		$folderPanel->update();
		$this->ajax->script( 'updateFolderCount();' );
		$this->ajax->script( 'fillFolderList();' );
	}

	public function selectFolder( $id ) {
		$Folder = $this->getService( 'Folders' )->getByID( $id );
		$pathToFolder = $Folder->getFullPath();
		$isTranscript = $Folder->class == 'Transcript';
		$bitesConversion = 1000;
		$files = (new DB\OrchestraFiles())->getFilesWithSortPos($id, $this->user->ID);
		$files = $this->sortList($files, 'Files');
		$filePanel = $this->get('file_panel');
		$fileList = [];
		foreach ($files as $file) {
			$File = new \ClickBlocks\DB\Files( $file['ID'] );
			if( !$File || !$File->ID || $File->ID != $file['ID'] ) {
				continue;
			}
			if( !$File->filesize ) {
				$File->setMetadata();
			}
			$fileSize = Utils::readableFilesize( $File->filesize, 1 );

			$fileList[] = [
				'ID' => $file['ID'],
				'name' => $file['name'],
				'created' => $file['created'],
				'size' => $fileSize,
				'canRename' => ( $Folder->class === self::FOLDERS_TRANSCRIPT && $this->user->typeID === self::USER_TYPE_REPORTER && (!$this->deposition->started || $this->deposition->finished) )
			];
		}

		$this->updateFileMgmtActions($id, count($files));

		$filePanel->tpl->fileList = $fileList;
		$filePanel->update();
		$this->ajax->script( 'updateFileCount();' );
		$this->ajax->script( 'updateSelectedFilesCount('. 0 .');' );
		$this->ajax->script( 'fillFileList();' );
	}

	public function updateFileMgmtActions($folderID, $filesCount = FALSE) {
		$folder = (new DB\ServiceFolders())->getByID($folderID);
		$filesCount = ($filesCount !== FALSE) ? $filesCount : count((new DB\OrchestraFiles())->getFilesByFolder($folderID));

		$actions = [
			'downloadFolder' => 0,
			'downloadFile' => 0,
			'deleteFile' => 0,
			'addDocs' => 0
		];

		if ($filesCount > 0) {
			$actions['downloadFolder'] = 1; //download
			$actions['downloadFile'] = 1; //download
			if ( $folder->class === self::FOLDERS_TRANSCRIPT && $this->user->typeID === self::USER_TYPE_REPORTER ) {
				$actions['deleteFile'] = 1; //delete
			}
		}
		if ($folder->class === self::FOLDERS_TRANSCRIPT && $this->user->typeID === self::USER_TYPE_REPORTER) {
			$actions['addDocs'] = 1; //add
		}

		$this->ajax->script( 'updateFileMgmtActions(' . implode( ',', $actions ) . ');' );
	}

	public function showEditFile($id) {
        $dlg = $this->get('fileEditPopup');

        $file = foo(new DB\ServiceFiles())->getByID($id);
        if ($file->ID > 0) {
            $dlg->tpl->ID = $file->ID;
            $pos = strrpos($file->name, '.');
            $dlg->get('fileName')->value = substr($file->name, 0, $pos);
        }
        $dlg->get('btnOK')->onclick = 'saveFile(' . $file->ID . ')';
        $dlg->show();
    }

	public function checkFileName($ctrls) {
        $file = foo(new DB\ServiceFiles())->getByID($this->get('fileEditPopup')->tpl->ID);
        $fileEdit = $file->name;
        $pos = strrpos($file->name, '.');
        $fileName = $this->getByUniqueID($ctrls[0])->value . substr($file->name, $pos);
        if (!empty($fileEdit) && $fileEdit == $fileName) {
            return true;
        }
        return foo(new DB\OrchestraFiles())->checkUniqueFile($file->folderID, $fileName);
    }

	public function saveFile($id) {
        if (!$this->validators->isValid('file'))
            return;
        $this->get('fileEditPopup')->hide();
        $file = foo(new DB\ServiceFiles())->getByID($id);
        $pos = strrpos($file->name, '.');
        $file->name = $this->get('fileEditPopup.fileName')->value . substr($file->name, $pos);
        foo(new DB\ServiceFiles())->save($file);
		$this->ajax->script( "selectFolder(selectedFolderID);" );

		$this->notifyNodeJSUsers( $file->folderID );
    }

	public function confirmDeleteFile($id) {
        $param = array();
		// See if we are deleting more than one document.
		if (strpos($id, ',') === false) {
			$param['title'] = 'Delete Document?';
			$param['message'] = 'Are you sure you want to delete this document?';
		} else {
			$param['title'] = 'Delete Documents?';
			$param['message'] = 'Are you sure you want to delete these documents?';
		}
        $param['OKName'] = 'Delete';
        $param['OKMethod'] = "deleteFile('" . $id . "', 1);";
        $this->showPopup('confirm', $param);
    }

	public function deleteFile( $IDs )
	{
		// Hide dialog popup
		$this->hidePopup( 'confirm' );

		$fileIDs = explode( ',', $IDs );
		$folders = [];
		$now = date( 'Y-m-d H:i:s' );

		foreach( $fileIDs as $fileID ) {
			//file
			$file = new DB\Files( $fileID );
			if( !$file || !$file->ID || $file->ID != $fileID ) {
//				\ClickBlocks\Debug::ErrorLog( "PageSessionView::deleteFile; fileID: {$fileID} not found" );
				continue;
			}

			//folder
			$folder = new \ClickBlocks\DB\Folders( $file->folderID );
			if( !$folder || !$folder->ID || $folder->ID != $file->folderID ) {
//				\ClickBlocks\Debug::ErrorLog( "PageSessionView::deleteFile; folderID: {$file->folderID} not found" );
				continue;
			}

			//deposition
			$deposition = new \ClickBlocks\DB\Depositions( $folder->depositionID );
			if( !$deposition || !$deposition->ID || $deposition->ID != $folder->depositionID ) {
				\ClickBlocks\Debug::ErrorLog( "PageCaseView::deleteFile; depositionID: {$folder->depositionID} not found" );
				continue;
			}
			if( $folder->class == self::FOLDERS_EXHIBIT ) {
				continue;
			}

			$file->delete();

			if( !isset( $folders[$folder->ID] ) ) {
				$folder->lastModified = $now;
				$folder->save();
				$folders[$folder->ID] = $folder->ID;
			}
        }
		$nodeJS = new \ClickBlocks\Utils\NodeJS( TRUE );
		$nodeJS->notifyUsersRemovedFile( $folders );
		$this->ajax->script( "selectFolder({$folder->ID});" );

		unset( $deposition, $folder, $file );
	}

	public function showUploadFiles( $folderID )
	{
        $folder = (new DB\ServiceFolders())->getByID( $folderID );

		if (!$folder->class === self::FOLDERS_TRANSCRIPT || !$this->user->typeID === self::USER_TYPE_REPORTER) {
			return;
		}

		$this->tpl->folderID = $folder->ID;
		$this->tpl->folderName = $folder->name;
		$this->tpl->fileArray = array();
        $this->tpl->fileSizeArray = array();

        $dlg = $this->get('uploadFilesPopup');
        $dlg->get('dropzone')->tpl->files = array();
        $dlg->show();
    }

	public function folderUploadDocument( $folderID, $uniqueID, $uploadID )
	{
        $results = ['args'=>(object)['uniqueID' => $uniqueID, 'uploadID' => $uploadID, 'folderID' => $folderID], 'success'=>TRUE];

		try {
			$upload = new Utils\FileUpload();
			$extensions = trim( preg_replace( '/\s+/', '', $this->config->valid_files['extensions'] ) );
			$upload->extensions = explode( ',', $extensions );
			$documentTypesStr = trim( preg_replace( '/\s+/', '', $this->config->valid_files['document_types'] ) );
			$documentTypes = explode( ',', $documentTypesStr );
			$multimediaTypesStr = trim( preg_replace( '/\s+/', '', $this->config->valid_files['multimedia_types'] ) );
			$multimediaTypes = explode( ',', $multimediaTypesStr );
			$upload->types = array_merge( $documentTypes, $multimediaTypes );

//			\ClickBlocks\Debug::ErrorLog( print_r( $upload->extensions, TRUE ) );
//			\ClickBlocks\Debug::ErrorLog( print_r( $upload->types, TRUE ) );

			$info = $upload->upload( $_FILES[$uniqueID] );

			if( $upload->error === UPLOAD_ERR_INI_SIZE ) {
				throw new \Exception( 'File is too large.' );
			}

			if( $upload->error === UPLOAD_ERR_EXTENSION ) {
				throw new \Exception( 'File type is not accepted.' );
			}

			if( !$info ) {
				throw new \Exception( 'File upload is invalid' );
			}

			// prepare meta info
			$folder = new \ClickBlocks\DB\Folders( $folderID );
			$deposition = new \ClickBlocks\DB\Depositions( $folder->depositionID );
			$trustedUsers = $this->getOrchestraDepositions()->getAllTrustedUsersAsMap( $deposition->ID );

            // Is this a demo, and are we at the limit for demo files?
            if( $deposition->class == self::DEPOSITION_CLASS_DEMO ) {
                if( $this->config->demoDocLimit && ($deposition->getFileCount() - $deposition->getDemoCannedFileCount()) >= $this->config->demoDocLimit ) {
                    throw new \Exception( 'Demo documents limited to ' . $this->config->demoDocLimit . '.' );
                }
            }

			if( !file_exists( $info['fullname'] ) ) {
				throw new \Exception( 'Document not found: "'.$info['fullname'].'"' );
			}

			$filename = $info['originalName'];
			$filepathname = $info['fullname'];
			$isMultimedia = FALSE;

			// ED-1519: Allow multimedia conversions
			//\ClickBlocks\Debug::ErrorLog( "\$info['type']: ".$info['type'] );
			//\ClickBlocks\Debug::ErrorLog( "\$info: " . print_r($info, 1) );

			if( !in_array( $info['type'], $documentTypes ) && !in_array( $info['type'], $multimediaTypes ) ) {
				unlink( $info['fullname'] );
				throw new \Exception( 'Invalid file type ('.$info['type'].')' );
			}

			$isPDF = ($info['type'] == 'application/pdf');
			$isMultimedia = in_array( $info['type'], $multimediaTypes );
			$pathInfo = pathinfo( $filename );

			// If file is not a pdf AND not one of the multimedia types, run conversion
			if( !$isPDF && !$isMultimedia ) {
				$pdfConverter = new EDPDFConvert();
				$pdfConverter->setFilename( $filepathname );
				$filepathname = $pdfConverter->convert();
				if( $deposition->class != self::DEPOSITION_CLASS_DEMO ) {
					$pdfConverter->pdfCleanup( $filepathname );
				}
				$pdfConverter->reset();
				$filename = $pathInfo['filename'] . '.pdf';
				unlink( $info['fullname'] );
				unset( $pdfConverter );
			} elseif( $isMultimedia ) {	// If file is one of the multimedia types, convert it to our supported type
				$mmConvert = new \ClickBlocks\EDMMConvert();
				$ffprobe = new \ClickBlocks\FFProbe( $filepathname );
				if( $ffprobe->isVideo() ) {
					$isSupportedMP4 = $ffprobe->videoIsH264withAAC();
					if( !$isSupportedMP4 ) {
						$filename = $pathInfo['filename'] . '.mp4';
						$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.mp4';
						$success = $mmConvert->toMP4( $filepathname, $outPath );
						if( !$success ) {
							\ClickBlocks\Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . "; Conversion from {$filepathname} to {$outPath} failed." );
							throw new \Exception( 'Video conversion failed.' );
						}
						copy( $outPath, $filepathname );
						unlink( $outPath );
					}
				} else {
					//mp3
					if( !$ffprobe->audioIsMP3() ) {
						$filename = $pathInfo['filename'] . '.mp3';
						$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.mp3';
						$success = $mmConvert->toMP3( $filepathname, $outPath );
						if( !$success ) {
							\ClickBlocks\Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . "; Conversion from {$filepathname} to {$outPath} failed." );
							throw new \Exception( 'Audio conversion failed.' );
						}
						copy( $outPath, $filepathname );
						unlink( $outPath );
					}
				}
				unset( $mmConvert );
			}

			$now = date( 'Y-m-d H:i:s' );

			// copy the file into a permanent folder
			$file = new \ClickBlocks\DB\Files();
			$file->name = $filename;
			$file->setSourceFile( $filepathname );
			$file->created = $now;
			$file->folderID = $folder->ID;
			$file->sourceUserID = $this->user->ID;
			$file->isUpload = 1;

			// assign to deposition leader if attendee is member of Trusted Users, otherwise assign to attendee
			$file->createdBy = isset( $trustedUsers[$this->user->ID] ) ? $deposition->createdBy : $this->user->ID;

			if ( $folder->class == 'Exhibit' ) {
				$file->isExhibit = 1;
			}
			if ($folder->class == 'Transcript') {
				$file->isExhibit = 1;
				$file->isTranscript = 1;
			}

			$file->insert();
			if( $folder->lastModified != $now ) {
				$folder->lastModified = $now;
				$folder->update();
			}

			// Charge user for uploaded file
			if( !$deposition->isDemo() && !$deposition->isWitnessPrep() ) {
				DB\ServiceInvoiceCharges::charge( $this->case->clientID, self::PRICING_OPTION_UPLOAD_PER_DOC, 1, $deposition->caseID, $deposition->ID );
			}
		} catch (\Exception $e) {
			$results['success'] = false;
			$results['error'] = $e->getMessage();
			\ClickBlocks\Debug::ErrorLog( print_r( $e, TRUE ) );
		}

		//ED-691; Watermark Demo PDFs
		if( $deposition->class == 'Demo' && $results['success'] && !$isMultimedia ) {
			$inPath = $file->getFullName();
			$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID();
			//\ClickBlocks\Debug::ErrorLog( "folderUploadDocument; Watermark --\nin: {$inPath}\nout: {$outPath}\n" );
			if( \ClickBlocks\PDFTools::watermark( $inPath, $outPath ) ) {
				//\ClickBlocks\Debug::ErrorLog( "folderUploadDocument; Watermark -- success" );
				rename2( $outPath, $inPath );
			} else {
				//\ClickBlocks\Debug::ErrorLog( "folderUploadDocument; Watermark -- failure" );
				if( file_exists( $outPath ) ) {
					unlink( $outPath );
				}
			}
		}

		$this->ajax->script( 'var result = '.json_encode( (object)$results ) );
		$this->ajax->script( "selectFolder({$folder->ID});" );
	}

	public function folderUploadComplete( $folderID, $successCount )
	{
		$folder = (new DB\ServiceFolders())->getByID( $folderID );
		$deposition = (new DB\ServiceDepositions())->getByID( $folder->depositionID );
		if ($successCount > 0)
		{
			$this->notifyNodeJSUsers( $folder->ID );
		}
	}

	public function closePopup( $id )
	{
		$this->get( $id )->hide();
	}

	function viewFile( $fileID ) {
		$fileID = (int)$fileID;
		$file = new \ClickBlocks\DB\Files( $fileID );
        if( !$file || !$file->ID || $file->ID != $fileID ) {
            return;
        }
		$this->checkUserAccess( NULL, $file->folderID );

		if( !$file->mimeType ) {
			$file->setMetadata();
		}
		$mimeType = $file->mimeType;
		if( mb_stristr( $mimeType, 'audio' ) ) {
			$dlg = $this->get( 'viewAudioPopup' );
			$dlg->tpl->mimeType = $mimeType;
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile={$fileID}";
		} elseif( mb_stristr( $mimeType, 'video' ) ) {
			$dlg = $this->get( 'viewVideoPopup' );
			$dlg->tpl->mimeType = $mimeType;
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile={$fileID}";
		} else {
			$dlg = $this->get( 'viewFilePopup' );
			$dlg->tpl->src = "https://{$this->config->pdfviewer['host']}{$this->config->pdfviewer['path']}{$fileID}/viewer/";
		}
		$dlg->tpl->title = $file->name;
		$dlg->show();
    }

	function notifyNodeJSUsers( $folderID )
	{
		//getNodeJS()
		$node = new Utils\NodeJS( TRUE );
		if ($this->config->isDebug)
		{
			$logCallback = function( $text ) {
				$text = 'Calling NodeJS: ' . $text;
				@file_put_contents( Core\IO::dir( 'logs' ) . '/api.log', $text . PHP_EOL, FILE_APPEND );
			};
			$node->setLogCallback( $logCallback );
		}
		$node->notifyDepositionUpload( $folderID, $this->user->ID );
	}

	private function checkUserAccess( $depositionID=NULL, $folderID=NULL )
	{
		$depositionID = (int)$depositionID;
		$folderID = (int)$folderID;
		$isAllowed = FALSE;
		if ( $depositionID ) {
			$isAllowed = $this->getOrchestraDepositions()->checkUserAccessToDeposition( $this->user->ID, $depositionID );
			if (!$isAllowed) {
				throw new \LogicException( "Unable to find Deposition ID ({$depositionID})" );
			}
		} else {
			$folder = new \ClickBlocks\DB\Folders( $folderID );
			if( !$this->isTrustedUser ) {
				if( $folder->class == self::FOLDERS_EXHIBIT ) {
					$folderCreator = new \ClickBlocks\DB\Users( $folder->createdBy );
					if( $this->case->clientID !== $folderCreator->clientID ) {
						throw new \LogicException( "Unable to find Exhibit Folder ID ({$folderID})" );
					}
				} elseif( $folder->createdBy != $this->user->ID ) {
					throw new \LogicException( "Unable to find Folder ID ({$folderID})" );
				}
			}
		}
	}

	function downloadFolder($id) {
        $folder = foo(new DB\ServiceFolders())->getByID($id);
		$fileIDs = [];
		foreach( $folder->files as $file ) {
			$fileIDs[] = $file->ID;
		}
		$this->checkUserAccess( NULL, $folder->ID );
        if (!$folder->ID || empty($fileIDs)) {
           $this->showMessage('Folder is empty!', 'Download folder');
           return;
        }
        $name = $folder->name.'_'.time().'.zip';
        $fullName = Core\IO::dir('temp').'/'.$name;
        $zip = new \ZipArchive();
        $zip->open($fullName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        foreach ($fileIDs as $fileID) {
             $file = foo(new DB\ServiceFiles())->getByID($fileID);
             if (!$file->ID) continue;
             $zip->addFile($file->getFullName(),$file->name);
        }
        $zip->close();
        $this->ajax->downloadFile($fullName, $folder->name.'.zip');
    }

    function downloadSelectedFiles($fileIDs) {
        $deposition = foo(new DB\ServiceDepositions())->getByID($this->deposition->ID);
        $this->checkUserAccess( $deposition->ID );

        $ids = explode(",", $fileIDs);
        foreach ($ids as $fileID) {
            $file = foo(new DB\ServiceFiles())->getByID($fileID);
            if (!$file->ID) continue;
            $this->checkUserAccess( NULL, $file->folderID );
        }

        $name = $deposition->depositionOf.'_'.time().'.zip';
        $fullName = Core\IO::dir('temp').'/'.$name;
        $zip = new \ZipArchive();
        $zip->open($fullName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        foreach ($ids as $fileID) {
            $file = foo(new DB\ServiceFiles())->getByID($fileID);
            if (!$file->ID) continue;
            $zip->addFile($file->getFullName(),$file->name);
        }
        $zip->close();
        $this->ajax->downloadFile($fullName, $deposition->depositionOf.'.zip');
    }

	function downloadFile($id) {
        $file = (new DB\ServiceFiles())->getByID($id);
        if (!$file->ID) {
            return;
        }
        $this->checkUserAccess( NULL, $file->folderID );

        $this->ajax->downloadFile($file->getFullName(), $file->name);
    }

	function downloadDeposition()
	{
        $deposition = (new DB\ServiceDepositions())->getByID( $this->deposition->ID );
        $this->checkUserAccess( $deposition->ID );

        $fullName = Core\IO::dir('temp').'/'.$deposition->depositionOf.'_'.time().'.zip';
        $zip = new \ZipArchive();
        $zip->open($fullName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
		$this->addDepositionToArchive( $zip, $deposition->ID );
        $zip->close();
        if (!file_exists($fullName))
		{
			$this->showMessage('Deposition is empty!', 'Download Deposition');
		} else {
			$this->ajax->downloadFile( $fullName, $deposition->depositionOf.'.zip' );
		}
    }

	private function addDepositionToArchive( \ZipArchive &$archive, $depositionID, $caseZip=false )
	{
		$deposition = (new DB\ServiceDepositions())->getByID( $depositionID );
		$folders = (new DB\OrchestraFolders())->getFoldersByDepo( $deposition->ID );
		foreach ($folders as $folder)
		{
			if ($folder['class'] == DB\Folders::CLASS_COURTESYCOPY)
			{
				continue;
			}
			if ($folder['class'] == DB\Folders::CLASS_WITNESSANNOTATIONS && $this->user->ID != $deposition->ownerID)
			{
				continue;
			}
			if ($folder['class'] == DB\Folders::CLASS_PERSONAL && $this->user->ID != $folder['createdBy'])
			{
				continue;
			}
			$files = (new DB\OrchestraFiles())->getFilesByFolder( $folder['ID'] );
			foreach ($files as $file)
			{
				$f = (new DB\ServiceFiles())->getByID( $file['ID'] );
				$user = (new DB\ServiceUsers())->getByID( $f->createdBy );
				$client = (new DB\ServiceClients())->getByID( $this->case->clientID );
				if( $user->clientID !== $this->case->clientID && ( $user->clientID !== $client->resellerID && $f->createdBy !== $user->ID && $deposition->isDemo() ) )
				{
					continue;
				}
				$folderPath = $user->email.'/'.$folder['name'].'/'.$f->name;
				if ($caseZip)
				{
					// remove any symbols that cause conflict with folder/file naming conventions
					$depositionName = preg_replace( '/[<_>:"\/\\\|\?\*]/', '', $deposition->depositionOf );
					$folderPath = ($depositionName.'_'.$deposition->volume.'_'.$deposition->ID).'/'.$folderPath;
				}
				$archive->addFile( $f->getFullName(), $folderPath );
			}
		}
	}

	public function depoExhibitLog()
	{
		$deposition = new \ClickBlocks\DB\Depositions( $this->deposition->ID );
		$rows = $this->getOrchestraExhibitHistory()->exhibitHistoryForDeposition( $deposition->ID, TRUE );
		$dlg = $this->get( 'exhibitLogPopup' );
		$dlg->tpl->type = 'depo';
		$dlg->tpl->rows = $rows;
		$dlg->tpl->headingName = "{$deposition->depositionOf} (vol: {$deposition->volume})";
		$dlg->get( 'btnExport' )->visible = (count( $rows ));
		$dlg->get( 'btnExportWithExhibits' )->visible = (count( $rows ));
		$dlg->update();
		$dlg->show();
	}

	public function exportExhibitLog()
	{
		$dlg = $this->get( 'exhibitLogPopup' );
		$rows = $dlg->tpl->rows;
		if( !is_array( $rows ) || !$rows ) return;
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.csv';
		$fh = fopen( $outPath, 'w' );
		if( $dlg->tpl->type == 'case' ) {
			fputcsv( $fh, ['Time','Deposition Of', 'Volume','Exhibit Name','Introduced By','Original File Name'] );
			foreach( $rows as $row ) {
				$data = [$row['lIntroducedDate'], $row['depositionOf'], $row['volume'], $row['exhibitFilename'], $row['introducedBy'], $row['sourceFilename'] ];
				fputcsv( $fh, $data );
			}
		} else {
			fputcsv( $fh, ['Time','Exhibit Name','Introduced By','Original File Name'] );
			foreach( $rows as $row ) {
				$data = [$row['lIntroducedDate'], $row['exhibitFilename'], $row['introducedBy'], $row['sourceFilename'] ];
				fputcsv( $fh, $data );
			}
		}
		fclose( $fh );

        if( file_exists( $outPath ) ) {
			$uid = ($dlg->tpl->type == 'case') ? "case_{$rows[0]['caseID']}" : "deposition_{$rows[0]['depositionID']}";
			$this->ajax->downloadFile( $outPath, "{$uid}_exhibit_logs.csv", 'application/octet-stream', FALSE );
		} else {
			$this->showMessage( 'Error generating export', 'Export Exhibit Log' );
		}
	}

	public function exportExhibitLogWithExhibits()
	{
		$dlg = $this->get( 'exhibitLogPopup' );

		$folderIDs = [];
		if ( $dlg->tpl->type == 'case' ) {
			$rows = $dlg->tpl->rows;
			$depoIDs = [];
			foreach ( $rows as $row ) {
				$depoIDs[$row['depositionID']] = $row['depositionID'];
			}
			foreach ( $depoIDs as $depoID ) {
				$deposition = new \ClickBlocks\DB\Depositions( $depoID );
				if ( !$deposition || !$deposition->ID || $deposition->ID != $depoID ) {
					continue;
				}
				$officialExhibitsFolder = $this->getOrchestraFolders()->getExhibitFolder( $deposition->ID );
				$folderIDs[] = $officialExhibitsFolder['ID'];
			}
			$exhibitLogXLSPath = $this->createExhibitLogXLS(true);
			$downloadFolderName = $this->case->name . " Log and Exhibits";
		} else {
			$depoID = $this->deposition->ID;
			$deposition = new \ClickBlocks\DB\Depositions( $depoID );
			if ( !$deposition || !$deposition->ID || $deposition->ID != $depoID ) {
				return;
			}
			$officialExhibitsFolder = $this->getOrchestraFolders()->getExhibitFolder( $deposition->ID );
			$folderIDs[] = $officialExhibitsFolder['ID'];
			$downloadFolderName = $deposition->depositionOf . " ID-" . $deposition->ID . " vol-" . $deposition->volume;
			$exhibitLogXLSPath = $this->createExhibitLogXLS(false);
		}
		if (!$folderIDs) {
			return;
		}

		$this->downloadExhibitLogFolder($folderIDs, $downloadFolderName, $exhibitLogXLSPath);
	}

	public function createExhibitLogXLS($isCaseLog)
	{
		if ($isCaseLog) {
			$template = '/_templates/caseexhibitlog.xls';
		} else {
			$template = '/_templates/exhibitlog.xls';
		}
		$xls = \PHPExcel_IOFactory::load(Core\IO::dir('backend') . $template);
        $sheet = $xls->setActiveSheetIndex(0);
        $sheet->setTitle('Exhibit Log');
		$this->fillExhibitLogSpreadSheet( $sheet, $isCaseLog );
        $writer = new \PHPExcel_Writer_Excel5($xls);
        $fileName = Core\IO::dir('temp').'/'.\ClickBlocks\Utils::createUUID().'.xls';
        $writer->save($fileName);
        return $fileName;
	}

	public function fillExhibitLogSpreadSheet(\PHPExcel_Worksheet $sheet, $isCaseLog)
	{
		$dlg = $this->get( 'exhibitLogPopup' );
		$rows = $dlg->tpl->rows;
		if( !is_array( $rows ) || !$rows ) return;

		if ($isCaseLog) {
			$colValueIndexes = ['lIntroducedDate', 'depositionOf', 'exhibitFilename', 'introducedBy', 'sourceFilename'];
		} else {
			$colValueIndexes = ['lIntroducedDate', 'exhibitFilename', 'introducedBy', 'sourceFilename'];
		}

		// xls template has column titles in first row, start filling spreadsheet on second row
		$rowI = 2;

		foreach ($rows as $row)
		{
			foreach ($colValueIndexes as $i => $valueIndex)
			{
				if (!isset( $row[$valueIndex] ))
				{
					continue;
				}
				$sheet->getCellByColumnAndRow( $i, $rowI )->setValue( $row[$valueIndex] );
				if ($valueIndex == 'exhibitFilename')
				{
					$sheet->getCellByColumnAndRow( $i, $rowI )->getHyperlink($row[$valueIndex])->setUrl($row[$valueIndex]);
				}
			}
			$rowI++;
		}
	}

	private function downloadExhibitLogFolder( $folderIDs, $downloadFolderName, $exhibitLogFilePath ) {
		if (!$folderIDs) {
			return;
		}

		$archiveName = Core\IO::dir( 'temp' ) . DIRECTORY_SEPARATOR . $this->case->name . '_' . time() . '.zip';
		$zip = new \ZipArchive();
		$zip->open( $archiveName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE );

		$zip->addFile( $exhibitLogFilePath, 'Exhibit Log.xls' );

		foreach ($folderIDs as $folderID) {
			$folder = new \ClickBlocks\DB\Folders( $folderID );

			if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
				$this->showMessage('Folder not found!', 'Export Log With Exhibits');
				return;
			}
			$this->checkUserAccess( NULL, $folder->ID );

			$files = (new DB\OrchestraFiles())->getFilesByUserID( $folder->ID, $this->user->ID );
			if (count( $files ) < 1) {
				$this->showMessage( 'Folder is empty!', 'Export Log With Exhibits' );
				return;
			}
			foreach ($files as $fileRow) {
				if ( !$fileRow['ID'] ) {
					continue;
				}
				$zip->addFile( $folder->getFullPath().DIRECTORY_SEPARATOR.$fileRow['name'], $fileRow['name'] );
			}

		}
		$zip->close();

		$this->ajax->downloadFile( $archiveName, $downloadFolderName.'.zip' );
	}

	public function showNoFilesSelectedNotice() {
		$popup = $this->get('noticePopup');
		$popup->tpl->title = '';
        $popup->tpl->message = '<div style="text-align:center;">No files have been selected.<br/>Please select one or more files.</div>';
		$popup->get( 'btnCancel' )->value = 'OK';
        $popup->show();
	}

	public function annotateFile( $form_values )
	{
		$dlg = $this->get( 'saveFilePopup' );
		$overwrite_dlg = $this->get('ConfirmFileOverwritePopup');

		$annotate = new Annotate();
		$annotate_result = $annotate->annotateFile( 'session', $form_values, $this->deposition->ID, $this->user->ID, $dlg );

		if( $annotate_result && $annotate_result['file_exists_id'] && $form_values['overwrite'] !== 'overwrite' ) {
			$overwrite_dlg->get( 'btnCancel' )->value = 'Cancel';
			$overwrite_dlg->show();
			return;
		} elseif( $annotate_result ) {
			$dlg->hide();
			$overwrite_dlg->hide();
			$this->notifyNodeJSUsers( $annotate_result['fileID'] );
			$this->refreshFolders();
			$this->ajax->script( 'selectFolder(' . $annotate_result['folderID'] . ')' );

			$notice_dlg = $this->get( 'noticeSavePopup' );
			$notice_dlg->get( 'btnCancel' )->value = 'Ok';

			if( (int)$annotate_result[ 'fileID' ] === (int)$form_values[ 'sourceFileID' ] ) {
				$notice_dlg->tpl->message = 'The document was successfully resaved! ';
			} else {
				$notice_dlg->tpl->message = 'The document was successfully saved!';
				$this->ajax->script( 'updateFileDetails("' . $annotate_result['fileName'] . '",' . $annotate_result['fileID'] . ');' );
			}
			$notice_dlg->show();
		}
	}

//	public function showNoAttendeesSelectedNotice() {
//		$popup = $this->get('noticePopup');
//		$popup->tpl->title = '';
//        $popup->tpl->message = '<div style="text-align:center;">No attendees have been selected.<br/>Please select one or more attendees.</div>';
//		$popup->get( 'btnCancel' )->value = 'OK';
//        $popup->show();
//	}

//	public function sendEmails($type, $ids) {
//		$msg = $this->get('noticePopup');
//		$emails = [];
//		$attendees = $this->tpl->attendees;
//		foreach ($attendees as $attendee) {
//			if (in_array($attendee['ID'], $ids)) {
//				$emails[$attendee['email']] = $attendee['name'];
//			}
//		}
//		if (!(count($emails) > 0)) {
//			$msg->tpl->title = 'Please select your email(s).';
//		} else {
//			$attachments = array();
//			if ($type == 'exhibit' || $type == 'all') {
//				$folder = foo(new DB\OrchestraFolders())->getExhibitFolderObject($this->deposition->ID);
//				$zip = new \ZipArchive();
//				$zipfile = Core\IO::dir('temp').'/'.uniqid('zipemail').'.zip';
//				$zip->open($zipfile, \ZipArchive::CREATE);
//				foreach ($folder->files as $file) $zip->addFile($file->getFullName(), $file->name);
//				if ($zip->numFiles == 0) $zip->addEmptyDir('no files');
//				if ($zip->close() === FALSE) throw new \Exception('Could not create ZIP file at '.$zipfile.'. Check permissions.');
//				$attachments[$folder->name.'.zip'] = $zipfile;
//			}
//			if ($type == 'transcript' || $type == 'all') {
//				$folder = foo(new DB\OrchestraFolders())->getTranscriptFolderObject($this->deposition->ID);
//				$zip = new \ZipArchive();
//				$zipfile = Core\IO::dir('temp').'/'.uniqid('zipemail').'.zip';
//				$zip->open($zipfile, \ZipArchive::CREATE);
//				foreach ($folder->files as $file) $zip->addFile($file->getFullName(), $file->name);
//				if ($zip->numFiles == 0) $zip->addEmptyDir('no files');
//				if ($zip->close() === FALSE) throw new \Exception('Could not create ZIP file at '.$zipfile.'. Check permissions.');
//				$attachments[$folder->name.'.zip'] = $zipfile;
//
////				$attachments[] = foo(new DB\ServiceFiles())->getByID($this->tpl->file['ID'])->getFullName();
//			}
//			switch ($type) {
//				case 'all': $result = Utils\EmailGenerator::sendTranscriptAndExhibits($emails, $attachments); break;
//				case 'transcript': $result = Utils\EmailGenerator::sendOfficialTranscript($emails, $attachments); break;
//				case 'exhibit': $result = Utils\EmailGenerator::sendExhibits($emails, $attachments); break;
//			}
//			if ($type == 'exhibit' || $type == 'all') {
//				unlink($zipfile);
//			}
//			if (count($result) > 0) {
//				$msg->tpl->title = 'Error sending email(s):'.implode(',',$result);
//			} else {
//				$msg->tpl->title = 'Sent email(s) successful.';
//			}
//		}
//
//		$msg->show();
//	}
}

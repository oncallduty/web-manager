<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web\UI\Helpers;

class PageCourtClientEdit extends Backend {

	/**
	 * Client
	 * @var \ClickBlocks\DB\Clients
	 */
	private $client = NULL;

	/**
	 * Client Admin
	 * @var \ClickBlocks\DB\Users
	 */
	private $clientAdmin = NULL;

	/**
	 * User is [Super] Admin
	 * @var boolean
	 */
	private $isEdepozeAdmin = FALSE;

	/**
	 * Add or Edit
	 * @var boolean
	 */
	private $isEdit = FALSE;

	public function __construct() {
		parent::__construct( 'clients/courtEdit.html' );

		$clientID = isset( $this->fv['ID'] ) ? (int)$this->fv['ID'] : 0;
		$this->client = new DB\Clients( $clientID );
		if( $clientID ) {
			if( $this->client && $this->client->ID == $clientID ) {
				$this->isEdit = TRUE;
				$clientAdminID = $this->getOrchestraUsers()->getClientAdminID( $this->client->ID );
				$this->clientAdmin = new DB\Users( $clientAdminID );
			} else {
				$this->client = NULL;
				$this->clientAdmin = NULL;
				$this->redirect( $this->basePath . '/courtclients' );
			}
		}
		$this->isEdepozeAdmin = in_array( $this->user->typeID, [self::USER_TYPE_SUPERADMIN, self::USER_TYPE_ADMIN] );
	}

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		return $this->isEdepozeAdmin;
	}

	public function init() {
		parent::init();
		$this->js->add( new Helpers\Script( NULL, NULL, Core\IO::url( 'backend-js' ) . '/sha1.js' ), 'link' );
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/country_toggle.js' ), 'link' );
		$this->head->name = $this->tpl->headName = ($this->isEdit ? 'Edit' : 'Add') . ': Court Client';
		$this->tpl->tab = 1;
		$this->tpl->isEdit = $this->isEdit;
		$this->tpl->adminURL = $this->config->adminPath;
		$this->tpl->cancelLink = $this->config->adminPath . '/courtclients';

		$statesSelect = self::getUSAStates();
		$step1 = $this->get( 'step1' );
		$step2 = $this->get( 'step2' );
		$step1StateCtrl = $step1->get( 'state' );
        $step1StateCtrl->options = $statesSelect;
        $step1StateCtrl->value = reset( $statesSelect );

		if( $this->isEdit ) {
			//Edit
            $this->tpl->headerName = $this->client->name;
			$this->tpl->deactivated = $this->client->deactivated;
            $step2->get( 'valpassword' )->groups = 'ignore';
            $step2->get( 'valconfirmpassword' )->groups = 'ignore';

			//step1
			$countriesSelect = self::getCountries(false);
			$statesSelect = self::getUSAStates();
			$provincesSelect = self::getCanadianProvinces();
            $step1->get( 'deactivated' )->value = $this->client->deactivated ? '1' : '0';
            $step1->get( 'name' )->value = $this->client->name;
			$step1->get( 'salesRep' )->value = $this->client->salesRep;
            $step1->get( 'startDate' )->value = date_format( date_create( $this->client->startDate ), 'm/d/Y' );
			$step1->get( 'location' )->value = $this->client->location;
            $step1->get( 'contactName' )->value = $this->client->contactName;
            $step1->get( 'contactPhone' )->value = \ClickBlocks\Utils::formatPhone( $this->client->contactPhone );
            $step1->get( 'contactEmail' )->value = $this->client->contactEmail;
            $step1->get( 'address1' )->value = $this->client->address1;
            $step1->get( 'address2' )->value = $this->client->address2;
            $step1->get( 'city' )->value = $this->client->city;
            $step1->get( 'ZIP' )->value = $this->client->ZIP;
            $step1->get( 'state' )->value = $this->client->state;
            $step1->get( 'comments' )->text = $this->client->description;
			$step1->get('countryCode')->options = $countriesSelect;
			$step1->get('countryCode')->value = reset($countriesSelect);
			$step1->get('state')->options = $statesSelect;
			$step1->get('province')->options = $provincesSelect;
			$step1->get('postCode')->value = $this->client->ZIP;
			$step1->get('state')->value = $this->client->state;
			$step1->get('province')->value = $this->client->state;
			$step1->get('region')->value = $this->client->region;
			$step1->get('countryCode')->value = $this->client->countryCode;

			//step2
			$step2->get( 'firstName' )->value = $this->clientAdmin->firstName;
			$step2->get( 'lastName' )->value = $this->clientAdmin->lastName;
			$step2->get( 'email' )->value = $this->clientAdmin->email;
			$step2->get( 'username' )->value = $this->clientAdmin->username;

			$svcUserAuth = new DB\ServiceUserAuth();
			$auth = $svcUserAuth->getByUserID( $this->clientAdmin->ID );
			$this->tpl->isAccountLocked = ($auth->hardLock !== NULL);
        } else {
			//Add
			$step1->get('countryCode')->options = self::getCountries(false);
			$step1->get('state')->options = self::getUSAStates();
			$step1->get('province')->options = self::getCanadianProvinces();

            $this->tpl->headerName = 'Add: Court Client';
            $step1->get( 'startDate' )->value = date( 'm/d/Y' );
        }

		$this->openTab( 1 );
	}

	public function openTab( $step ) {
        $this->ajax->script( "controls.display('" . $this->get( 'step1' )->uniqueID . "', 'none');" );
        $this->ajax->script( "controls.display('" . $this->get( 'step2' )->uniqueID . "', 'none');" );
		$this->ajax->script( "controls.display('" . $this->get( "step{$step}" )->uniqueID . "', '');" );
    }

	public function toStep2( $fv ) {
		if( $this->validators->isValid( 'step1' ) ) {
			$this->ajax->script( "controls.display('" . $this->get( 'step1' )->uniqueID . "', 'none');" );
			$this->ajax->script( "controls.display('" . $this->get( 'step2' )->uniqueID . "', '');" );
			$this->ajax->script( "changeTab( 0, 2 )" );
		}
    }

	public function toStep1( $fv ) {
		$this->ajax->script( "controls.display('" . $this->get( 'step2' )->uniqueID . "', 'none');" );
		$this->ajax->script( "controls.display('" . $this->get( 'step1' )->uniqueID . "', '');" );
		$this->ajax->script( "changeTab( 0, 1 )" );
	}

	public function checkUsername( $ctrls ) {
        return $this->getOrchestraUsers()->checkUniqueUsername( $this->getByUniqueID( $ctrls[0] )->value, $this->clientAdmin->ID );
    }

	public function save() {
        $this->ajax->script( 'btnLock = false' );

		$fv['name'] = $this->get( 'step1.name' )->value;
		$fv['username'] = $this->get( 'step2.username' )->value;

        $isValid = TRUE;
		$isValid &= $this->validators->isValid( 'step1' );
		$isValid &= $this->validators->isValid( 'step2' );

		switch( $this->get('step1')->get('countryCode')->value ) {
			case 'US':
				$isValid &= $this->validators->isValid( 'countryUS' );
				break;
			case 'CA':
				$isValid &= $this->validators->isValid( 'countryCA' );
				break;
			case 'OT':
				$isValid &= $this->validators->isValid( 'countryOT' );
				break;
		}

        if( !$isValid ) {
			return;
		}

        $client = $this->client;
        $user = new DB\Users();
		$now = date( 'Y-m-d H:i:s' );

		$step1 = $this->get( 'step1' );
        $client->name = $step1->get( 'name' )->value;
		$client->salesRep = $step1->get('salesRep')->value;
        $client->startDate = date_format( date_create_from_format( 'm/d/Y', $step1->get( 'startDate' )->value ), 'Y-m-d' );
        $client->deactivated = $step1->get( 'deactivated' )->value == '1' ? $now : NULL;
		$client->location = trim( $step1->get( 'location' )->value ) == FALSE ? NULL : $step1->get( 'location' )->value;
        $client->contactName = $step1->get( 'contactName' )->value;
        $client->contactPhone = $step1->get( 'contactPhone' )->value;
        $client->contactEmail = $step1->get( 'contactEmail' )->value;
		$client->countryCode = $step1->get( 'countryCode' )->value;
        $client->address1 = $step1->get( 'address1' )->value;
        $client->address2 = $step1->get( 'address2' )->value;
        $client->city = $step1->get( 'city' )->value;
        $client->state = $step1->get( 'state' )->value;
        $client->ZIP = $step1->get( 'ZIP' )->value;
		$client->region = $step1->get( 'region' )->value;
		$client->description = $step1->get( 'description' )->text;
        $client->casesDemoDefault = 0;

		switch( $step1->get('countryCode')->value ) {
			case 'US':
				$client->region = '';
				break;
			case 'CA':
				$client->state = $step1->get('province')->value;
				$client->ZIP = $step1->get('postCode')->value;
				$client->region = '';
				break;
			case 'OT':
				$client->state = '';
				$client->ZIP = $step1->get('postCode')->value;
				break;
		}

		$client->typeID = self::CLIENT_TYPE_COURTCLIENT;
		if( $this->isEdit ) {
			$client->update();
			$user = $this->clientAdmin;
		} else {
			$client->created = $now;
			$client->save();
		}

		// Data from step 2
		$step2 = $this->get( 'step2' );
		$user->email = $step2->get( 'email' )->value;
		$user->username = $step2->get( 'username' )->value;
		$user->firstName = $step2->get( 'firstName' )->value;
		$user->lastName = $step2->get( 'lastName' )->value;
		$user->countryCode = 'US';
		$user->typeID = self::USER_TYPE_CLIENT;
		$user->clientAdmin = TRUE;
		if( $this->isEdit ) {
			$user->update();
		} else {
			$user->created = $now;
			$user->clientID = $client->ID;
			$user->insert();
		}
		$password = $step2->get( 'passauth' )->value;
		if( $password ) {
			DB\UserAuth::updatePassword( $user->ID, $password, TRUE );
		}
		$this->ajax->redirect( $this->config->adminPath . '/courtclients' );
    }

	public function confirmDeleteAccount() {
		if( !$this->client->ID ) {
			return;
		}
		$dialogInfo = [
			'title' => 'Delete Court Client',
			'warning' => 'Warning!!! This is not recoverable! If you delete this account, all the information related to this account will be lost, including their files.',
			'message' => 'Are you sure you would like to delete this court client?',
			'OKName' => 'Delete',
			'OKMethod' => "deleteAccount(1);"
		];
		$this->showPopup( 'confirm', $dialogInfo );
	}

	public function deleteAccount() {
		// Hide dialog popup
		$this->hidePopup('confirm');
		$this->client->setAsDeleted();
		$this->ajax->redirect( "{$this->config->adminPath}/courtclients" );
	}
}

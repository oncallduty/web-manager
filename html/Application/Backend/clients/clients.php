<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageClients extends Backend
{

    public function __construct()
    {
       parent::__construct("clients/clients.html");
    }

	public function access() {
		if( !parent::access() ) {
			return false;
		}
		$this->noAccessURL = $this->basePath . '/';
		return in_array( $this->user->typeID, [self::USER_TYPE_SUPERADMIN, self::USER_TYPE_ADMIN, self::USER_TYPE_RESELLER] );
	}

    public function init()
    {
        parent::init();
        $this->tpl->tab = 1;
        $this->head->name = 'Reseller Management';

        if ($this->user->typeID == self::USER_TYPE_RESELLER){
            $this->head->name = 'Client Management';
            $this->tpl->headName = 'Client Management';
            $this->get('clients')->typeID = self::CLIENT_TYPE_CLIENT;
            $this->get('clients')->resellerID = $this->reg->reseller['ID'];
            $this->tpl->isAdmin = false;
            $this->tpl->url = mb_strtolower( $this->reg->reseller['URL'] );
        } else {
            $this->head->name = 'Reseller Management';
            $this->tpl->headName = 'Reseller Management';
            $this->tpl->isAdmin = true;
			$this->get('clients')->typeID = self::CLIENT_TYPE_RESELLER;
        }
        $this->get('clients')->update();
    }

	public function search()
	{
		$txt = $this->get( 'searchText' );
		$this->get( 'clients' )->searchValue = trim( $txt->value );
		$this->get( 'clients' )->update();
	}
}

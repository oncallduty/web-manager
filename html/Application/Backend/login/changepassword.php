<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageChangePassword extends Backend
{
    protected $request;
    const REQUEST_LIFETIME = 172800;

	public function __construct()
	{
		parent::__construct( 'login/changepassword.html' );

		if ($_GET['hash'])
		{
			$requestID = (new DB\OrchestraPasswordResetRequests())->getHashByPassword( $_GET['hash'] );
			if (!empty( $requestID ))
			{
				$this->request = new DB\PasswordResetRequests( $requestID['hash'] );
			}
		}
	}

	public function access()
	{
		$this->noAccessURL = $this->basePath.'/login';
		if (!$this->request || !$this->request->isKeyFilled() || $this->request->finished!==null)
		{
			return false;
		}

		$ts = (int)Utils\DT::format($this->request->created, 'Y-m-d H:i:s', 'U');
		$lifetime = $this->config->passwordRequestLifetime ?: self::REQUEST_LIFETIME;
		return (time() < ($ts + $lifetime));
	}

	public function init() {
		parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url('backend-js').'/sha1.js' ), 'link' );
		$this->head->name = 'Change Password';
	}

	public function changePass($fv)
	{
		if( !$this->validators->isValid( 'default' ) ) return;

		DB\UserAuth::updatePassword( $this->request->userID, $fv['passauth'], true );

		$this->request->finished = 'NOW()';
		$this->request->update();

		$this->ajax->redirect( $this->basePath.'/login' );
	}

	public function validatePassword( $ctrls )
	{
		$password = $this->getByUniqueID( $ctrls[0] )->value;
		if (strlen( $password ) < 1)
		{
			return true;
		}
		return DB\UserAuth::validatePassword( $password );
	}
}
?>


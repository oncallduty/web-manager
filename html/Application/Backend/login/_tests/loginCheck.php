<?php

namespace ClickBlocks\UnitTest\MVC\Backend\PageLogin;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\API,
    ClickBlocks\UnitTest;

/**
 * @group logout
 */
class loginCheckTest extends UnitTest\MVC\PageMethodTest
{
   protected function getPageClassName() {
      return '\ClickBlocks\MVC\Backend\PageLogin';
   }

   protected static $okParams = array('email'=>'test@example.com', 'password'=>'pass');


   public function setUp()
   {
     parent::setUp();
     //$this->setUp
     $this->orc = $this->setUpMockOrchestra('Users');
     $reg = Core\Register::getInstance();
     $reg->uri->path[0] = substr($reg->config->adminPath,1);
   }

   public function test_login_admin()
   {
     $this->orc->expects(self::once())->method('loginAdmin')->will(self::returnValue(
         array('ID'=>'123','typeID'=>'C')
     ));
     $this->setUpPageProperty('afv', self::$okParams);
     $res=$this->invokeThis();
     self::assertTrue($res);
   }

   /*
   public function test_login_reseller()
   {
      self::$reg->uri->path[0] = 'someone';
     $this->orc->expects(self::once())->method('loginReseller')->will(self::returnValue(
         array('ID'=>'123','typeID'=>'R')
     ));
     $this->setUpPageProperty('afv', self::$okParams);
     $res=$this->invokeThis();
     self::assertTrue($res);
   }

   public function test_login_failed()
   {
      self::$reg->uri->path[0] = 'someone';
     $this->orc->expects(self::once())->method('loginReseller')->will(self::returnValue(
         null
     ));
     $this->setUpPageProperty('afv', self::$okParams);
     $res=$this->invokeThis();
     self::assertFalse($res);
   }
	*/

}

?>
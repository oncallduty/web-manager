<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageInvoiceView extends Backend {

    protected $clients;
    protected $invoices;
    public function __construct() {
        $print = $this->isPrintMode();
        parent::__construct("invoices/invoiceView" . ($print ? '_print' : '') . ".html");

		if ( isset( $this->fv['cID'] ) )
		{
			$i = 0;
			foreach ((array) $this->fv['cID'] as $clientID) {
				$this->checkClientAccess( $clientID );
				$this->clients[$i] = new DB\Clients;
				$this->clients[$i]->assignByID($clientID);
				$invoiceID = foo(new DB\OrchestraInvoices())->getLastInvoiceByClient($this->clients[$i]->ID);
				$this->invoices[$i] = $invoiceID;
				$i++;
			}
		}
		else
		{
			$this->checkClientAccess( $this->fv['clientID'] );
		}
    }

	private function checkClientAccess( $clientID )
	{
		$clientID = (int)$clientID;
		$client = new DB\Clients;
        $client->assignByID( $clientID );
		switch( strtolower( $this->user->typeID ) ):
			case 'r':
				if ( $client->resellerID != $this->user->clientID )
				{
					$client = NULL;
					throw new \LogicException( "Client ID ({$clientID}) not found" );
				}
				break;
			default:
				break;
		endswitch;
	}

    public function init() {
        parent::init();
        $this->head->name = 'Reports';
        if ($this->user->typeID == self::USER_TYPE_RESELLER) {
            $this->tpl->headName = 'Client Reports';
        } else {
            $this->tpl->headName = 'Reseller Reports';
            $this->tpl->isAdmin = true;
        }

        $this->tpl->url = $this->basePath;

        if (isset($this->fv['ID'])) {
            $invoiceID = (int) $this->fv['ID'];
            $invoice = foo(new DB\ServiceInvoices())->getByID($invoiceID);
            $client = foo(new DB\ServiceClients())->getByID($invoice->clientID);
        } else {
            $clientID = isset($this->fv['clientID']) ? (int) $this->fv['clientID'] : 0;
            $client = foo(new DB\ServiceClients())->getByID($clientID);
            if (!$this->isPrintMode()) {
                $this->get('startDate')->value = \DateTime::createFromFormat('Ymd', $this->fv['startDate'])->format('m/d/Y');
                $this->get('endDate')->value = \DateTime::createFromFormat('Ymd', $this->fv['endDate'])->format('m/d/Y');
            }
        }

        if ($client->ID) $this->clients[count($this->clients)] = $client;
        $this->get('repeat')->count = count($this->clients);
        $this->get('invoicecharges')->disabled = true;
        foreach ($this->clients as $i => $c) {
            $panel = $this->get('client_' . $i);
            $widget = $panel->get('invoicecharges');
            if ($this->isPrintMode())
                $widget->setTemplate(Core\IO::dir('backend') . '/widgets/invoices/invoiceCharges_print.html');
            if (isset($this->fv['cID'])) {
                $invoiceID = $this->invoices[$i];
            }
            if ($invoiceID){
                $widget->ID = $invoiceID;
                $invoice = foo(new DB\ServiceInvoices())->getByID($invoiceID);
                $panel->tpl->startDate = date_format(date_create($invoice->startDate), 'm/d/Y');
                $panel->tpl->endDate = date_format(date_create($invoice->endDate), 'm/d/Y');
                $panel->tpl->invoiceID = $invoiceID;
            } elseif (!isset($this->fv['cID'])) {
                $widget->clientID = isset($this->fv['clientID']) ? (int) $this->fv['clientID'] : 0;
                $widget->startDate = $this->fv['startDate'];
                $widget->endDate = $this->fv['endDate'];
                $panel->tpl->startDate = \DateTime::createFromFormat('Ymd', $this->fv['startDate'])->format('m/d/Y');
                $panel->tpl->endDate = \DateTime::createFromFormat('Ymd', $this->fv['endDate'])->format('m/d/Y');
            }
            $panel->tpl->client = $c;
            $panel->tpl->isLast = ($i == count($this->clients)-1);
        }

        $this->tpl->clientName = $client->name;
        $this->tpl->clientID = $client->ID;
        $this->tpl->contact = $client->contactName;
        $this->tpl->sinceDate = date_format(date_create($client->startDate), 'm/d/Y');
        $client->contactPhone = \ClickBlocks\Utils::formatPhone($client->contactPhone);
        $this->tpl->phone = $client->contactPhone;
        $this->tpl->email = $client->contactEmail;
        $this->tpl->address = $client->address1;
        $this->tpl->city = $client->city . ', ' . $client->state;
    }

    public function runInvoice() {
        $viewLink = '';
        if ($this->tpl->isAdmin) {
            $viewLink .= '/admin/report/';
        } else {
            $viewLink .= '/' . mb_strtolower( $this->reg->reseller['URL'] ) . '/report/';
        }
        $viewLink .= 'invoice?clientID=' . $this->tpl->clientID;

        $viewLink .= '&startDate=' . date_format(date_create($this->get('startDate')->value), 'Ymd');
        $viewLink .= '&endDate=' . date_format(date_create($this->get('endDate')->value), 'Ymd');

        $this->ajax->redirect($viewLink);
    }

    public function savePDF() {
        $this->downloadPageAsPDF();
    }

    public function isPrintable() {
        return true;
    }

}
?>


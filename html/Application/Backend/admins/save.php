<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

class PageAdminEdit extends Backend {
	protected $userAdmin = null;

	public function __construct() {
		parent::__construct( 'admins/save.html' );

		$id = isset( $this->fv['ID'] ) ? (int) $this->fv['ID'] : 0;
		$this->userAdmin = foo( new DB\ServiceUsers )->getByID( $id );
	}

	public function init() {
		parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/sha1.js' ), 'link' );
		$this->tpl->showTabs = false;
		$this->tpl->isNew = true;

		if( $this->userAdmin->ID > 0 ) {
			$this->head->name = $this->tpl->htitle = 'Edit Admin';
			$this->tpl->isNew = false;
			$this->tpl->ID = $this->userAdmin->ID;

			$this->get( 'aname' )->value = $this->userAdmin->firstName.' '.$this->userAdmin->lastName;
			$this->get( 'email' )->value = $this->userAdmin->email;
			$this->get( 'username' )->value = $this->userAdmin->username;
			$this->get( 'valFields' )->controls = 'aname,email,username';
		} else {
			$this->head->name = $this->tpl->htitle = 'Add New Admin';
			$this->get( 'aname' )->value = '';
			$this->get( 'email' )->value = '';
		}
	}

	public function access() {
		if( !parent::access() ) {
			return false;
		}
		$this->noAccessURL = $this->basePath.'/';
		return ( $this->user->typeID == self::USER_TYPE_SUPERADMIN );
	}

	public function checkUsername($ctrls) {
		$value = $this->getByUniqueID($ctrls[0])->value;
		if( !empty( $this->userAdmin->email ) && $this->userAdmin->email == $value ) {
			return true;
		}
		return foo(new DB\OrchestraUsers)->checkUniqueUsername($value, $this->userAdmin->ID);
	}

	public function save(array $params) {
		$this->ajax->script('btnLock = false');
		if( !$this->validators->isValid() ) {
			return;
		}

		if( isset( $params['password'] ) && $params['password'] != $params['confirmPassword'] ) {
			$this->get('password')->class = 'w_220 error';
			$this->get('confirmPassword')->class = 'w_220 error';
			$this->get('password')->update();
			$this->get('confirmPassword')->update();
			return;
		}

		if( !isset( $params['password'] ) ) {
			unset( $params['password'] );
		}

		$name = explode( " ", $params['aname'] );
		$params['firstName'] = $name[0];
		array_shift( $name );
		$params['lastName'] = implode(" ", $name);
		$params['countryCode'] = 'US';
		$params['typeID'] = \ClickBlocks\MVC\Edepo::USER_TYPE_ADMIN;
		if( $this->tpl->isNew ) {
			$params['created'] = 'NOW()';
		}
		$this->userAdmin->setValues($params);
		foo( new DB\ServiceUsers() )->save( $this->userAdmin );

		$password = $this->get('passauth')->value;
		if( $password ) {
			DB\UserAuth::updatePassword( $this->userAdmin->ID, $password, TRUE );
		}

		$this->ajax->redirect($this->basePath.'/admins');
	}

	public function deleteUser($ID) {
		$user = $this->userAdmin;
		if( (int)$user->ID == 0 ) {
			return;
		}
		$this->get( 'confirmPopup' )->hide();
		$user->delete();
		$this->ajax->redirect( $this->basePath.'/admins' );
	}

	public function showDelPopup($ID = null) {
		$user = foo( new DB\ServiceUsers() )->getByID( $ID );
		$popup = $this->get( 'confirmPopup' );
		$this->cleanByUniqueID( $popup->uniqueID );
		$popup->get( 'btnOK' )->value = 'Delete';
		$popup->get( 'btnOK' )->onclick = 'ajax.doit(\'->deleteUser\', \'' . (int)$user->ID . '\');';
		$popup->tpl->title = 'Delete Admin?';
		$popup->tpl->message = 'Are you sure you want to delete <b>' . $user->firstName . ' ' . $user->lastName.'</b>?';
		$popup->show();
	}

}

<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\API\IEdepoBase,
	ClickBlocks\Utils\EmailGenerator,
	ClickBlocks\Debug;

class PageSignup extends Backend {

	public function __construct() {
        parent::__construct( 'signup/signup.html' );
    }

    public function access() {
		$this->noAccessURL = '/';
		if( !$this->config->signup['resellerID'] ) {
			return FALSE;
		}
		return !isset( $this->reg->reseller );
    }

    public function init() {
        parent::init();
		$this->js->add( new Helpers\Script( NULL, NULL, Core\IO::url( 'backend-js' ) . '/sha1.js' ), 'link' );
		$this->css->add( new Helpers\Style( 'signup-general', NULL, Core\IO::url( 'backend-css' ) . '/signup-general.css' ), 'link' );
		$this->head->addMeta( new Helpers\Meta( 'viewport', 'viewport', 'width=device-width, initial-scale=1.0' ) );
		$this->head->name = 'Sign up';

		$countries = self::getCountries( FALSE );
		array_shift( $countries ); // remove first (empty) item
		$states = self::getUSAStates( TRUE );
		$states[''] = 'State'; // relabel first (empty) item
		$provinces = self::getCanadianProvinces( TRUE );
		$provinces[''] = 'Province'; // relabel first (empty) item

		$this->get( 'countrycode' )->options = $countries;
		$this->get( 'countrycode' )->value = reset( $countries );
		$this->get( 'state' )->options = $states;
		$this->get( 'state' )->value = reset( $states );
		$this->get( 'province' )->options = $provinces;
		$this->get( 'province' )->value = reset( $provinces );
	}

	public function checkUsername( $ctrls ) {
		return $this->getOrchestraUsers()->checkUniqueUsername( $this->getByUniqueID( $ctrls[0] )->value );
    }

	public function signup( $fv ) {
		array_walk( $fv, 'trim' );
		$isValid = $this->validators->isValid( 'signup' );
		switch( $fv['countrycode'] ) {
			case 'CA':
				$isValid &= $this->validators->isValid( 'countryCA' );
				break;
			case 'OT':
				$isValid &= $this->validators->isValid( 'countryOT' );
				break;
			case 'US':
			default:
				$isValid &= $this->validators->isValid( 'countryUS' );
				break;
		}
		if( !$isValid ) {
			Debug::ErrorLog( 'PageSignup::signup; failed form validation' );
			return;
		}
		$reseller = new DB\Clients( $this->config->signup['resellerID'] );
		if( !$reseller || !$reseller->ID || $reseller->ID != $this->config->signup['resellerID'] || $reseller->typeID !== IEdepoBase::CLIENT_TYPE_RESELLER ) {
			$this->showFailureDialog();
			return;
		}

		$now = date( 'Y-m-d H:i:s' );
		$email = mb_strtolower( trim( $fv['email'] ) );

		// client
		$client = new DB\Clients();
		$client->typeID = IEdepoBase::CLIENT_TYPE_CLIENT;
		$client->resellerID = $reseller->ID;
		$client->name = ($fv['firm'] ? $fv['firm'] : $fv['fullName']);
		$client->startDate = $now;
		$client->created = $now;
		$client->contactName = $fv['fullName'];
		$client->contactPhone = '';
		$client->contactEmail = $email;
		$client->address1 = $fv['address1'];
		$client->address2 = ($fv['address2'] ? $fv['address2'] : NULL);
		$client->city = $fv['city'];
		$client->state = ($fv['state'] ? $fv['state'] : $fv['province'] ? $fv['province'] : '');
		$client->region = ($fv['region'] ? $fv['region'] : NULL);
		$client->ZIP = ($fv['zipcode'] ? $fv['zipcode'] : $fv['postalcode'] ? $fv['postalcode'] : '');
		$client->countryCode = ($fv['countrycode'] ? $fv['countrycode'] : 'US');
		$client->casesDemoDefault = 1;
		$client->insert();
		if( !$client->ID ) {
			$this->showFailureDialog();
			return;
		}

		// client admin, user
		$name = preg_split( '/\s+(?=\S*+$)/', $fv['fullName'] );
		$cAdmin = new DB\Users();
		$cAdmin->typeID = IEdepoBase::USER_TYPE_CLIENT;
		$cAdmin->clientID = $client->ID;
		$cAdmin->firstName = trim( $name[0] );
		$cAdmin->lastName = (isset( $name[1] ) ? trim( $name[1] ) : '');
		$cAdmin->email = $email;
		$cAdmin->username = $email;
		$cAdmin->address1 = $client->address1;
		$cAdmin->address2 = $client->address2;
		$cAdmin->city = $client->city;
		$cAdmin->state = $client->state;
		$cAdmin->region = $client->region;
		$cAdmin->ZIP = $client->ZIP;
		$cAdmin->countryCode = $client->countryCode;
		$cAdmin->created = $now;
		$cAdmin->termsOfService = 0;
		$cAdmin->clientAdmin = 1;
		$cAdmin->insert();
		if( !$cAdmin->ID ) {
			$this->showFailureDialog();
			return;
		}
		$cAdmin->activated = 0; // deactivate until email validation
		$cAdmin->update();

		DB\UserAuth::updatePassword( $cAdmin->ID, $fv['password'] );

		self::sendActivationEmail( $email );

		$countries = self::getCountries( FALSE );
		$msg = "The following info was submitted for an account:\r\n\r\n";
		$msg .= "Full Name: {$cAdmin->firstName} {$cAdmin->lastName}\r\n";
		$msg .= ($fv['firm'] ? "Firm: {$fv['firm']}\r\n" : '');
		$msg .= "Email: {$email}\r\n";
		$msg .= "Address: {$cAdmin->address1}\r\n";
		$msg .= ($cAdmin->address2 ? "Address: {$cAdmin->address2}\r\n" : '');
		$msg .= "City: {$cAdmin->city}\r\n";
		$msg .= ($cAdmin->countryCode == 'US' ? "State: {$cAdmin->state}\r\n" : '');
		$msg .= ($cAdmin->countryCode == 'CA' ? "Province: {$cAdmin->state}\r\n" : '');
		$msg .= ($cAdmin->countryCode == 'OT' ? "Region: {$cAdmin->region}\r\n" : '');
		$msg .= ($cAdmin->countryCode == 'US' ? "Zipcode: {$cAdmin->ZIP}\r\n" : '');
		$msg .= (in_array( $cAdmin->countryCode, ['CA','OT'] )  ? "Postal Code: {$cAdmin->ZIP}\r\n" : '');
		$msg .= "Country: {$countries[$cAdmin->countryCode]}\r\n";
		mail( $this->config->signup['notify'], "Account signup submission: {$cAdmin->firstName} {$cAdmin->lastName}", $msg, "From: {$this->config->email['fromName']} <{$this->config->email['fromEmail']}>" );

		$this->ajax->redirect( "{$this->config->signupPath}/submission" );
	}

	protected function showFailureDialog() {
		$dlg = $this->get( 'confirmDialog' );
		$dlg->tpl->message = 'Unable to process your sign up at this time, please try again later';
		$dlg->get( 'btnOK' )->visible = FALSE;
		$dlg->get( 'btnCancel' )->value = 'OK';
		$dlg->show();
	}

	public static function sendActivationEmail( $email ) {
		//Debug::ErrorLog( "PageSignup::sendActivationEmail; {$email}" );
		$orcUsers = new DB\OrchestraUsers();
		$user = $orcUsers->getUnactivatedUser( $email );
		if( $user && $user->email ) {
			EmailGenerator::sendAccountActivationEmail( $user );
		}
	}
}

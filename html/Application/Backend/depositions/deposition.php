<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
ClickBlocks\DB,
ClickBlocks\Web,
ClickBlocks\Web\UI\POM,
ClickBlocks\Web\UI\Helpers,
ClickBlocks\Utils;

//require_once 'vendor/autoload.php';


define('CLIENT_ID', 'z8bAdu8uQqiNKp9DRBXz5A');
define('CLIENT_SECRET', 'YdGUg6Mph7qzABOiZJHqEgPwvJI3jeSc');
define('REDIRECT_URI', 'https://staging2.edepoze.com/zoom-demo/callback.php');

class PageDepositionEdit extends Backend
{
	/**
	 * @var \ClickBlocks\DB\Depositions
	 */
	protected $deposition = NULL;
	protected $case = null;
	protected $exhibit = 'Exhibit';
	protected $canDelete = false;
	protected $isAdmin = false;
	protected $isCreator = false;
	protected $isDepoOwner = false;
	protected $isDemoAdmin = false;
	protected $isNew = false;

	public function __construct()
	{
		parent::__construct("depositions/deposition.html");
		$dpid = isset($this->fv['depoID']) ? (int) $this->fv['depoID'] : 0;
		$cid = isset($this->fv['caseID']) ? (int) $this->fv['caseID'] : 0;
		$this->deposition = foo(new DB\ServiceDepositions)->getByID($dpid);
		$this->case = foo(new DB\ServiceCases)->getByID($cid);
		$this->client = foo(new DB\ServiceClients)->getByID( $this->case->clientID );
		$this->isNew = (!$this->case->ID || !$this->deposition->ID);
	}

	public function access() {
		if (!parent::access()) {
			return false;
		}
		$this->noAccessURL = $this->basePath . '/cases';

		if( $this->deposition->ID && $this->deposition->isWitnessPrep() ) {
			//redirect
			$this->noAccessURL = "{$this->basePath}/case/witnessprep/edit?caseID={$this->fv['caseID']}&depoID={$this->fv['depoID']}";
			return FALSE;
		}
		if( $this->deposition->ID && $this->deposition->class !== self::DEPOSITION_CLASS_DEPOSITION ) {
			return FALSE;
		}

		$this->isAdmin = ($this->case->ID && (new DB\OrchestraCases())->checkCaseAdmin($this->case->ID, $this->user->ID));
		$this->isCaseManager = ($this->case->ID && (new DB\OrchestraCaseManagers())->checkCaseManager( $this->case->ID, $this->user->ID ));
		$this->isDepoOwner = ($this->deposition->ID && $this->deposition->ownerID == $this->user->ID);
		$this->isDemoAdmin = ($this->case->isDemoCase() && $this->user->clientID == $this->case->clientID);

		if ( $this->isAdmin || $this->isCaseManager || $this->isDemoAdmin ) {
			$this->canDelete = TRUE;
		} else {
			$this->canDelete = FALSE;
		}

		return ($this->isDepoOwner || $this->canDelete);
	}

	public function init()
	{
		parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/title_toggle.js' ), 'link' );
		//$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/zoom_toggle.js' ), 'link' );
		//$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/jquery-ui-timepicker-addon.js' ), 'link' );
		//$this->css->add( new Helpers\Style( 'custom', null, Core\IO::url( 'backend-css' ) . '/jquery-ui-timepicker-addon.css' ), 'link' );


		$this->tpl->cname = $this->case->name;
		$this->tpl->canDelete = $this->canDelete;
		$this->tpl->isAdmin = $this->isAdmin;
		$this->tpl->caseID = $this->case->ID;
		$this->tpl->depoID = $this->deposition->ID;
		$this->tpl->depoStatusID = $this->deposition->statusID;
		$this->tpl->isEnterprise = ($this->client->typeID == self::CLIENT_TYPE_ENT_CLIENT);
		$this->tpl->isDemoCase = ($this->case->class == self::CASE_CLASS_DEMO);
		$this->tpl->isSchedulingReseller = ($this->reseller['class'] != self::RESELLERCLASS_RESELLER);

		$this->tpl->showOwnerPanel = $this->isNew || ($this->deposition->createdBy == $this->user->ID) || $this->isAdmin || foo(new DB\OrchestraCaseManagers())->checkCaseManager($this->case->ID, $this->user->ID);
		if ($this->tpl->showOwnerPanel)
		{
			if( !$this->isNew ) {
				$own = $this->deposition->owner[0];
				if( $this->deposition->statusID != self::DEPOSITION_STATUS_NEW ) {
					$this->get('ownerSearch')->disabled = true;
				}
				$this->tpl->hasOwner = TRUE;
				$this->get('ownerID')->value = $own->ID;
				$this->get('ownerName')->text = $own->firstName.' '.$own->lastName;
			} else {
				$this->tpl->hasOwner = FALSE;
			}
		}

		$reseller = (new DB\ServiceClients)->getByID( $this->client->resellerID );

//		if( $this->tpl->isEnterprise || !$this->tpl->isSchedulingReseller ) {
//			$schedulingResellers = [];
//			if( $this->isNew && $reseller->resellerClass == self::RESELLERCLASS_RESELLER ) {
//				$schedulingResellers[-1] = '';
//			}
//			$this->get( 'enterpriseResellerID' )->options = $schedulingResellers + $this->getSchedulingResellers();
//		}

		if( $this->isNew ) {
			$localTime = time() + ($this->session['timeZoneOffset'] * 60);
			$this->get('date')->value = date('m/d/Y', $localTime);
			if( $this->case->ID == 0 ) {
				header("Location: " . $this->basePath . "/cases");
				exit();
			}
			$this->head->name = $this->tpl->hname = 'Add: Deposition';
			$this->tpl->isNew = TRUE;

			$this->get( 'mainform' )->assign( ['courtReporterEmail'=>$this->user->clients[0]->courtReporterEmail] );

//			if( isset( $this->get( 'enterpriseResellerID' )->options[$reseller->ID] ) ) {
//				$this->get( 'enterpriseResellerID' )->value = $reseller->ID;
//			}

			$this->get('sPasscode')->value = self::createPasscode();

			$this->tpl->cancelURL = $this->basePath . '/case/view?ID=' . $this->case->ID;
		}
		else
		{
			$openDate = date_create_from_format('Y-m-d H:i:s', $this->deposition->openDateTime);
			$this->get('date')->value = $openDate->format('m/d/Y');

			$this->head->name = $this->tpl->hname = 'Edit: Deposition';
			$this->tpl->isNew = FALSE;

			$deposition = (new DB\ServiceDepositions)->getByID( $this->deposition->ID );
			if( !$deposition->courtReporterEmail || strlen( $deposition->courtReporterEmail ) < 1 ) {
				$deposition->courtReporterEmail = $this->user->clients[0]->courtReporterEmail;
			}
			$this->get( 'mainform' )->assign( $deposition->getValues() );

			$da = new \ClickBlocks\DB\DepositionsAuth( $deposition->ID );
			if( $da->ID ) {
				$this->get('sPasscode')->value = $da->getPasscode();
			}

			$this->tpl->depoID = $this->deposition->ID;
			$this->get('valpass')->groups = 'ignore';

//			if( $this->deposition->enterpriseResellerID ) {
//				$schedulingReseller = new \ClickBlocks\DB\Clients( $this->deposition->enterpriseResellerID );
//				$this->get( 'enterpriseResellerName' )->value = $schedulingReseller->name;
//				if( isset( $this->get( 'enterpriseResellerID' )->options[$schedulingReseller->ID] ) ) {
//					$this->get( 'enterpriseResellerID' )->value = $this->deposition->enterpriseResellerID;
//				}
//			}

			$dpAssistants = foo(new DB\OrchestraDepositionAssistants())->getByDepo($this->deposition->ID);
			if (count($dpAssistants) > 0)
			{
				$ids = array();
				foreach ($dpAssistants as $dpAssistant)
				{
					$ids[] = $dpAssistant['userID'];
				}
			}
			$this->get('users')->ids = $ids;

			if ( isset($this->fv['view']) ) {
				$this->tpl->cancelURL = $this->basePath . '/session/view?caseID=' . $this->case->ID . '&depoID=' . $this->deposition->ID;
			} else {
				$this->tpl->cancelURL = $this->basePath . '/case/view?ID=' . $this->case->ID;
			}
		}
	}

	
public function scheduleMeeting( array $params )
	{

	//$db = new DB();
	//$db->get_access_token();
	//$accessToken = $arr_token->access_token;

	//$db = DB\ORM::getInstance()->getDB('db0');

	    // Check is table empty
    function is_table_empty() {
		$db = DB\ORM::getInstance()->getDB('db0');
        $result = $db->rows("SELECT id FROM token");
		echo $result->num_rows;
		return;

        if($result->num_rows) {
            return false;
        }
  
        return true;
    }
  
    // Get access token
    function get_access_token() {
		$db = DB\ORM::getInstance()->getDB('db0');
		
		if(is_table_empty()) {
	
			$client = new \GuzzleHttp\Client(['base_uri' => 'https://zoom.us']);
												
			$options = [
					"headers" => [
						"Authorization" => "Basic ". base64_encode(CLIENT_ID.':'.CLIENT_SECRET)
					],
					'form_params' => [
						"grant_type" => "authorization_code",
						"code" => $_GET['code'],
						"redirect_uri" => REDIRECT_URI
					],
				];
		 
							 

				$response = $client->post("/oauth/token", $options);
				echo $response->getBody();
				return;

				$token = json_decode($response->getBody()->getContents(), true); 
						
				$db->rows("INSERT INTO token(access_token) VALUES('$token')");	
				echo "Access token inserted successfully.";			
				return $token;


		} else {
			$rows = $db->rows("SELECT access_token FROM token");	
			$access_token = json_decode($rows[0]['access_token'], true);
			return $access_token['access_token'];

		}

		
    }
  
    // Get referesh token
    function get_refersh_token() {
        $result = $this->get_access_token();
        return $result->refresh_token;
    }
  
    // Update access token
    function update_access_token($token) {
		$db = DB\ORM::getInstance()->getDB('db0');
        if($this->is_table_empty()) {
    
			$db->rows("INSERT INTO token(access_token) VALUES('$token')");	
	

        } else {
            $db->rows("UPDATE token SET access_token = '$token' WHERE id = (SELECT id FROM token)");

        }
    }

	//$rows = $db->rows("SELECT access_token FROM token");	
	//$access_token = json_decode($rows[0]['access_token'], true);
	//$accessToken = $access_token['access_token'];

	
    $accessToken = get_access_token();
	//echo $accessToken;
	//return;

	

	//$this->ajax->script('alert("'.$access_token['access_token'].'")');
	

	$client = new \GuzzleHttp\Client(['base_uri' => 'https://api.zoom.us']);


	$options = [
					"headers" => [
						"Authorization" => "Bearer $accessToken",
					    'Content-Type' => 'application/json',
						'Accept' => 'application/json',
					],
					'json' => [
						"topic" => $params['depositionOf'],
						"type" => 2,                              
						"start_time" => date('m/d/Y', $localTime),    // meeting start time
						"duration" =>  '60',    
						"timezone" => "America/New_York",				// 30 minutes
						"password" =>  $params['sPasscode'],       // meeting password
						"timezone"=> 'Asia/Tashkent',
						//"host_email" =>  $params['phost'], 
						//"settings" => [
						 // "host_video": true,
						 // "participant_video": true,
						 // "cn_meeting": false,
						 // "in_meeting": false,
						 // "join_before_host": false,
						 // "mute_upon_entry": true,
						 // "watermark": false,
						 // "use_pmi": false,
						  //"approval_type": 0,
						  //"registration_type": 1,
						 // "audio":"voip", 
						 // "enforce_login": false,
						 // "enforce_login_domains": "",
						  "alternative_hosts" => $params['shost'],
						 // "registrants_email_notification": false
						//]
						//"host_email" =>  $params['phost'] ,        // meeting password
						//'alternative_hosts' => $params['shost']         // meeting password

					]
				]; 
		

 
    										try {

											$response = $client->post("/v2/users/me/meetings", $options);

																							
											$meeting_details = json_decode($response->getBody(),true);	
											$join_url =  $meeting_details['join_url'];
											//echo $join_url;
											//return;
										
											
											
											$this->get('meetingurl')->value = $join_url;
										
											

										

										 	//$this->ajax->script('alert("'.$response.'")');


												$data = json_decode($response->getBody());
												/*echo "Thank You";
												echo "<br>";
												echo "You have been registered";
												echo "<br>";
												echo "Join URL: ". $data->join_url;
												echo "<br>";
												echo "Meeting Password: ". $data->password;*/

												$to1 = $params['recipients'];
												$to2 = $params['phost'];
												$subject = $params['depositionOf'];
											
												
												$message = "<html>
															<body>
															<p>Greetings, Edepoze's Zoom Meeting Scheduled!</p>
															<p>Please Join the Meeting at the below link.</p>
															<p>&nbsp;</p>
															<table>
															<tbody>
															<tr>
															<td><strong>Meeting Title: </strong>".$params['depositionOf']."</td>
															</tr>
															<tr>
															<td><strong>Zoom Meeting URL:</strong> ". $data->join_url ."</td>
															</tr>
															<tr>
															<td><strong>Meeting Passcode: </strong>" . $data->password ."</td>
															</tr>
															<tr>
															</tr>
															</tbody>
															</table>
															<p>&nbsp;</p>
														
															<p>Thank you, <br />eDdepoze</p>
															</body>
															</html>";
															// Always set content-type when sending HTML email
															//$headers = "MIME-Version: 1.0" . "\r\n";
															//$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

															// More headers
															//$headers .= 'From: <zoom@edepoze.com>' . "\r\n";
															//$headers .= 'Cc: ashok.metla@genysoft.com' . "\r\n";

																

												//sendmail($to,$subject,$message,$headers);
												//echo $start_url;
												//echo $subject;
												//echo $to;
												//return;


											
												$mailer = Utils\EmailGenerator::sendZoomMeetingInvite( [
														'sendToName' => 'Edepoze Deposition Recipients',
														'sendToAddress1' => $to1,
														'sendToAddress2' => $to2,
														'subject' => $subject,
														'content' => $message,
												] );
												if ($mailer->IsError()) {
													  $request->delete();
													  $this->except ('Error sending email', 1002);
												   }
												return array('email'=>$user->email);
																					 
											} catch(Exception $e) {

											
												echo $e->getMessage(), "\n";
												return;

												if( 401 == $e->getCode() ) {


													$refresh_token = $this->get_refersh_token();
										 
													$client = new \GuzzleHttp\Client(['base_uri' => 'https://zoom.us']);

												
													$options = [
														"headers" => [
															"Authorization" => "Basic ". base64_encode(CLIENT_ID.':'.CLIENT_SECRET)
														],
														'form_params' => [
															"grant_type" => "refresh_token",
															"refresh_token" => $refresh_token
														],
													];
													
													$response = $client->post("/oauth/token", $options);
													echo $accessToken;
										
													$this->update_access_token($response->getBody());
										 
													scheduleMeeting();
												} else {
													echo $e->getMessage();
												}
											}

						
		
	return;
        
	}


	public function save( array $params )
	{
		$this->ajax->script( 'btnLock = false' );
		if( !$this->validators->isValid( 'depo' ) ) {
			return;
		}
		if( !isset( $params['courtReporterEmail'] ) || $params['courtReporterEmail'] == '' ) {
			$params['courtReporterEmail'] = NULL;
		}
		$params['openDateTime'] = date( 'Y-m-d H:i:s', strtotime( "{$params['date']} 08:00:00" ) );
		$params['caseID'] = $this->case->ID;
		if( !$params['ownerID'] ) {
			$params['ownerID'] = $this->user->ID;
		}

		$now = date( 'Y-m-d H:i:s' );
		if( !$this->deposition->ID ) {
			$params['created'] = $now;
			$params['uKey'] = time() . mt_rand( 1000000, 9999999 );
			$params['createdBy'] = $this->user->ID;
			$params['class'] = self::DEPOSITION_CLASS_DEPOSITION;
		}

//		$oldCREmail = $this->deposition->courtReporterEmail;
		$this->deposition->setValues( $params );
		$this->deposition->save();

		if( isset( $params['sPasscode'] ) && $params['sPasscode'] ) {
			$da = new \ClickBlocks\DB\DepositionsAuth( $this->deposition->ID );
			if( $da->ID ) {
				$da->spice = DB\OrchestraDepositionsAuth::generateSpice();
				$da->code = $params['sPasscode'];
				$da->update();
			} else {
				$da->ID = $this->deposition->ID;
				$da->spice = DB\OrchestraDepositionsAuth::generateSpice();
				$da->code = $params['sPasscode'];
				$da->insert();
			}
		}
		// check if uKey exists
		if( $this->deposition->uKey != $this->deposition->ID ) {
			// Update uKey by deposition ID
			$this->deposition->uKey = $this->deposition->ID;
//			(new DB\ServiceDepositions())->save($this->deposition);
			$this->deposition->update();
		}

		$userIDs = (array)$this->get( 'users' )->ids;
		$userIDs = array_combine( $userIDs, $userIDs );
		$removedAssistants = [];
		$svCM = new \ClickBlocks\DB\ServiceCaseManagers();
		foreach( $this->deposition->depositionAssistants as $rec ) {
			if( isset( $userIDs[$rec->userID] ) ) {
				unset( $userIDs[$rec->userID] );
			} else {
				if( $rec->userID == $this->deposition->ownerID && !$svCM->getByID( ['caseID'=>$this->deposition->caseID, 'userID'=>$rec->userID] )->userID ) {
					$this->deposition->resetOwner();
				}
				$removedAssistants[] = $rec->userID;
				$rec->delete();
			}
		}
		//foo(new DB\OrchestraDepositionAssistants())->deleteByDeposition($this->deposition->ID);
		foreach( $userIDs as $id ) {
			if( $id == 0 ) {
				continue;
			}
			$dpAssistant = new \ClickBlocks\DB\DepositionAssistants();
			$dpAssistant->depositionID = $this->deposition->ID;
			$dpAssistant->userID = $id;
			$dpAssistant->insert();
			$rKey = array_search( $id, $removedAssistants );
			if( $rKey !== FALSE ) {
				unset( $removedAssistants[$rKey] );
			}
		}

//		$depo = $this->deposition;
//		if( $depo->statusID == self::DEPOSITION_STATUS_FINISHED && $depo->courtReporterEmail && $depo->courtReporterEmail != $oldCREmail ) {
//			// send notification if email was set (or changed)
//			Utils\EmailGenerator::sendCourtReporterInvite( $depo->ID );
//		}

		$node = new \ClickBlocks\Utils\NodeJS( TRUE );
		$node->notifyCasesUpdated( $this->deposition->caseID, $this->deposition->ID );

		if( $removedAssistants ) {
			$node->notifyUserCasesUpdated( $removedAssistants );
		}

		if ( $this->isNew ) {
			$this->ajax->redirect( $this->basePath . '/session/view?caseID=' . $this->deposition->caseID . '&depoID=' . $this->deposition->ID );
		} else {
			$this->ajax->redirect($this->tpl->cancelURL);
		}
	}

	public function searchUsers($name, $uniqueID)
	{
		$panel = $this->get('userAutofillTemplate');
		$ids = $this->get('users')->ids;
		$isOwner = (stripos($uniqueID, 'owner') === 0);
		if ($isOwner) {
		  // $rows = foo(new DB\OrchestraUsers())->searchTrustedUsers($this->case->ID, $this->deposition->ID, $name);
			$excludeIDs = [];
			if( $this->get('ownerID')->value ) {
				$excludeIDs[] = (int)$this->get('ownerID')->value;
			}
		  $rows = foo(new DB\OrchestraUsers())->getUsersForDeposition($name, false, $this->user->clientID, $excludeIDs);
		   $panel->tpl->callback = '->setOwner';
		} else {
		  $ids[] = $this->user->ID;
		  $rows = foo(new DB\OrchestraUsers())->getUsersForDeposition($name, false, $this->user->clientID, $ids);
		  $panel->tpl->callback = '->addUser';
		}
		if (!$rows) return false;
		$panel->tpl->rows = $rows;
		$panel->tpl->uniqueID = $uniqueID;
		return trim($panel->getInnerHTML());
	}

	public function addUser($userID) {
		$ids = (array)$this->get('users')->ids;
		$user = foo(new DB\ServiceUsers())->getByID($userID);
		if ($user->ID > 0)
		{
		  if (in_array($user->ID,$ids)) return;
		  array_push($ids, $user->ID);
		  $this->get('users')->ids = $ids;
		  $this->get('users')->update();
		}
		$this->get('userSearch')->value = '';
		$this->ajax->script( 'adjustBorder();' );
	}

	public function setOwner($userID)
	{
		$this->get('ownerID')->value = $userID;
		$own = foo(new DB\ServiceUsers)->getByID($userID);
		$this->get('ownerName')->text = $own->firstName.' '.$own->lastName;
		$this->get('ownerSearch')->value = '';
		$this->tpl->hasOwner = TRUE;
		$this->get( 'depoOwner' )->update();
		$this->ajax->script( 'setInputsClassOfValidators();validators.validate("depo", "error", "");' );
	}

	public function showAssistants()
	{
		$popup = $this->get('noticePopup');
		$this->cleanByUniqueID($popup->uniqueID);
		$popup->tpl->title = 'As members of the team, assistants can:';
		$popup->tpl->message = '* Receive and view all documents and exhibits in sessions <br> * Save annotated documents to the repository <br> * Upload/Download files in a session';
		$popup->show();
	}

	public function confirmDeleteDepo($id) {
		$depo = $this->getService('Depositions')->getByID($id);
		if (0 == $depo->ID)
		  return;
		$param = array();
		$param['title'] = 'Delete Session';
		$param['message'] = 'Are you sure you want to delete this session and all its contents?';
		$param['OKName'] = 'Delete';
		$param['OKMethod'] = "deleteDepo({$depo->ID}, 1);";
		$this->showPopup('confirm', $param);
	}

	public function deleteDepo( $depositionID )
	{
		$depositionID = (int)$depositionID;
		$this->hidePopup( 'confirm' );
		$depo = $this->getService( 'Depositions' )->getByID( $depositionID );
		if( !$depo->ID || $depo->ID != $depositionID ) {
			return;
		}
		$depo->setAsDeleted();
		$nodeJS = new \ClickBlocks\Utils\NodeJS( TRUE );
		$nodeJS->notifyCasesUpdated( $this->case->ID );
		$this->ajax->redirect( $this->basePath . '/case/view?ID=' . $this->case->ID );
	}

//	public function setResellerProductionEmail()
//	{
//		$reseller = (new DB\ServiceClients)->getByID( $this->get('enterpriseResellerID')->value );
//		$this->get('courtReporterEmail')->value = $reseller->courtReporterEmail;
//	}

	protected function getSchedulingResellers()
	{
		$resellers = [];
		$rows = $this->getOrchestraClients()->getSchedulingResellers( $this->user->clients[0]->resellerID );
		if( is_array( $rows ) ) {
			foreach( $rows as $reseller ) {
				$resellers[$reseller['ID']] = $reseller['name'];
			}
		}
		return $resellers;
	}

}

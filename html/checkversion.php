<?php

require_once( __DIR__ . '/Application/connect.php' );

\ClickBlocks\Debug::ErrorLog( '> ' . date( 'Y-m-d H:i:s' ) . ' > Request from ' . $_SERVER['REMOTE_ADDR'] . ': /checkversion; ' . $_SERVER['REQUEST_METHOD'] . ': ' . print_r( json_encode( $_REQUEST ), TRUE ) );

$iPadBuild = $_REQUEST['iPadBuild'];

$versions = new \ClickBlocks\Core\Config();
$versions->init( $versions->root . '/Application/_config/versions.ini' );
$versions->init( $versions->root . '/Application/_config/.local.ini' );

if( !isset( $versions['device']['appStoreVersion'] ) ) {
	throw new \LogicException( 'Unable to determine AppStore version.' );
}
if( !isset( $versions['device']['releaseVersion'] ) ) {
	throw new \LogicException( 'Unable to determine Release version.' );
}

function compareVersions( $v1, $v2 ) {
	$v1Cmp = explode( '.', $v1 );
	$v2Cmp = explode( '.', $v2 );
	array_splice( $v1Cmp, 3 );
	array_splice( $v2Cmp, 3 );
	$v1Cmp[0] = (isset( $v1Cmp[0] )) ? (int)$v1Cmp[0] : 0;
	$v2Cmp[0] = (isset( $v2Cmp[0] )) ? (int)$v2Cmp[0] : 0;
	$v1Cmp[1] = (isset( $v1Cmp[1] )) ? (int)$v1Cmp[1] : 0;
	$v2Cmp[1] = (isset( $v2Cmp[1] )) ? (int)$v2Cmp[1] : 0;
	$v1Cmp[2] = (isset( $v1Cmp[2] )) ? (int)$v1Cmp[2] : 0;
	$v2Cmp[2] = (isset( $v2Cmp[2] )) ? (int)$v2Cmp[2] : 0;
	$v1n = implode( '.', $v1Cmp );
	$v2n = implode( '.', $v2Cmp );
//	\ClickBlocks\Debug::ErrorLog( "compareVersions: {$v1n}, {$v2n}" );
	return strcmp( $v1n, $v2n );
}

$response = [
	'appStoreVersion' => $versions['device']['appStoreVersion'],
	'remoteConfiguration' => $versions['remoteConfiguration']
];

$bundle = isset( $_REQUEST['bundleName'] ) ? $_REQUEST['bundleName'] : 'edepoze';

$versionCmp = compareVersions( $versions['device']['releaseVersion'], $versions['device']['appStoreVersion'] );
$deviceCmp = compareVersions( $iPadBuild, $versions['device']['appStoreVersion'] );
if( compareVersions( $versions['device']['appStoreVersion'], $versions['device']['prereleaseVersion'] ) === -1 && compareVersions( $iPadBuild, $versions['device']['prereleaseVersion'] ) === 0 ) {
	$versionCmp = -1;	//prerelease
}

switch( $versionCmp ) {
	case -1:	//release before appstore -- App version isn't released yet
//		$response['versionCheck'] = 'prerelease';
//		$response['dialogMessage'] = 'This application is pending release.';
//		if( stripos( $bundle, "edepoze" ) >= 0 || !isset( $versions['iTunesAppStore'][$bundle] ) ) {
//			$response['appStoreURL'] = $versions['iTunesAppStore']["edepoze164"];
//		} else {
//			$response['appStoreURL'] = $versions['iTunesAppStore'][$bundle];
//		}
		\ClickBlocks\Debug::ErrorLog( 'Prerelease version, using custom remote configuration' );
		$response['versionCheck'] = 'ok';
		$response['remoteConfiguration'] = [
			'ServerURL' => 'https://staging.edepoze.com/api/2.1.0/',
			'ServerAddress' => 'cs-staging.edepoze.com',
			'Port' => 443,
			'VersionURL' => 'https://staging.edepoze.com/checkversion.php'
		];
		break;
	case 1:		//release after appstore
		\ClickBlocks\Debug::ErrorLog( 'Configuration error! Release version should never be greater than AppStore version' );
		break;
	default:	//release equals appstore -- Okay To Go
		$response['versionCheck'] = ($deviceCmp >= 0) ? 'ok' : 'update';
		if( $response['versionCheck'] == 'update' ) {
			$response['appStoreURL'] = (isset( $versions['iTunesAppStore'][$bundle] )) ? $versions['iTunesAppStore'][$bundle] : $versions['iTunesAppStore']['edepoze'];
		}
		break;
}
$reg = \ClickBlocks\Core\Register::getInstance();
$response['remoteConfiguration']['MaxUploadSize'] = $reg->config->maxFilesize;
$response['remoteConfiguration']['TrialBinder_MaxUploadSize'] = $reg->config->trialbinder['maxFilesize'];


$result = json_encode( $response );
\ClickBlocks\Debug::ErrorLog( 'Response: ' . print_r( $result, TRUE ) );
header( 'Content-Type: application/json' );
echo $result;

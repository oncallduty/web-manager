<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Exceptions;

class SQLValue 
{
   protected $expression;
   
   public function __construct($expression) {
      if (!is_scalar($expression)) throw new Core\Exception('SQL expression should be a string value.');
      $this->expression = $expression;
   }
   
   public function getSQL()
   {
      return $this->expression;
   }

   public function __toString() {
      return $this->expression;
   }
}

class SQLNullValue extends SQLValue
{
   public function __construct() {
      parent::__construct('NULL');
   }
}

class SQLCurrentTimestampValue extends SQLValue
{
   public function __construct() {
      parent::__construct('CURRENT_TIMESTAMP');
   }
}

class SQLNOWValue extends SQLValue
{
   public function __construct() {
      parent::__construct('NOW()');
   }
}


?>

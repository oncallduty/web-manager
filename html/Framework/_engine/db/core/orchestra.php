<?php
/**
 * ClickBlocks.PHP v. 1.0
 *
 * Copyright (C) 2010  SARITASA LLC
 * http://www.saritasa.com
 *
 * This framework is free software. You can redistribute it and/or modify
 * it under the terms of either the current ClickBlocks.PHP License
 * viewable at theclickblocks.com) or the License that was distributed with
 * this file.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the ClickBlocks.PHP License
 * along with this program.
 *
 * Responsibility of this file: archestra.php
 *
 * @category   DB
 * @package    DB
 * @copyright  2007-2010 SARITASA LLC <info@saritasa.com>
 * @link       http://www.saritasa.com
 * @since      File available since Release 1.0.0
 */
namespace ClickBlocks\DB;

use ClickBlocks\Core;

/**
 * The class is an abstraction used for the concentration of plain sql query in one place.
 * Класс является абстракцией, используется для концентрации всех plain sql запросов в одном месте.
 */
class Orchestra
{
   protected $objInfo = null;
   protected $orm = null;
   /**
    * DataBase object
    * @var \ClickBlocks\DB\DB
    */
   protected $db = null;
   protected $config = null;
   protected $className = null;

   /**
    * The class constructor.
    * Конструктор класса.
    * 
    * @param string $className 
    */
   public function __construct($className = null)
   {
      if ($className === NULL) $className = static::getBLLClassName();
      $this->config = Core\Register::getInstance()->config;
      $this->orm = ORM::getInstance();
      $this->objInfo = $this->orm->getORMInfoObject();
      $this->className = $className;
      $this->db = foo(new SQLGenerator())->getDBByClassName($this->className);
   }

   /**
    * Get appropriate DB object
    * Получает нужный DB объект
    * 
    * @return \ClickBlocks\DB\DB
    */
   protected static function getDB()
   {
      return foo(new SQLGenerator())->getDBByClassName(static::getBLLClassName());
   }
   
   public static function getBLLClassName()
   {
      return '';
   }
   
   protected function getTableName()
   {
      $info = $this->objInfo;
      $tb = $info->getTableAliasByClassName(\ClickBlocks\Utils\PHPParser::getClassName($this->className));
      return $info->getTableNameByTableAlias($tb[0],$tb[1]);
   }

   /**
    * Returns array of BLL objects by specified criteria.
    * Возвращает массив объектов BLL по заданным критериям.
    * 
    * @param string|array $where - Where clause (w/o the WHERE keyword), OR associative array with fields and values to search for.
    * @param string ORDER BY clause without the keyword
    * @param int $start Start offset
    * @param int $limit Record limit
    * @param bool $multiple
    * @return array Array of BLL Objects
    */
   protected function getObjectsByQuery($where, $order = null, $start = null, $limit = null)
   {
      $className = $this->className;
      $rows = $this->db->rows(foo(new SQLGenerator())->getByQuery($className, $where, $order, $start, $limit), is_array($where) ? $where : array());
      return $this->rowsToObjects($rows, true);
   }
   
   /**
    * Returns BLL object by specified criteria. If more than one object met the criteria than first one (order by primary key) 
    * will be returned. If no object found - empty BLL will returned.
    * Возвращает BLL объект по заданным критериям. Если найдено несколько объектов, вернет первый (по первичному ключу).
    * Если объекта в базе нет - вернет пустой BLL.
    * 
    * @param string|array $where - Where clause (w/o the WHERE keyword), OR associative array with fields and values to search for.
    * @return \ClickBlocks\DB\BLLTable
    */
   protected function getObjectByQuery($where)
   {
      $rows = $this->getObjectsByQuery($where, null, null, 1);
      if (count($rows) == 0) {
         $className = $this->className;
         return new $className();
      } else {
         return $rows[0];
      }
   }
   
   protected function rowsToObjects(array $rows)
   {
      $className = $this->className;
      $objects = array();
      foreach ($rows as $row) {
         $tb = new $className();
         $tb->assign($row);
         $objects[] = $tb;
      }
      return $objects;
   }
}

?>

<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Exceptions;

interface IBLLTable extends IDALTable
{

}

/**
 *
 * Класс, реализующий бизнес-логику и представляющий собою агрегатор для DALTable
 * @method bool isKeyFilled() true if primary key is currently filled
 * @method bool isInstantiated() true if record is instantiated in DB table
 * @method string|array getKey($isRawData = false) Key field name/names
 * @method string|array getKeyValue($isRawData = false)
 */
class BLLTable implements IBLLTable, \Serializable
{
   const BUILD_BY_DB = 0;
   const BUILD_BY_CACHE = 1;
   const BUILD_BY_NEW = 2;

   private $buildBy = self::BUILD_BY_NEW;

   protected $dal = array();
   protected $config = null;
   protected $fields = array();
   protected $navigationFields = array();
   protected $navigators = array();
   public $expire = null;
   public $navigator = null;

   /**
    *
    * Конструктор класса
    *
    * @access public
    */
   public function __construct()
   {
      $this->init();
      $this->setExpire($this->config->cacheBLLObjectExpire);
   }

   /**
    *
    * Функция инициализации
    *
    * @access protected
    */
   protected function init()
   {
      $this->config = Core\Register::getInstance()->config;
      $info = ORM::getInstance()->getORMInfo();
      $class = $this->getShortClassName();
      $this->fields = array_merge($info['classes'][$class]['table']['fields'], $info['classes'][$class]['table']['logicFields']);
      $this->navigationFields = $info['classes'][$class]['table']['navigationFields'];
   }

   protected function getShortClassName()
   {
      $class = get_class($this);
      $k = strrpos($class, '\\');
      if ($k !== false) $class = substr($class, $k + 1);
      return $class;
   }

   /**
    * Устанавливает время жизни
    *
    * @param integer $value
    * @access public
    */
   public function setExpire($value)
   {
      $this->expire = $value;
   }

   /**
    * Получить значение
    *
    * @return integer
    * @access public
    */
   public function getExpire()
   {
      return $this->expire;
   }

   /**
    *
    * @param IDALTable $tb
    * @param string $className
    * @return \ClickBlocks\DB\BLLTable
    * @access public
    */
   public function addDAL(IDALTable $tb, $className)
   {
      $this->dal[$className] = $tb;
      return $this;
   }

   /**
    *
    * @access public
    */
   public function getDAL($className = null)
   {
      return $this->dal[($className) ? $className : get_class($this)];
   }

   /**
    *
    * @return array
    * @access public
    */
   public function getDALs()
   {
      return $this->dal;
   }

   /**
    *
    * @param type $field
    * @return type
    * @access public
    */
   public function getField($field)
   {
      foreach ($this->dal as $dal) if (is_array($dal->getField($field))) return $dal->getField($field);
   }

   /**
    * Проверка, получены ли данные из базы
    *
    * @return bool
    * @access public
    */
   public function isFromDB()
   {
      return ($this->buildBy == self::BUILD_BY_DB);
   }

   /**
    * Проверка, получены ли данные из кэша
    *
    * @return bool
    * @access public
    */
   public function isFromCache()
   {
      return ($this->buildBy == self::BUILD_BY_CACHE);
   }

   /**
    * Проверка, являются ли даные новыми
    *
    * @return bool
    * @access public
    */
   public function isFromNew()
   {
      return ($this->buildBy == self::BUILD_BY_NEW);
   }

   /**
    *
    *
    * @param type $fields
    * @param bool $isRawData
    * @return \ClickBlocks\DB\BLLTable
    * @access public
    */
   public function setValues($fields, $isRawData = false)
   {
      foreach ($this->dal as $dal) $dal->setValues($fields, $isRawData);
      $this->checkNavigator();
      return $this;
   }

	/**
	 *
	 * @param bool $isRawData
	 * @param bigint $userID
	 * @return array
	 */
	public function getValues( $isRawData=FALSE, $userID=NULL )
	{
		$values = $aliases = [];
		foreach( $this->dal as $dal ) {
		   $values = array_merge( $values, $dal->getValues( $isRawData ) );
		   $aliases = array_merge( $aliases, $dal->getAliases() );
		}
		if( !$isRawData ) {
			return array_intersect_key( $values, $this->fields );
		}
		return array_intersect_key( $values, $aliases );
	}

   /**
    *
    * @param array $data
    * @return \ClickBlocks\DB\BLLTable
    * @access public
    */
   public function assign(array $data = null)
   {
      $n = 0;
      foreach ($this->dal as $dal) $dal->assign($data);
      $this->initNavigators();
      $this->buildBy = self::BUILD_BY_DB;
      return $this;
   }

   /**
    *
    * @param string $field
    * @param string $value
    * @throws \Exception
    * @access public
    */
   public function __set($field, $value)
   {
      if (isset($this->fields[$field]))
      {
         $dal = $this->dal[$this->fields[$field]];
         $fieldInfo = $dal->getField($field);
         $dal->{$field} = $value;
         if (count($fieldInfo['navigators']) > 0 && $fieldInfo['value'] != $value)
         {
            foreach ($fieldInfo['navigators'] as $navigator=>$v)
            {
               if (isset($this->navigators[$navigator])) $this->navigators[$navigator]->reinitialize();
            }
         }
         $this->checkNavigator();
         return;
      }
      else if (isset($this->navigationFields[$field]))
      {
         $info = $this->dal[$this->navigationFields[$field]]->getNavigationField($field);
         if ($info['output'] == 'object' && !is_a($value, $info['to']['bll'])) throw new \Exception(err_msg('ERR_BLL_3', array($field)));
         if ($info['output'] == 'raw' && !is_array($value)) throw new \Exception(err_msg('ERR_BLL_4', array($field)));
         if (!isset($this->navigators[$field])) $this->initNavigator($field);
         if ($info['multiplicity']) $this->navigators[$field][] = $value;
         else $this->navigators[$field][0] = $value;
         return;
      }
      throw new \Exception(err_msg('ERR_BLL_1', array($field, get_class($this))));
   }

   /**
    *
    * @param string $field
    * @return type
    * @throws \Exception
    * @access public
    */
   public function __get($field)
   {
      if (isset($this->fields[$field])) return $this->dal[$this->fields[$field]]->{$field};
      else if (isset($this->navigationFields[$field]))
      {
         if (!isset($this->navigators[$field])) $this->initNavigator($field);
         return $this->navigators[$field];
      }
      throw new \Exception(err_msg('ERR_BLL_1', array($field, get_class($this))));
   }

   /**
    *
    * @param string $field
    * @return bool
    * @access public
    */
   public function __isset($field)
   {
      return (isset($this->fields[$field]) || isset($this->navigationFields[$field]));
   }

   /**
    *
    * @param string $method
    * @param array $params
    * @return mixed
    * @throws \Exception
    * @access public
    */
   public function __call($method, $params)
   {
      if (isset($this->navigationFields[$method])) return call_user_func_array(array($this->navigators[$method], 'limit'), $params);
      foreach ($this->dal as $dal)
      {
         try
         {
            return call_user_func_array(array($dal, $method), $params);
         }
         catch(\Exception $e){}
      }
      throw new \Exception(err_msg('ERR_BLL_2', array($method, get_class($this))));
   }

   /**
    *
    * @return string
    * @access public
    */
   public function serialize()
   {
      $data = get_object_vars($this);
      unset($data['fields']);
      unset($data['navigationFields']);
      unset($data['config']);
      unset($data['navigator']);
      return serialize($data);
   }

   /**
    *
    * @param type $data
    * @access public
    */
   public function unserialize($data)
   {
      $data = unserialize($data);
      foreach ($data as $k => $v) $this->{$k} = $v;
      $this->init();
      foreach ($this->navigationFields as $field => $class)
      {
         if (!isset($this->navigators[$field]))
         {
            if ($this->isFromCache()) $this->initNavigator($field);
         }
         else $this->navigators[$field]->initialize($this);
      }
   }

   /**
    *
    * @return type
    * @access public
    */
   public function isCalledFromService()
   {
      return true;
      $trace = debug_backtrace();
      return ($trace[2]['object'] instanceof Service && $trace[2]['object']->getObjectName() == '\\' . get_class($this));
   }

   public function getRow($pk = null, $isRawData = false)
   {
      if (!$pk) return array();
      $dal = $this->getDAL();
      if (!is_array($pk) && $dal->getKeyLength() > 1) throw new \Exception(err_msg('ERR_SVC_1', array(get_called_class())));
      if (!is_array($pk)) $pk = array($dal->getKey($isRawData) => $pk);
      $sql = foo(new SQLGenerator())->getRow(get_called_class(), $pk, $isRawData);
      return $dal->getDB()->row($sql, $pk);
   }

   public function assignByID($pk)
   {
      $this->assign($this->getRow($pk, false));
   }

   /**
    *
    * @return type
    * @throws \Exception
    * @access public
    */
   public function save()
   {
      if (!$this->isCalledFromService()) throw new \Exception(err_msg('ERR_BLL_5', array('save', get_class($this))));
      foreach ($this->dal as $dal)
      {
         if ($ID) $dal->setKeyValue($ID);
         $res += $dal->save();
         $ID = $dal->getKeyValue(false);
      }
      $this->saveNavigators();
      return $res;
   }

   /**
    *
    * @return type
    * @throws \Exception
    * @access public
    */
   public function insert()
   {
      if (!$this->isCalledFromService()) throw new \Exception(err_msg('ERR_BLL_5', array('insert', get_class($this))));
      foreach ($this->dal as $dal)
      {
         if ($ID) $dal->setKeyValue($ID);
         $res += $dal->insert();
         $ID = $dal->getKeyValue(false);
      }
      return $res;
   }

   /**
    *
    * @return type
    * @throws \Exception
    * @access public
    */
   public function delete()
   {
      if (!$this->isCalledFromService()) throw new \Exception(err_msg('ERR_BLL_5', array('delete', get_class($this))));
      foreach (array_reverse($this->dal) as $dal) $res += $dal->delete();
      return $res;
   }

   /**
    *
    * @return type
    * @throws \Exception
    * @access public
    */
   public function update()
   {
      if (!$this->isCalledFromService()) throw new \Exception(err_msg('ERR_BLL_5', array('update', get_class($this))));
      foreach ($this->dal as $dal) $res += $dal->update();
      return $res;
   }

   /**
    *
    * @return type
    * @throws \Exception
    * @access public
    */
   public function replace()
   {
      if (!$this->isCalledFromService()) throw new \Exception(err_msg('ERR_BLL_5', array('replace', get_class($this))));
      foreach ($this->dal as $dal) $res += $dal->replace();
      return $res;
   }

   public function getLastInsertId()
   {
	   foreach ($this->dal as $dal)
	   {
		   return $dal->getLastInsertId();
	   }
   }

   /**
    *
    * @access protected
    */
   protected function saveNavigators()
   {
      foreach ($this->navigators as $key => $navigator) $navigator->save();
   }

   /**
    * @access protected
    */
   protected function initNavigators()
   {
      foreach ($this->navigationFields as $field => $class) $this->initNavigator($field);
   }

   /**
    *
    * @param type $field
    * @access protected
    */
   protected function initNavigator($field)
   {
      if (isset($this->navigators[$field])) {
         $this->navigators[$field]->reinitialize();
      } else
      {
         $data = $this->dal[$this->navigationFields[$field]]->getNavigationField($field);
         $this->{$data['init']['name']}();
      }
   }

   /**
    *
    * @access protected
    */
   protected function checkNavigator()
   {
      if (!is_array($this->navigator)) return;
      $this->navigator['navObject']->offsetSet($this->navigator['offset'], $this, true);
      $this->navigator = null;
   }

	protected function addNavigator( $navigator, $bllClass ) {
		if( $bllClass == __CLASS__ ) {
			return;
		}
		$this->navigators[$navigator] = new \ClickBlocks\DB\NavigationProperty( $this, $bllClass, $navigator );
	}

	public static function generateSpice() {
		return bin2hex( \mcrypt_create_iv( \mcrypt_get_iv_size( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC ), MCRYPT_RAND ) );
	}
}

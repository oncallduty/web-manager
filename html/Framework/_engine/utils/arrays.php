<?php

namespace ClickBlocks\Utils;

/**
 * The class provides several functions for working with arrays.
 * <br>Класс предоставляет несколько функций для работы с массивом.
 */
class Arrays
{
   public static function swap(array &$array, $key1, $key2)
   {
      $keys = array_flip(array_keys($array));
      $array = array_merge(array_slice($array, 0, $keys[$key1]), array_reverse(array_slice($array, $keys[$key1], $keys[$key2])) ,array_slice($array, $keys[$key2]));
   }

   /**
    * Add item ['key']=value in the $array after $keySource
    * <br>Добавить элемент ['key']=value в массив $array после элемента $keySource
    *
    * @param array $array
    * @param mixed $keySource
    * @param mixed $key
    * @param mixed $value
    * @return array 
    * @access public
    * @static 
    */
   public static function inject(array &$array, $keySource, $key, $value)
   {
      $keys = array_flip(array_keys($array));
      $array = array_slice($array, 0, $keys[$keySource] + 1, true) + array($key => $value) + array_slice($array, $keys[$keySource] + 1, null, true);
      return $array;
   }
}

?>
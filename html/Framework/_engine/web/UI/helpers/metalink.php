<?php

namespace ClickBlocks\Web\UI\Helpers;

use ClickBlocks\Core,
    ClickBlocks\Web;

/**
 * The class extends Control and is designed to generate the html code tag <link />
 */
class MetaLink extends Control
{
   /**
    * The class constructor
    *
    * @param string $id
    * @param string $href
    * @param string $text
    */
   public function __construct($rel=null, $type=null, $href=null)
   {
      parent::__construct();
      $this->attributes['rel'] = $rel;
	  $this->attributes['type'] = $type;
      $this->attributes['href'] = $href;
	  $this->attributes['media'] = null;
      $this->attributes['hreflang'] = null;
	  $this->attributes['crossorigin'] = null;
   }

   /**
    * Generates the html code to link taking into account all the attributes set
    *
    * @return string
    */
   public function render()
   {
      return '<link' . $this->getParams() . '/>';
   }
}

?>

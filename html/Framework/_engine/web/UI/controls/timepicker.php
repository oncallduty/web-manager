<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class TimePicker extends TextBox
{
   public function __construct($id, $value = null)
   {
      parent::__construct($id, $value);
      $this->properties['classContainer'] = null;
      $this->properties['styleContainer'] = null;
      $this->properties['options'] = array();
      $this->properties['tagContainer'] = 'span';
      $this->attributes['maxlength'] = '8';
   }

   public function &__get($param)
   {
      if ($param == 'options') return $this->properties['options'];
      return parent::__get($param);
   }

   public function CSS()
   {
	  $this->css->add(new Helpers\Style('timepicker', null, Core\IO::url('framework') . '/web/js/timepicker/jquery.timepicker.css'), 'link');
      return $this;
   }

   public function JS()
   {
      $this->js->addTool('timepicker');
      if (!$this->properties['visible']) return $this;
      if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
      else $this->js->add(new Helpers\Script('timepicker_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
      return $this;
   }

   public function render()
   {
      if (!$this->properties['visible']) return $this->invisible();
      $html = '<' . $this->properties['tagContainer'] . ' id="container_' . $this->attributes['uniqueID'] . '" style="' . htmlspecialchars($this->properties['styleContainer']) . '" class="' . htmlspecialchars($this->properties['classContainer']) . '">';
      $html .= '<input type="text"';
      if ($this->properties['disabled']) $html .= ' disabled="disabled"';
      if ($this->properties['readonly']) $html .= ' readonly="readonly"';
      $html .= $this->getParams() . ' />';
      $html .= '</' . $this->properties['tagContainer'] . '>';
      return $html;
   }

   protected function repaint()
   {
	  parent::repaint();
      $this->ajax->script($this->getConstructor());
      //$this->CSS();
      //$this->JS();
   }

   protected function getRepaintID()
   {
      return 'container_' . $this->attributes['uniqueID'];
   }

   protected function remove($time = 0)
   {
     $this->ajax->script($this->getDestructor(), $time, true);
     return parent::remove($time);
   }

   protected function getConstructor()
   {
      return "$('#{$this->attributes['uniqueID']}').timepicker({'scrollDefaultNow':true});";
   }

   protected function getDestructor()
   {
	  return "$('#{$this->attributes['uniqueID']}').timepicker('remove');";
   }
}

?>
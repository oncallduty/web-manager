<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

/**
 * The class is designed to generate a CAPTCHA.
 * It requires that the directory of the folder was "_includes\verification" that will contain the font and background images. 
 * Default:
 * The font arial.ttf
 * 33 images, bgcolor1.jpg, bgcolor2.jpg, bgcolor3.jpg ...
 */
class Verification extends Image
{
   /**
    * Class constructor
    * Sets the initial attributes and properties.
    * The length of the generated code - 7
    * Character set used in the verification code - alphabet and numbers
    *
    * @param string $id
    * @access public 
    */
   public function __construct($id)
   {
      parent::__construct($id);
      $this->properties['length'] = 7;
      $this->properties['chars'] = 'abcdefghijklmnopqrstvuwxyzABCDEFGHIJKLMNOPQRSTVUWXYZ123456789';
      $this->properties['value'] = null;
   }

   /**
    * Validation of control. 
    * The method passes the "value" attribute to the callback function $check.
    *
    * @param string $type
    * @param Core\IDelegate $check
    * @return mixed
    */
   public function validate($type, Core\IDelegate $check)
   {
      return $check($this->attributes['value']);
   }

   /**
    * The method assigns a "value" property generated code
    * 
    * @access public
    */
   public function init()
   {
      $this->properties['value'] = $this->getRandomCode();
   }

   /**
    * Creates the html code of the control on the basis of established properties and attributes
    *
    * @return string
    * @access public 
    */
   public function render()
   {
      $this->attributes['src'] = 'data:image/jpeg;base64,' . $this->getVerificationImage();
      return parent::render();
   }

   /**
    * The method is intended to generate new code and redraw the image verification
    *
    * @param array $parameters
    * @return type 
    * @access public
    */
   public function redraw(array $parameters = null)
   {
      $this->properties['value'] = $this->getRandomCode();
      return parent::redraw($parameters);
   }

   /**
    * The method generates converted image verification code to base64.
    *
    * @return string 
    * @access public
    */
   public function getVerificationImage()
   {
      $font = Core\IO::dir('application') . '/_includes/verification/arial.ttf';
      $img = imagecreatefromjpeg(Core\IO::dir('application') . '/_includes/verification/bgcolor' . rand(1, 33) . '.jpg');
      $x = imagesx($img);
      $y = imagesy($img);
      $fontsize = 20;
      $fontsizedate = 20;
      $strx = 10;
      $dy = 18;
      $xpad = 25;
      $ypad = 25;
      $white = imagecolorallocate($img, 255, 255, 255);
      $black = imagecolorallocate($img, 0, 0, 0);
      imagettftext($img, $fontsize, 0, intval($xpad / 2 + 1), $dy + intval($ypad / 2), $black, $font, $this->properties['value']);
      imagettftext($img, $fontsizedate, 0, intval($xpad / 2), $dy + intval($ypad / 2) - 1, $black, $font, $this->properties['value']);
      ob_start();
      imagejpeg($img);
      $base64 = base64_encode(ob_get_contents());
      ob_end_clean();
      imagedestroy($img);
      return $base64;
   }

   /**
    * Get a random code of a given length and containing the specified characters.
    *
    * @return string
    * @access protected 
    */
   protected function getRandomCode()
   {
      if ($this->properties['length'] < 1) $this->properties['length'] = 1;
      for ($i = 0; $i < $this->properties['length']; $i++)
      {
         $n = rand(1, strlen($this->properties['chars']));
         $code .= substr($this->properties['chars'], $n - 1, 1);
      }
      return $code;
   }
}

?>
<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijDropDownBox extends DropDownBox
{
  public function __construct($id, $value = null)
  {
    parent::__construct($id, $value);
    $this->properties['theme'] = 'rocket';
  }

  public function CSS()
  {
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }

  public function JS()
  {
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijdropdownbox_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }
  
  protected function repaint()
  {
    $this->ajax->script($this->getDestructor(), 0, true);
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }
  
  protected function getConstructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijdropdown();';
  }
  
  protected function getDestructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijdropdown(\'destroy\');';
  }
}

?>
<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijButton extends WebControl
{
  public function __construct($id, $text = 'Button')
  {
    parent::__construct($id);
    $this->attributes['name'] = $id;
    $this->attributes['type'] = 'button';
    $this->attributes['value'] = null;
    $this->properties['text'] = $text;
  }
  
  public function CSS()
  {
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }

  public function JS()
  {
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijbutton_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }

  public function render()
  {
    if (!$this->properties['visible']) return $this->invisible();
    $html = '<button';
    if ($this->properties['disabled']) $html .= ' disabled="disabled"';
    $html .= $this->getParams() . '>' . $this->properties['text'] . '</button>';
    return $html;
  }
  
  protected function repaint()
  {
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }
  
  protected function getConstructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').button();';
  }
  
  protected function getDestructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').button(\'destroy\');';
  }
}

?>
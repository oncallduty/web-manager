<?php

namespace ClickBlocks\Web\UI\POM;

/**
 *  Class to work with the control "HYPERLINK"
 */
class HyperLink extends WebControl
{
   /**
    * Class constructor
    * Sets the initial attributes and properties
    *
    * @param string $id
    * @param string $href
    * @param string $text 
    */
   public function __construct($id, $href = null, $text = null)
   {
      parent::__construct($id);
      $this->attributes['type'] = null;
      $this->attributes['href'] = $href;
      $this->attributes['target'] = null;
      $this->attributes['hreflang'] = null;      
      $this->properties['text'] = $text;
   }
   
   /**
    * Creates the html code of the control on the basis of established properties and attributes
    *
    * @return string 
    */
   public function render()
   {
      if (!$this->properties['visible']) return $this->invisible();
      return '<a' . $this->getParams() . '>' . $this->properties['text'] . '</a>';
   }
}

?>

<?php
/**
 * ClickBlocks.PHP v. 1.0
 *
 * Copyright (C) 2010  SARITASA LLC
 * http://www.saritasa.com
 *
 * This framework is free software. You can redistribute it and/or modify
 * it under the terms of either the current ClickBlocks.PHP License
 * viewable at theclickblocks.com) or the License that was distributed with
 * this file.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the ClickBlocks.PHP License
 * along with this program.
 *
 * Responsibility of this file: mvc.php
 *
 * @category   MVC
 * @package    MVC
 * @copyright  2007-2010 SARITASA LLC <info@saritasa.com>
 * @link       http://www.saritasa.com
 * @since      File available since Release 1.0.0
 */

namespace ClickBlocks\MVC;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\Web;

/**
 * This class intended for the controling of Page classes.
 *
 * Класс разработан для управления классами страниц.
 *
 * @category   MVC
 * @package    MVC
 * @copyright  2007-2010 SARITASA LLC <info@saritasa.com>
 * @version    Release: 1.0.0
 * @abstract
 */
abstract class MVC
{
  /**
   * HTTP status codes.
   *
   * @var array $codes
   * @access protected
   */
  protected $codes = array(100 => 'Continue',
                           101 => 'Switching Protocols',
                           102 => 'Processing',
                           103 => 'Checkpoint',
                           122 => 'Request-URI too long',
                           200 => 'OK',
                           201 => 'Created',
                           202 => 'Accepted',
                           203 => 'Non-Authoritative Information',
                           204 => 'No Content',
                           205 => 'Reset Content',
                           206 => 'Partial Content',
                           207 => 'Multi-Status',
                           208 => 'Already Reported',
                           226 => 'IM Used',
                           300 => 'Multiple Choices',
                           301 => 'Moved Permanently',
                           302 => 'Found',
                           303 => 'See Other',
                           304 => 'Not Modified',
                           305 => 'Use Proxy',
                           306 => 'Switch Proxy',
                           307 => 'Temporary Redirect',
                           308 => 'Resume Incomplete',
                           400 => 'Bad Request',
                           401 => 'Unauthorized',
                           402 => 'Payment Required',
                           403 => 'Forbidden',
                           404 => 'Not Found',
                           405 => 'Method Not Allowed',
                           406 => 'Not Acceptable',
                           407 => 'Proxy Authentication Required',
                           408 => 'Request Timeout',
                           409 => 'Conflict',
                           410 => 'Gone',
                           411 => 'Length Required',
                           412 => 'Precondition Failed',
                           413 => 'Request Entity Too Large',
                           414 => 'Request-URI Too Long',
                           415 => 'Unsupported Media Type',
                           416 => 'Requested Range Not Satisfiable',
                           417 => 'Expectation Failed',
                           418 => 'I\'m a teapot',
                           420 => 'Enhance Your Calm',
                           422 => 'Unprocessable Entity',
                           423 => 'Locked',
                           424 => 'Failed Dependency',
                           425 => 'Unordered Collection',
                           426 => 'Upgrade Required',
                           428 => 'Precondition Required',
                           429 => 'Too Many Requests',
                           431 => 'Request Header Fields Too Large',
                           444 => 'No Response',
                           449 => 'Retry With',
                           450 => 'Blocked by Windows Parental Controls',
                           499 => 'Client Closed Request',
                           500 => 'Internal Server Error',
                           501 => 'Not Implemented',
                           502 => 'Bad Gateway',
                           503 => 'Service Unavailable',
                           504 => 'Gateway Timeout',
                           505 => 'HTTP Version Not Supported',
                           506 => 'Variant Also Negotiates',
                           507 => 'Insufficient Storage',
                           508 => 'Loop Detected',
                           509 => 'Bandwidth Limit Exceeded',
                           510 => 'Not Extended',
                           511 => 'Network Authentication Required',
                           598 => 'Network read timeout error',
                           599 => 'Network connect timeout error');

   /**
    * The instance of ClickBlocks\Core\Register class.
    *
    * Экземпляр класса ClickBlocks\Core\Register.
    *
    * @var object
    * @access protected
    */
   protected $reg = null;

   /**
    * The instance of URI class.
    *
    * Экземпляр класса URI.
    *
    * @var object
    * @access protected
    */
   protected $uri = null;

   /**
    * The array as a result of merging of GET, POST and FILES data.
    *
    * Массив как результат соединения массивов GET, POST и FILES.
    *
    * @var array
    * @access protected
    */
   protected $fv = null;

   /**
    * Constructs a new MVC object.
    *
    * Конструктор класса.
    *
    * @access public
    */
   public function __construct()
   {
      $this->reg = Core\Register::getInstance();
      $this->reg->fv = $this->fv = array_merge($_GET, $_POST, $_FILES);
      $this->reg->uri = $this->uri = new Utils\URI();
      if ($this->reg->config->isLocked)
      {
         $config = $this->reg->config;
         if (strpos($_SERVER['REQUEST_URI'], $config->unlockKey) !== false || $_COOKIE[md5($config->unlockKey)])
         {
            setcookie(md5($config->unlockKey), true, time() + $config->unlockKeyExpire);
         }
         else
         {
            exit((!is_file(Core\IO::dir($config->lockedPage))) ? MSG_GENERAL_0 : file_get_contents(Core\IO::dir($config->lockedPage)));
         }
      }
   }

   /**
    * Returns the instance of a Page class that associated with a page's URL.
    *
    * Возвращает экземпляр страничного класса, ассоциированного с некоторым URL.
    *
    * @return IPage
    * @access public
    * @abstract
    */
   abstract public function getPage();

   /**
    * Verifies the first time whether the request happened to a page.
    *
    * Проверяет первый ли раз была запрошена страница.
    *
    * @return boolean
    * @access public
    * @static
    */
   public static function isFirstRequest()
   {
      return (Page::isGETRequest() && !Web\Ajax::isAction());
   }

   /**
    * Launches the sequence of calls of page-class's methods.
    *
    * Запускает последовательность вызовов методов страничного класса.
    *
    * @param IPage $page
    * @access public
    */
   public function execute(IPage $page = null)
   {
      $page = ($page) ?: $this->getPage();
      if (!($page instanceof IPage)) throw new \Exception(err_msg('ERR_GENERAL_6', array(get_class($this))));
      $this->reg->page = $page;
      if (!$page->access())
      {
         if ($page->noAccessURL) Web\JS::goURL($page->noAccessURL);
         return;
      }
      if (self::isFirstRequest())
      {
         if ($page->cacheExists())
         {
            echo $page->cacheGet();
            return;
         }
         $page->preparse();
         $page->parse();
         $page->init();
         $page->load();
         $page->unload();
         $page->show();
      }
      else
      {
         $page->input();
         $page->assign();
         $page->load();
         $page->process();
         $page->unload();
         $page->redraw();
         $page->perform();
      }
   }
   
   /**
   * Stops the script execution with some message and HTTP status code.
   *
   * @param integer $status - status of an HTTP response.
   * @param string $body - some text.
   * @access public
   */
   public function stop($status, $body = null)
   {
     $isBody = ($status < 100 || $status >= 200) && $status != 204 && $status != 304 && isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] != 'HEAD';
     if (!headers_sent()) 
     {
       if (substr(PHP_SAPI, 0, 3) === 'cgi') header('Status: ' . $status . ' ' . $this->codes[$status]);
       else header('HTTP/1.1 ' . $status . ' ' . $this->codes[$status]);
     }
     if ($isBody) 
     {
       if ($body !== null) echo $body;
       else
       {
         $file = Core\IO::dir('application') . '/errors/' . $status . '.html';
         if (is_file($file)) echo file_get_contents($file);
       }
     }
     exit;
   }
}

?>

<?php

?>
<html>
<head>
<title>View report</title>

<style type="text/css">
table {
  text-align: left;
  border-collapse: collapse;
  margin: 20px;
}

th, td {
  margin: 0;
  padding: 0px;
  border: 1px solid #ccc;
  background: #E8EDFF;
  border-bottom: 1px solid white;
  color: #669;
  border-top: 1px solid transparent;
  padding: 8px;
}

pre {
  width: 800px;
  overflow: auto;
  margin: 0px 0;
  padding: 10px 8px 10px 12px;
  color: #3F3B36;
  border: 1px solid #E9E7E0;
  border-left: 6px solid #F5D995;
  font: lighter 12px/20px Monaco,'MonacoRegular',monospace;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  height: 200px;
  display: none;
  background: url("data:image/gif;base64,R0lGODlhAQAoAIAAAP////n38CH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1IE1hY2ludG9zaCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpGMjNCRjc2NTZCMUYxMUUxOUNENEUzMjYxM0JCQjhBMSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpGMjNCRjc2NjZCMUYxMUUxOUNENEUzMjYxM0JCQjhBMSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkYyM0JGNzYzNkIxRjExRTE5Q0Q0RTMyNjEzQkJCOEExIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkYyM0JGNzY0NkIxRjExRTE5Q0Q0RTMyNjEzQkJCOEExIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAAAAAAAsAAAAAAEAKAAAAgeMj5nA7f8KADs=") repeat scroll 0 -9px;
}

.toggle {
  cursor: pointer;
  font-style: italic;
  font-weight: bold;
  font-size: 16px;
  color: blue;
}

.toggle {
  display: block;
  width: 800px;
}

</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
  
  $('.toggle').click(function() {
    $(this).parent().find('.log').toggle(500); 
  });

});
</script>
</head>
<body>
<table>
<tr>
  <th>id</th>	
  <th>contact</th>
  <th>version</th>
  <th>crashappversion</th>	
  <th>startmemory</th>
  <th>endmemory</th>	
  <th>log</th>	
  <th>timestamp</th>	
  <th>done</th>	
  <th>analyzed</th>
</tr>

<?php foreach ($rows as $row): ?>

<tr>
  <td><?php echo $row['id'];?></td>	
  <td><?php echo $row['contact'];?></td>
  <td><?php echo $row['version'];?></td>
  <td><?php echo $row['crashappversion'];?></td>	
  <td><?php echo $row['startmemory'];?></td>
  <td><?php echo $row['endmemory'];?></td>	
  <td class="code">
    <span class="toggle">+</span>
    <pre class="log"><?php echo $row['log'];?></pre>
  </td>	
  <td><?php echo $row['timestamp'];?></td>	
  <td>
    <?php if (!$row['done']) : ?>
    <a href="view.php?action=done&id=<?php echo $row['id'];?>&all=<?php echo $all?>">click here to done</a>
    <?php else: echo '1'; endif; ?>
  </td>	
  <td>
    <?php if (!$row['analyzed']) : ?>
    <a href="view.php?action=analyzed&id=<?php echo $row['id'];?>&all=<?php echo $all?>">mark analyzed</a>
    <?php else: echo '1'; endif; ?>
  </td>
</tr>

<?php endforeach; ?>

</table>
</body>
</html>
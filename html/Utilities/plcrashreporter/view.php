<?php

//error_reporting(E_ALL);
//ini_set('display_errors', true);

require_once(__DIR__ . '/../../Application/connect.php');

$reg = \ClickBlocks\Core\Register::getInstance();

$dbconfig = $reg->config->db0;

$dsn = $dbconfig['dsn'];
$dsn = explode(';', $dsn);

$server = explode('=', $dsn[1]);
$server = $server[1];

$base = explode('=', $dsn[0]);
$base = $base[1];

$loginsql = $dbconfig['dbuser'];
$passsql  = $dbconfig['dbpass'];
/*
$server = 'localhost';
$loginsql = 'root'; //'open4sale';
$passsql = ''; //'o6bbggxd98';
$base = 'o4s_saritasa_com'; //sprint2';
*/
$dbcrashtable = 'app_crash';
$dbanalyzetable = 'app_crash_analyzed';
$dbversiontable = 'app_versions';

$link = mysql_connect($server, $loginsql, $passsql) or die("Error connecting");
mysql_select_db($base) or die("Error selecting database");

if (isset($_REQUEST['action']) && isset($_REQUEST['id'])) {
  $action = $_REQUEST['action'];
  $id     = (int) $_REQUEST['id'];
  if ($action == 'done' && $id > 0) {
    $query = 'update ' . $dbcrashtable . ' set done = 1 where id = ' . $id;
    mysql_query($query) or die('Error query update');
  } 
  elseif ($action == 'analyzed' & $id > 0) {
    $query = 'update ' . $dbcrashtable . ' set analyzed = 1 where id = ' . $id;
    mysql_query($query) or die('Error query update');
  }
}

if ($_REQUEST['clear'] == '1') {
  (mysql_query("DELETE FROM $dbcrashtable") and mysql_query("DELETE FROM $dbanalyzetable")) or die('Error query delete all rows');
  header( 'Location: ' . $_SERVER['SCRIPT_NAME'] );
}

if (!empty($_REQUEST['all'])) {
  $all = 1;
} 
else {
  $all = 0;
}

$query = 'select * from ' . $dbcrashtable . ($all ?'':' where done = 0');
$result = mysql_query($query) or die('Error query select');

$rows = array();
while ($row = mysql_fetch_assoc($result)) {
  $rows[] = $row;
}

ob_start();
require __DIR__ . '/view.tpl.php';
echo ob_get_clean();

mysql_free_result($result);

mysql_close($link);
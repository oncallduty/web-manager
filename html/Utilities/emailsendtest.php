<?php

require_once(__DIR__ . '/../Application/connect.php');

use ClickBlocks\Core;

$size = (int)$_REQUEST['size'];
$size or die('size not set');

$email = $_REQUEST['email'];
$email or die('email not set');

$num = (int)$_REQUEST['num'];
$num or die('num not set');

($size > 1024 && $size < 10485760) or die('size should be between 1024 and 10485760');
preg_match(ClickBlocks\Web\UI\POM\ValidatorEmail::EMAIL_REG_EXP, $email) or die('incorrect email');
($num > 0 && $num < 100) or die('num should be between 1 and 100');

$attachment = Core\IO::dir('temp').'/attachtest.bin';
$f = fopen($attachment, 'wb');
define ('BLOCK_SIZE', 102400);
$block = str_repeat("\0", BLOCK_SIZE);
$o = 0;
while ($o < $size)
{
  fwrite($f, $block, min([BLOCK_SIZE, $size-$o]));
  $o += BLOCK_SIZE;
}
fclose($f);

$timer = new \ClickBlocks\Utils\Timer();
$timer->start('Send emails');
$mailer = new \ClickBlocks\Utils\Mailer();
$mailer->AddAttachment($attachment);  
for ($i=0; $i<$num; $i++)
{
  $m = clone $mailer;
  $m->AddAddress($email);
  $m->Body = $m->Subject = 'Test email batch #'.$i;
  //echo 'Send to '.$email;
  if ($m->Send() == false) die ($m->ErrorInfo);
}
$timer->stop();
print_r($timer->getTimes());

?>

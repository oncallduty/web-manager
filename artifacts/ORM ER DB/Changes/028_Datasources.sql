
CALL _assert_db_version(27);

CREATE TABLE `Datasources` (
	`ID` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`clientID` BIGINT(20) UNSIGNED NOT NULL,
	`vendor` ENUM('Relativity') NOT NULL,
	`name` CHAR(64) NOT NULL DEFAULT '',
	`URI` TEXT NULL,
	PRIMARY KEY (`ID`),
	INDEX `Clients_Datasources_clientID_idx` (`clientID` ASC),
	CONSTRAINT `Clients_Datasources_clientID`
	FOREIGN KEY (`clientID`) REFERENCES `Clients` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
);

UPDATE _db_version SET version = 28;
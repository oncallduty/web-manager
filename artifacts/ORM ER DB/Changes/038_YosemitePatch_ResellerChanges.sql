CALL _assert_db_version(37);


/* ED-2567; eDiscovery (Non-scheduling) Reseller */
ALTER TABLE `Clients` ADD COLUMN `resellerClass` ENUM('Scheduling','Reseller','Demo') NULL DEFAULT NULL AFTER `resellerLevel`;
UPDATE `Clients` SET `resellerClass`='Scheduling' WHERE `typeID`='R';
UPDATE `Clients` SET `resellerClass`='Demo' WHERE `typeID`='DR';
UPDATE `Clients` SET `resellerClass`='Demo' WHERE `resellerLevel`='9:Demo';
UPDATE `Clients` SET `resellerLevel`='5:None' WHERE `resellerLevel`='9:Demo';
ALTER TABLE `Clients` CHANGE COLUMN `resellerLevel` `resellerLevel` ENUM('Platinum','Gold','Silver','Bronze','None','1:Platinum','2:Gold','3:Silver','4:Bronze','5:None') NULL DEFAULT NULL;
UPDATE `Clients` SET `resellerLevel`='Platinum' WHERE `resellerLevel`='1:Platinum';
UPDATE `Clients` SET `resellerLevel`='Gold' WHERE `resellerLevel`='2:Gold';
UPDATE `Clients` SET `resellerLevel`='Silver' WHERE `resellerLevel`='3:Silver';
UPDATE `Clients` SET `resellerLevel`='Bronze' WHERE `resellerLevel`='4:Bronze';
UPDATE `Clients` SET `resellerLevel`='None' WHERE `resellerLevel`='5:None';
ALTER TABLE `Clients` CHANGE COLUMN `resellerLevel` `resellerLevel` ENUM('Platinum','Gold','Silver','Bronze','None') NULL DEFAULT NULL;

UPDATE _db_version SET version = 38;


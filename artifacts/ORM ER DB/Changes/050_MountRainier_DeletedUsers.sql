CALL _assert_db_version(49);

ALTER TABLE `Users` CHANGE COLUMN `username` `username` VARCHAR(255) NULL COMMENT '',
DROP INDEX `IDX_Users_Auth`,
ADD UNIQUE INDEX `idx_username` (`username` ASC)  COMMENT '';
UPDATE `Users` SET `username`=NULL WHERE `deleted` IS NOT NULL;

UPDATE _db_version SET version = 50;

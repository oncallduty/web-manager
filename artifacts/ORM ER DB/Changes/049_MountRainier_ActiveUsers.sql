CALL _assert_db_version(48);

UPDATE Users SET deleted=NOW() WHERE deactivated IS NOT NULL;
ALTER TABLE Users DROP COLUMN deactivated;

UPDATE _db_version SET version = 49;

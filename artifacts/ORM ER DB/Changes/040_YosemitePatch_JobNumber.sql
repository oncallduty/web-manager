CALL _assert_db_version(39);


/* ED-2615; JobNumber */
ALTER TABLE `Depositions` ADD COLUMN `jobNumber` VARCHAR(20) NULL DEFAULT NULL AFTER `liveTranscript`;


UPDATE _db_version SET version = 40;

CALL _assert_db_version(38);


/* ED-2605; PlatformLog */
CREATE TABLE `PlatformLog` (
  `ID` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `resellerID` BIGINT(20) UNSIGNED NOT NULL,
  `clientID` BIGINT(20) UNSIGNED NULL,
  `optionID` TINYINT(10) UNSIGNED NOT NULL,
  `pricingTerm` CHAR(3) NOT NULL DEFAULT 'O',
  `created` DATETIME NOT NULL,
  `userID` BIGINT(20) UNSIGNED NULL,
  `caseID` BIGINT(20) UNSIGNED NULL,
  `depositionID` BIGINT(20) UNSIGNED NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `quantity` INT(11) NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `idx_resellerID_clientID` USING BTREE (`resellerID` ASC, `clientID` ASC),
  INDEX `idx_PlatformLog_resellerID_Clients_ID` (`resellerID` ASC),
  INDEX `idx_PlatformLog_clientID_Clients_ID` (`clientID` ASC),
  INDEX `idx_PlatformLog_userID_Users_ID` (`userID` ASC),
  INDEX `idx_PlatformLog_caseID_Cases_ID` (`caseID` ASC),
  INDEX `idx_PlatformLog_depositionID_Depositions_ID` (`depositionID` ASC),
  INDEX `idx_PlatformLog_optionID_LookupPricingOptions_ID` (`optionID` ASC),
  INDEX `idx_PlatformLog_pricingTerm_LookupPricingTerms_typeID` (`pricingTerm` ASC),
  CONSTRAINT `fk_PlatformLog_resellerID_Clients_ID` FOREIGN KEY (`resellerID`) REFERENCES `Clients` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PlatformLog_clientID_Clients_ID` FOREIGN KEY (`clientID`) REFERENCES `Clients` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PlatformLog_optionID_LookupPricingOptions_ID` FOREIGN KEY (`optionID`) REFERENCES `LookupPricingOptions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PlatformLog_pricingTerm_LookupPricingTerms_typeID` FOREIGN KEY (`pricingTerm`) REFERENCES `LookupPricingTerms` (`typeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PlatformLog_userID_Users_ID` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PlatformLog_caseID_Cases_ID` FOREIGN KEY (`caseID`) REFERENCES `Cases` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PlatformLog_depositionID_Depositions_ID` FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


UPDATE _db_version SET version = 39;

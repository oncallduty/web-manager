CALL _assert_db_version(51);

ALTER TABLE `Cases` ADD COLUMN `sourceID` BIGINT(20) UNSIGNED NULL AFTER `class`,
  ADD INDEX `idx_Cases_sourceID_Cases_ID` (`sourceID` ASC);
ALTER TABLE `Cases`
  ADD CONSTRAINT `fk_Cases_sourceID_Cases_ID` FOREIGN KEY (`sourceID`) REFERENCES `Cases` (`ID`) ON DELETE SET NULL ON UPDATE SET NULL;

UPDATE _db_version SET version = 52;

CALL _assert_db_version(41);


/* ED-2681; store exhibit filename to track filename and content type changes */
ALTER TABLE `ExhibitHistory` ADD COLUMN `exhibitFilename` VARCHAR(255) NOT NULL AFTER `introducedBy`;

/* ED-2681; historical data for exhibitFilename */
UPDATE `ExhibitHistory` AS `eh`, `Files` AS `f` SET `eh`.`exhibitFilename`=`f`.`name` WHERE `f`.`ID`=`eh`.`exhibitFileID`;


UPDATE _db_version SET version = 42;

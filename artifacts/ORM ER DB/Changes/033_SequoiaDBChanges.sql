CALL _assert_db_version(32);

ALTER TABLE `Folders`
  ADD COLUMN `class` enum('Folder','Exhibit','Personal','CourtesyCopy','Transcript','WitnessAnnotations') NOT NULL DEFAULT 'Folder' AFTER `isCourtesyCopy`;
UPDATE `Folders` SET `class`='Exhibit' WHERE `isExhibit` = 1;
UPDATE `Folders` SET `class`='Personal' WHERE `isPrivate` = 1;
UPDATE `Folders` SET `class`='CourtesyCopy' WHERE `isCourtesyCopy` = 1;
UPDATE `Folders` SET `class`='Transcript' WHERE `name`='Transcripts';

ALTER TABLE `Clients`
  ADD COLUMN `courtReporterEmail` VARCHAR(255) DEFAULT NULL AFTER `location`,
  ADD COLUMN `salesRep` VARCHAR(255) DEFAULT NULL AFTER `courtReporterEmail`;

ALTER TABLE `ClientNotifications` DROP FOREIGN KEY `FK_USERID_idx`;
ALTER TABLE `ClientNotifications`
	ADD COLUMN `name` VARCHAR(255) NOT NULL AFTER `userID`,
	ADD COLUMN `email` VARCHAR(255) NOT NULL AFTER `name`,
	ADD CONSTRAINT `FK_USERID_idx` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE;

INSERT INTO LookupClientTypes (typeID, type) VALUES ('ER', 'Enterprise Reseller'), ('EC', 'Enterprise Client'), ('DR', 'Demo Reseller');

ALTER TABLE `Depositions`
  ADD COLUMN `enterpriseResellerID` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `attendeeLimit`,
  ADD INDEX `Client_Depositions_ResellerID_idx` (`enterpriseResellerID` ASC);
ALTER TABLE `Depositions`
  ADD CONSTRAINT `Client_Depositions_ResellerID`
    FOREIGN KEY (`enterpriseResellerID`) REFERENCES `Clients` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE;

CREATE TABLE `EnterpriseAgreements` (
	`ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	`enterpriseResellerID` bigint(20) unsigned NOT NULL,
	`enterpriseClientID` bigint(20) unsigned NOT NULL,
	`isInactive` tinyint(1) unsigned NULL DEFAULT '0',
	`isDisabled` tinyint(1) unsigned NULL DEFAULT '0',
	`created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	`accepted` timestamp NULL DEFAULT NULL,
	`deleted` timestamp NULL DEFAULT NULL,
	PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `LookupPricingOptions`
ADD COLUMN `isEnterprise` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `name`;

INSERT INTO `LookupPricingOptions` (`name`,`isEnterprise`)
VALUES ('Enterprise Client - Per Deposition Fee (0-15)', '1'), ('Enterprise Client - Per Deposition Fee (16-100)', '1'),('Enterprise Client - Per Deposition Fee (101+)', '1');

CREATE TABLE `ExhibitHistory` (
  `ID` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exhibitFileID` BIGINT(20) UNSIGNED NOT NULL,
  `sourceFileID` BIGINT(20) UNSIGNED NULL,
  `caseID` BIGINT(20) UNSIGNED NOT NULL,
  `depositionID` BIGINT(20) UNSIGNED NOT NULL,
  `introducedDate` DATETIME NOT NULL,
  `introducedBy` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `idx_ExhibitHistory_Files_exhibitFileID` (`exhibitFileID` ASC),
  INDEX `idx_ExhibitHistory_Files_sourceFileID` (`sourceFileID` ASC),
  INDEX `idx_ExhibitHistory_Cases_caseID` (`caseID` ASC),
  INDEX `idx_ExhibitHistory_Depositions_depositionID` (`depositionID` ASC),
  CONSTRAINT `fk_ExhibitHistory_Files_exhibitFileID` FOREIGN KEY (`exhibitFileID`) REFERENCES `Files` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ExhibitHistory_Files_sourceFileID` FOREIGN KEY (`sourceFileID`) REFERENCES `Files` (`ID`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_ExhibitHistory_Cases_caseID` FOREIGN KEY (`caseID`) REFERENCES `Cases` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ExhibitHistory_Depositions_depositionID` FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UPDATE `LookupPricingOptions` SET `name` = 'Leader - Per Deposition Fee (0-15)' WHERE `ID` = 13;
UPDATE `LookupPricingOptions` SET `name` = 'Leader - Per Deposition Fee (16-100)' WHERE `ID` = 14;
UPDATE `LookupPricingOptions` SET `name` = 'Leader - Per Deposition Fee (101+)' WHERE `ID` = 15;

CREATE TABLE `UserAuth` (
  `ID` bigint(24) unsigned NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) unsigned NOT NULL,
  `word` char(128) DEFAULT NULL,
  `spice` char(64) DEFAULT '',
  `lockCount` tinyint(1) unsigned DEFAULT '0',
  `softLock` timestamp NULL DEFAULT NULL,
  `hardLock` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `expired` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_UserAuth_userID_Users_ID_idx` (`userID`),
  CONSTRAINT `FK_UserAuth_userID_Users_ID` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `PasswordResetRequests`
CHANGE COLUMN `hash` `hash` CHAR(128) NOT NULL ,
ADD COLUMN `salt` VARCHAR(64) NULL DEFAULT NULL AFTER `hash`;

CREATE TABLE `DepositionsAuth` (
  `ID` BIGINT(20) UNSIGNED NOT NULL,
  `code` CHAR(255) NOT NULL,
  `spice` CHAR(32) NOT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `fk_DepositionsAuth_Depositions_ID` FOREIGN KEY (`ID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `PricingModelOptions` DROP FOREIGN KEY `LookupPricingOptions_PricingModelOptions`;
ALTER TABLE `InvoiceCharges` DROP FOREIGN KEY `LookupPricingOptions_InvoiceCharges`;
ALTER TABLE `PricingModelOptions` CHANGE COLUMN `optionID` `optionID` TINYINT(10) UNSIGNED NOT NULL ;
ALTER TABLE `InvoiceCharges` CHANGE COLUMN `optionID` `optionID` TINYINT(10) UNSIGNED NOT NULL ;
ALTER TABLE `LookupPricingOptions` CHANGE COLUMN `ID` `ID` TINYINT(10) UNSIGNED NOT NULL ;
ALTER TABLE `PricingModelOptions`
ADD CONSTRAINT `LookupPricingOptions_PricingModelOptions` FOREIGN KEY (`optionID`) REFERENCES `LookupPricingOptions` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `InvoiceCharges`
ADD CONSTRAINT `LookupPricingOptions_InvoiceCharges` FOREIGN KEY (`optionID`) REFERENCES `LookupPricingOptions` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `PricingEnterpriseOptions` (
  `clientID` BIGINT(20) UNSIGNED NOT NULL,
  `resellerID` BIGINT(20) UNSIGNED NOT NULL,
  `optionID` TINYINT(10) UNSIGNED NOT NULL,
  `typeID` CHAR(3) NOT NULL DEFAULT 'O',
  `price` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`clientID`, `resellerID`, `optionID`),
  INDEX `FK_PricingEnterpriseOptions_resellerID_idx` (`resellerID` ASC),
  INDEX `FK_PricingEnterpriseOptions_idx` (`optionID` ASC),
  INDEX `FK_PricingEnterpriseOptions_typeID_idx` (`typeID` ASC),
  CONSTRAINT `FK_PricingEnterpriseOptions_clientD` FOREIGN KEY (`clientID`) REFERENCES `Clients` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_PricingEnterpriseOptions_resellerID` FOREIGN KEY (`resellerID`) REFERENCES `Clients` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_PricingEnterpriseOptions` FOREIGN KEY (`optionID`) REFERENCES `LookupPricingOptions` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_PricingEnterpriseOptions_typeID` FOREIGN KEY (`typeID`) REFERENCES `LookupPricingTerms` (`typeID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

UPDATE _db_version SET version = 33;

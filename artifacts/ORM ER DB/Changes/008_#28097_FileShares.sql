START TRANSACTION;
CALL _assert_db_version(7);

CREATE TABLE `FileShares` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `depositionID` BIGINT UNSIGNED NOT NULL,
    `createdBy` BIGINT UNSIGNED NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `fileID` BIGINT UNSIGNED NOT NULL,
    `copyFileID` BIGINT UNSIGNED,
    `userID` BIGINT UNSIGNED,
    `attendeeID` BIGINT UNSIGNED,
    CONSTRAINT `PK_FileShares` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `FileShares` ADD CONSTRAINT `Files_FileShares` 
    FOREIGN KEY (`fileID`) REFERENCES `Files` (`ID`) ON DELETE CASCADE;
ALTER TABLE `FileShares` ADD CONSTRAINT `Users_FileShares` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;
ALTER TABLE `FileShares` ADD CONSTRAINT `DepositionAttendees_FileShares` 
    FOREIGN KEY (`attendeeID`) REFERENCES `DepositionAttendees` (`ID`) ON DELETE CASCADE;
ALTER TABLE `FileShares` ADD CONSTRAINT `Depositions_FileShares` 
    FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE;
ALTER TABLE `FileShares` ADD CONSTRAINT `Users_FileShares_Creator` 
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;
ALTER TABLE `FileShares` ADD CONSTRAINT `Files_FileShares_Copy` 
    FOREIGN KEY (`copyFileID`) REFERENCES `Files` (`ID`) ON DELETE CASCADE;

UPDATE _db_version SET version = 8;
COMMIT;
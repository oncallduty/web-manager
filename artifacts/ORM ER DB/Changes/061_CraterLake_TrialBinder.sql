CALL _assert_db_version(60);

/* ED-3539 Files metadata */
ALTER TABLE `Files` ADD COLUMN `mimeType` CHAR(32) NOT NULL DEFAULT '' AFTER `isUpload`, ADD COLUMN `filesize` INT(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `mimeType`, ADD COLUMN `hash` CHAR(32) NOT NULL DEFAULT '' AFTER `filesize`;

/* Trial Binder */
ALTER TABLE `Depositions` CHANGE COLUMN `class` `class` ENUM('Deposition','Demo','WitnessPrep','WPDemo','Trial','Demo Trial','Trial Binder','Mediation','Arbitration','Hearing') NOT NULL DEFAULT 'Deposition';


UPDATE _db_version SET version = 61;

CALL _assert_db_version(15);

ALTER TABLE `DepositionAttendees` 
  ADD `banned` TIMESTAMP NULL AFTER `email`;

UPDATE _db_version SET version = 16;
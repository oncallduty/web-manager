START TRANSACTION;
CALL _assert_db_version(1);

ALTER TABLE `Cases`
	ALTER `typeID` DROP DEFAULT;
ALTER TABLE `Cases`
	CHANGE COLUMN `typeID` `typeID` INT(10) UNSIGNED NULL AFTER `clientID`;

UPDATE _db_version SET version = 2;
COMMIT;
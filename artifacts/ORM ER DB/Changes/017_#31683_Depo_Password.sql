CALL _assert_db_version(16);

ALTER TABLE `Depositions` 
  ADD `password` VARCHAR(255) AFTER `uKey`;

UPDATE `Depositions` SET `password`=MD5('123Gs51&zJzwEy01apJ') WHERE 1;

UPDATE _db_version SET version = 17;
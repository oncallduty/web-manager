CALL _assert_db_version(30);

CREATE TABLE `DatasourceUsers` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) unsigned NOT NULL,
  `datasourceID` bigint(20) unsigned NOT NULL,
  `username` char(255) DEFAULT NULL,
  `password` char(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Users_DatasourceUsers_userID_idx` (`userID`),
  KEY `Datasources_DatasourceUsers_datasourceID_idx` (`datasourceID`),
  KEY `userID_datasourceID_idx` (`userID`,`datasourceID`),
  CONSTRAINT `Datasources_DatasourceUsers_datasourceID` FOREIGN KEY (`datasourceID`) REFERENCES `Datasources` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Users_DatasourceUsers_userID` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

UPDATE _db_version SET version = 31;


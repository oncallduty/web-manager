CALL _assert_db_version(40);


/* ED-2646; Tracking uploads for monthly usage and billing */
ALTER TABLE `Files` CHANGE COLUMN `isTranscript` `isTranscript` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	ADD COLUMN `isUpload` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `sourceID`;

/* ED-2621; adding terms of service */
ALTER TABLE `Users` ADD COLUMN `termsOfService` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `deleted`;


UPDATE _db_version SET version = 41;


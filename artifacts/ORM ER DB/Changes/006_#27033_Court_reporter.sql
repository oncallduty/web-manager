START TRANSACTION;
CALL _assert_db_version(5);

ALTER TABLE `Depositions` DROP FOREIGN KEY `Users_Depositions_CourtReporter`;

ALTER TABLE `Depositions` 
  DROP `courtReporterID`,
  ADD `courtReporterEmail` VARCHAR(255) AFTER `finished`,
  ADD `courtReporterPassword` VARCHAR(255) AFTER `courtReporterEmail`;

ALTER TABLE `Cases` DROP FOREIGN KEY `Users_Cases`;
ALTER TABLE `Cases` ADD CONSTRAINT `Users_Cases` 
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE RESTRICT;

ALTER TABLE `Depositions` DROP FOREIGN KEY `Users_Depositions`;	
ALTER TABLE `Depositions` ADD CONSTRAINT `Users_Depositions` 
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE RESTRICT;

ALTER TABLE `Folders` DROP FOREIGN KEY `Users_Folders`;	
ALTER TABLE `Folders` ADD CONSTRAINT `Users_Folders`
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE RESTRICT;

ALTER TABLE `Files` DROP FOREIGN KEY `Users_Files`;
ALTER TABLE `Files` ADD CONSTRAINT `Users_Files`
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE RESTRICT;
	
ALTER TABLE `CaseManagers` DROP FOREIGN KEY `Users_CaseManagers`;
ALTER TABLE `CaseManagers` ADD CONSTRAINT `Users_CaseManagers` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;
	
ALTER TABLE `DepositionAssistants` DROP FOREIGN KEY `Users_DepositionAssistants`;
ALTER TABLE `DepositionAssistants` ADD CONSTRAINT `Users_DepositionAssistants` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;
	
ALTER TABLE `UserRoles` DROP FOREIGN KEY `Users_UserRoles`;
ALTER TABLE `UserRoles` ADD CONSTRAINT `Users_UserRoles` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;
	
ALTER TABLE `PasswordResetRequests` DROP FOREIGN KEY `Users_PasswordResetRequests`;
ALTER TABLE `PasswordResetRequests` ADD CONSTRAINT `Users_PasswordResetRequests` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;
	
UPDATE Users SET deleted=NULL WHERE deleted IS NOT NULL;

UPDATE _db_version SET version = 6;
COMMIT;
CALL _assert_db_version(46);

ALTER TABLE Users ADD  lastLogin TIMESTAMP NULL DEFAULT NULL;

UPDATE _db_version SET version = 47;
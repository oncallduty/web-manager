UPDATE Cases c
LEFT JOIN Users u ON(c.clientID = u.clientID AND u.typeID = 'C')
SET c.createdBy = u.ID;

UPDATE Depositions d
LEFT JOIN Cases c ON(d.caseID = c.ID)
LEFT JOIN Users u ON(c.clientID = u.clientID AND u.typeID = 'C')
SET d.createdBy = u.ID;

UPDATE Folders f
LEFT JOIN Depositions d ON(f.depositionID = d.ID)
LEFT JOIN Cases c ON(d.caseID = c.ID)
LEFT JOIN Users u ON(c.clientID = u.clientID AND u.typeID = 'C')
SET f.createdBy = u.ID
WHERE f.class IN ('Exhibit', 'CourtesyCopy', 'Transcript', 'Trusted');